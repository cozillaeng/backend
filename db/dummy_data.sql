INSERT INTO `product_categories` (`id`, `name`, `display_name`, `is_enabled`, `created_at`, `updated_at`, `created_by_id`, `updated_by_id`)
VALUES
  (1, 'living-room', 'Living Room', 1, '2015-09-03 08:54:15', '2015-09-03 08:54:15', 0, 0),
  (2, 'sofa', 'Sofa', 1, '2015-09-03 08:56:07', '2015-09-03 08:56:07', 0, 0),
  (3, 'bed-room', 'Bed Room', 1, '2015-09-11 06:56:01', '2015-09-11 06:56:01', 0, 0),
  (4, 'guest-room', 'Guest Room', 1, '2015-09-11 06:56:23', '2015-09-11 06:56:23', 0, 0),
  (5, 'storage', 'Storage', 1, '2015-09-11 06:57:02', '2015-09-11 06:57:02', 0, 0),
  (6, 'small-storage', 'Small Storage', 1, '2015-09-11 06:57:15', '2015-09-11 06:57:15', 0, 0),
  (7, 'large-storage', 'Large Storage', 1, '2015-09-11 06:57:26', '2015-09-11 06:57:26', 0, 0),
  (8, 'wardrobe', 'Wardrobe', 1, '2015-09-11 06:57:36', '2015-09-11 06:57:36', 0, 0),
  (9, 'side-storage', 'Side Storage', 1, '2015-09-11 06:57:47', '2015-09-11 06:57:47', 0, 0),
  (10, '2-seater-sofa', '2 Seater Sofa', 1, '2015-09-11 07:04:25', '2015-09-11 07:04:25', 0, 0),
  (11, '3-seater-sofa', '3 Seater Sofa', 1, '2015-09-11 07:04:32', '2015-09-11 07:04:32', 0, 0),
  (12, '2-seater-sofa', 'Single Seater Sofa', 1, '2015-09-11 07:04:54', '2015-09-11 07:04:54', 0, 0),
  (13, 'dining-room', 'Dining Room', 1, '2015-09-11 07:04:54', '2015-09-11 07:04:54', 0, 0);



INSERT INTO `product_category_children` (`id`, `category_id`, `child_category_id`, `created_at`, `created_by_id`)
VALUES
  (1, 1, 2, '2015-09-11 06:55:33', 1),
  (2, 1, 5, '2015-09-11 07:01:55', 0),
  (3, 3, 2, '2015-09-11 07:03:24', 0),
  (4, 3, 5, '2015-09-11 07:03:31', 0),
  (5, 4, 2, '2015-09-11 07:04:02', 0),
  (6, 4, 5, '2015-09-11 07:04:05', 0),
  (7, 2, 10, '2015-09-11 07:05:08', 0),
  (8, 2, 11, '2015-09-11 07:05:12', 0),
  (9, 2, 12, '2015-09-11 07:05:16', 0),
  (10, 5, 6, '2015-09-11 07:05:28', 0),
  (11, 5, 7, '2015-09-11 07:05:32', 0),
  (12, 7, 8, '2015-09-11 07:05:55', 0),
  (13, 6, 9, '2015-09-11 07:06:06', 0);


INSERT INTO `states` (`id`, `name`, `display_name`)
VALUES
  (1, 'karnataka', 'Karnataka'),
  (2, 'delhi', 'Delhi');


INSERT INTO `cities` (`id`, `name`, `display_name`, `state_id`, `is_enabled`)
VALUES
  (1, 'bangalore', 'Bangalore', 1, 1),
  (2, 'delhi', 'Delhi', 2, 1);


INSERT INTO `designers` (`id`, `first_name`, `last_name`, `display_name`, `email`, `phone_primary`, `phone_secondary`, `address`, `city_id`, `pincode`, `source`, `is_approved`, `is_enabled`, `profile_image_filename`, `created_at`, `updated_at`, `created_by_id`, `updated_by_id`, `website_designer_id`, `about_me`)
VALUES
  (1, 'Rajeev', 'Sharma', 'Rajeev Sharma', 'rajeev.sharma1@livspace.com', '9611666009', '', 'Whitefield', 1, '560067',
   'IN_HOUSE', 1, 1, NULL, '2015-09-03 08:53:38', '2015-09-03 08:53:38', 0, 0, 10, 'What the luck?');


insert into statuses(`name`, `display_name`, `entity_type`) values('STARTED','Project Started','PROJECT');
INSERT INTO `customers` (`id`, `first_name`, `last_name`, `display_name`, `email`, `phone_primary`, `phone_secondary`, `address`, `city_id`, `pincode`, `skype_id`, `created_at`, `updated_at`, `created_by_id`, `updated_by_id`)
VALUES
	(1,'shubham','choudhary','shubham choudhary','shubham.choudhary@livspace.com','1234567890','1234567890','ABCD',1,'123456','skype_id_1','2015-09-24 16:15:46','2015-09-24 16:15:46',1.0,0),
	(2,'name2','last_name','name2 last_name','hello@gmail.com','1234567890','1234567890','ABCD',1,'123456', 'skype_id_2','2015-09-28 10:42:14','2015-09-28 10:42:14',1.0,0),
	(3,'shubham','last_name','shubham last_name','third@gmail.com','1234567890','1234567890','ABCD',1,'123456', 'skype_id_3','2015-09-28 10:45:32','2015-09-28 10:50:58',1.0,0);



INSERT INTO `projects` (`id`, `display_name`, `designer_id`, `start_date`, `status`, `cover_image_filename`, `is_enabled`, `layout_filename`, `property_name`, `preferences`, `budget_min`, `budget_max`, `customer_id`, `created_at`, `updated_at`, `created_by_id`, `updated_by_id`, `address`, `city_id`, `pincode`, `occupancy_date`, `timeline_date`, `commencement_date`, `next_interaction_date`, `last_interaction_date`, `mom_link`)
VALUES
	(1,'DUMMY-PROJECT1',1,'2015-09-25 00:00:00',1,'dummyfilename.jpg',1,'layout_filename_1','property_name_1','hqkrjbq	',12312,12414,1,'2015-09-24 11:24:03','2015-09-24 11:24:03',1.0,0,'address_1',1,'123456','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','mom_link_1'),
	(2,'DUMMY-next_project',1,'2015-09-25 00:00:00',1,'dummyfilename.jpg',1,'layout_filename_2','property_name_2','dlkfblef',123,12414,1,'2015-09-24 11:24:03','2015-09-24 11:24:03',1.0,0,'address_2',1,'23456','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','mom_link_2');

INSERT INTO `designs`(`display_name`,`project_id`,`designer_id`, `is_enabled`,`created_at`,`updated_at`,`created_by_id`,`updated_by_id`,`design_image_filename`,`cover_image_filename`,`design_type`,location_x,location_y,location_z,rotation_x,rotation_y,rotation_z,scale_x,scale_y,scale_z,visible)
VALUES
  ('Design test 3',1,1,1,'2015-09-11 06:55:33','2015-09-11 06:55:33',1,1,'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTVeLYsnjlhpjjErbcfbAZyJesfm-u-SWw5MH3SFpUZUjAo9xXQ', 'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTVeLYsnjlhpjjErbcfbAZyJesfm-u-SWw5MH3SFpUZUjAo9xXQ', 'TEMPLATE',1.0,0,1.0,0,1.0,0,1.0,0,1.0,0),
  ('Design test 4',2,1,1,'2015-09-11 06:55:33','2015-09-11 06:55:33',2,2,'http://2q53b31nfg3p3ge9lk1e9sxx.wpengine.netdna-cdn.com/wp-content/uploads/sites/24/2015/06/D90910PlizzlineS.jpg', 'http://2q53b31nfg3p3ge9lk1e9sxx.wpengine.netdna-cdn.com/wp-content/uploads/sites/24/2015/06/D90910PlizzlineS.jpg', 'CONCRETE',1.0,0,1.0,0,1.0,0,1.0,0,1.0,0),
  ('Design test 5',1,1,1.0,'2015-09-11 06:55:33','2015-09-11 06:55:33',1,1,'http://2q53b31nfg3p3ge9lk1e9sxx.wpengine.netdna-cdn.com/wp-content/uploads/sites/24/2015/06/D90910PlizzlineS.jpg', 'http://2q53b31nfg3p3ge9lk1e9sxx.wpengine.netdna-cdn.com/wp-content/uploads/sites/24/2015/06/D90910PlizzlineS.jpg', 'CONCRETE',1.0,0,1.0,0,1.0,0,1.0,0,1.0,0);

INSERT INTO `catalogue_providers` (`name`,`display_name`)
VALUES
  ('Livspace','Livspace'),
  ('Launchpad','Launchpad');




INSERT INTO `notes` (`description`,`owner_id`,`owner_type`,`created_at`,`updated_at`,`created_by_id`,`updated_by_id`)
VALUES
  ('This is the 1st starting point for our design',1,'DESIGN','2015-09-24 06:55:33','2015-09-24 06:55:33',1,1),
  ('This is the 2nd starting point for our design',1,'DESIGN','2015-09-24 07:55:33','2015-09-24 06:55:33',1,1);

INSERT INTO `tasks` (`id`, `description`, `owner_id`, `owner_type`, `status`, `created_at`, `updated_at`, `created_by_id`, `updated_by_id`)
VALUES
	(1,'wer',1,'DESIGN','COMPLETED','2015-10-13 11:53:40','2015-10-12 11:03:23',1,1);


INSERT INTO `time_lines` (`description`,`owner_id`,`owner_type`,`created_at`)
VALUES
  ('This is the 1st timeline for our design',1,'DESIGN','2015-09-24 06:55:33'),
  ('This is the 1st timeline for our note',1,'NOTE','2015-09-24 07:55:33');

INSERT INTO `products` (id,code,category_id)
VALUES
  (1,"code001",13),
  (2,"code002",13),
  (13,"code0013",13);

insert into sku(id,display_name, detail_page_filename, product_id, top_view_image_filename, cover_image_filename, liv_home_2d_filename, liv_home_3d_filename,texture_image_filename,backed_texture_image_filename,catalouge_image_filename,default_sku,width,height) values
(1,'bar_stool', 'catalouge/bar_stool.png', 13, 'catalouge/bar_stool.png', 'catalouge/bar_stool.png', 'model_2d/bar_stool.png', 'model_3d/bar_stool.js', 'components/sh/bar_stool.jpg',
'components/sh/bar_stool.jpg','1/catalouge/bar_stool.png',1,100,100),

(2,'crockery_unit_01', 'catalouge/crockery_unit_01.png', 13, 'design_image_file.jpg', 'catalouge/crockery_unit_01.png', 'model_2d/crockery_unit_01.png', 'model_3d/crockery_unit_01.js', 'components/sh/crockery_unit_01.jpg',
'components/sh/crockery_unit_01.jpg','2/catalouge/crockery_unit_01.png',0,100,100),

(3,'crockery_unit_02', 'catalouge/crockery_unit_02.png', 13, 'design_image_file.jpg', 'catalouge/crockery_unit_02.png', 'model_2d/crockery_unit_02.png', 'model_3d/crockery_unit_02.js', 'components/sh/crockery_unit_02.jpg',
'components/sh/crockery_unit_02.jpg','3/catalouge/crockery_unit_02.png',0,100,100),

(4,'crockery_unit_03', 'catalouge/crockery_unit_03.png', 13, 'design_image_file.jpg', 'catalouge/crockery_unit_03.png', 'model_2d/crockery_unit_03.png', 'model_3d/crockery_unit_03.js', 'components/sh/crockery_unit_03.jpg',
'components/sh/crockery_unit_03.jpg','4/catalouge/crockery_unit_03.png',0,100,100),

(5,'dining_bench', 'catalouge/dining_bench.png', 13, 'design_image_file.jpg', 'catalouge/dining_bench.png', 'model_2d/dining_bench.png', 'model_3d/dining_bench.js', 'components/sh/dining_bench.jpg',
'components/sh/dining_bench.jpg','5/catalouge/dining_bench.png',0,100,100),

(6,'dining_chair', 'catalouge/dining_chair.png', 13, 'design_image_file.jpg', 'catalouge/dining_chair.png', 'model_2d/dining_chair.png', 'model_3d/dining_chair.js', 'components/sh/dining_chair.jpg',
'components/sh/dining_chair.jpg','6/catalouge/dining_chair.png',0,100,100),

(7,'dining_table_01', 'catalouge/dining_table_01.png', 13, 'design_image_file.jpg', 'catalouge/dining_table_01.png', 'model_2d/dining_table_01.png', 'model_3d/dining_table_01.js', 'components/sh/dining_table_01.jpg',
'components/sh/dining_table_01.jpg','7/catalouge/dining_table_01.png',0,100,100),

(8,'dining_table_02', 'catalouge/crockery_unit_01.png', 13, 'design_image_file.jpg', 'catalouge/dining_table_02.png', 'model_2d/dining_table_02.png', 'model_3d/dining_table_02.js', 'components/sh/dining_table_02.jpg',
'components/sh/dining_table_02.jpg','8/catalouge/dining_table_02.png',0,100,100),

(9,'dining_table_03', 'catalouge/crockery_unit_01.png', 13, 'design_image_file.jpg', 'catalouge/dining_table_03.png', 'model_2d/dining_table_03.png', 'model_3d/dining_table_03.js', 'components/sh/dining_table_03.jpg',
'components/sh/dining_table_03.jpg','9/catalouge/dining_table_03.png',0,100,100),

(10,'dining_table_04', 'catalouge/crockery_unit_01.png', 13, 'design_image_file.jpg', 'catalouge/dining_table_04.png', 'model_2d/dining_table_04.png', 'model_3d/dining_table_04.js', 'components/sh/dining_table_04.jpg',
'components/sh/dining_table_04.jpg','catalouge/dining_table_04.png',0,100,100);


INSERT INTO `doors`(floor_id,sku_id,location_x,location_y,location_z,rotation_x,rotation_y,rotation_z,scale_x,scale_y,scale_z,created_at,updated_at,uuid,wall_uuid,name,visible,width,height,offsetX,offsetY,image_path)
VALUES
  (1,1,1.0,0,1.0,1,2,3,1.5,2.0,3.1,'2015-09-24 07:55:33','2015-09-24 07:55:33','12341','11','door_1',1,10,10,10,10,'/static/images/products/canvas/window_02.png'),
  (2,1,1.0,0,1.0,1,2,3,1.5,2.0,3.1,'2015-09-24 07:55:33','2015-09-24 07:55:33','12342','11','door_2',1,10,10,10,10,'/static/images/products/canvas/window_02.png'),
  (1,2,1.0,0,1.0,1,2,3,1.5,2.0,3.1,'2015-09-24 07:55:33','2015-09-24 07:55:33','12343','13','door_3',1,10,10,10,10,'/static/images/products/canvas/window_02.png'),
  (2,2,1.0,0,1.0,1,2,3,1.5,2.0,3.1,'2015-09-24 07:55:33','2015-09-24 07:55:33','12344','14','door_4',1,10,10,10,10,'/static/images/products/canvas/window_02.png');

INSERT INTO `windows`(floor_id,sku_id,position_x,position_y,position_z,rotation_x,rotation_y,rotation_z,scale_x,scale_y,scale_z,created_at,updated_at,uuid)
VALUES
  (1,1,1.0,0,1.0,1,2,3,1.5,2.0,3.1,'2015-09-24 07:55:33','2015-09-24 07:55:33','12310'),
  (2,1,1.0,0,1.0,1,2,3,1.5,2.0,3.1,'2015-09-24 07:55:33','2015-09-24 07:55:33','12311'),
  (1,2,1.0,0,1.0,1,2,3,1.5,2.0,3.1,'2015-09-24 07:55:33','2015-09-24 07:55:33','12312'),
  (2,2,1.0,0,1.0,1,2,3,1.5,2.0,3.1,'2015-09-24 07:55:33','2015-09-24 07:55:33','12313');


INSERT INTO `sku_to_provider_mappings` (sku_id,provider_id)
VALUES
  (1,1),
  (1,2),
  (2,1);


INSERT INTO `product_components` (id,name,display_name,product_id)
VALUES
  (1,'up','Upholstery',1),
  (2,'st','Structure',2),
  (3,'up','Upholstery',13),
  (4,'st','Structure',13);

INSERT INTO `product_component_options` (id,name,display_name,swatch_image_url,texture_url,baked_texture_url,product_component_id)
VALUES 
  (1,'red','Crimson Red','http://cdn.livspace.com//img/materials/big_images/900.jpg',
    'http://cdn.livspace.com//img/materials/big_images/900.jpg','http://cdn.livspace.com//img/materials/big_images/900.jpg',3),
  (2,'yellow','Gold Yellow','http://cdn.livspace.com//img/materials/big_images/900.jpg',
    'http://cdn.livspace.com//img/materials/big_images/900.jpg','http://cdn.livspace.com//img/materials/big_images/900.jpg',3),
  (3,'walnut','walnut','http://cdn.livspace.com//img/materials/big_images/900.jpg',
    'http://cdn.livspace.com//img/materials/big_images/900.jpg','http://cdn.livspace.com//img/materials/big_images/900.jpg',3),
  (4,'teek','teek','http://cdn.livspace.com//img/materials/big_images/900.jpg',
    'http://cdn.livspace.com//img/materials/big_images/900.jpg','http://cdn.livspace.com//img/materials/big_images/900.jpg',3),
  (5,'Structure1','Structure1','http://cdn.livspace.com//img/materials/big_images/900.jpg',
    'http://cdn.livspace.com//img/materials/big_images/900.jpg','http://cdn.livspace.com//img/materials/big_images/900.jpg',4),
  (6,'Structure2','Structure2','http://cdn.livspace.com//img/materials/big_images/900.jpg',
    'http://cdn.livspace.com//img/materials/big_images/900.jpg','http://cdn.livspace.com//img/materials/big_images/900.jpg',4),
  (7,'Structure3','Structure3','http://cdn.livspace.com//img/materials/big_images/900.jpg',
    'http://cdn.livspace.com//img/materials/big_images/900.jpg','http://cdn.livspace.com//img/materials/big_images/900.jpg',4),
  (8,'Structure4','Structure4','http://cdn.livspace.com//img/materials/big_images/900.jpg',
    'http://cdn.livspace.com//img/materials/big_images/900.jpg','http://cdn.livspace.com//img/materials/big_images/900.jpg',4);
    
INSERT INTO `sku_component_options_map` (id,sku_id,component_option_id)
VALUES
  (1,1,1),
  (2,1,5),
  (3,2,2),
  (4,2,6),
  (5,3,1),
  (6,3,6);
--
--INSERT INTO `furniture_categories` (id,type)
--VALUES
--  (1,'1_seater_sofa'),
--  (2,'2_seater_sofa'),
--  (3,'3_seater_sofa'),
--  (4,'4_seater_sofa'),
--  (5,'5_seater_sofa');
--
INSERT INTO `locations` (`id`, `name`, `display_name`, `is_enabled`, `city_id`, `created_at`, `updated_at`, `created_by_id`, `updated_by_id`)
VALUES
  ('1', 'wakad', 'Wakad', '1', '1', '2015-09-24 07:55:33', '2015-09-24 07:55:33', '1', '1');


INSERT INTO `owners` (`id`, `first_name`, `last_name`, `date_of_birth`, `profile_pic`, `email`, `phone_primary`, `phone_secondary`, `facebook_profile`, `linkedin_profile`, `twitter_profile`, `pancard`, `bank_acc_no`, `bank_ifsc_code`, `company_name`, `designation`, `gender`, `permanent_address`, `permanent_city_id`, `permanent_pincode`, `created_at`, `updated_at`, `created_by_id`, `updated_by_id`)
VALUES
   ('1', 'owner1', '-', '2015-09-24 07:55:33', 'profile_pic', 'abc@gmail.com', '1234567', '1234567', 'facebook1', 'linkedin1', 'twitter', 'pancard', '23543645643', 'saafafq3', 'company1', 'sde', 'MALE', 'address', '1', '123345', '2015-09-24 07:55:33', '2015-09-24 07:55:33', '1', '1');


INSERT INTO `societies` (`id`, `name`, `type`, `location_id`, `address`, `city_id`, `pincode`, `geolat`, `geolong`, `no_of_wings`, `flats_in_a_wing`, `total_flats`, `floors_in_a_wing`, `security_type`, `security_cameras`, `security`, `lift`, `swimming_pool`, `gym`, `badminton`, `playing_area`, `parking`, `club_house`, `power_backup`, `lawn_tennis`, `table_tennis`, `basket_ball`, `gas_pipeline`, `updated_at`, `created_by_id`, `updated_by_id`)
VALUES
    ('1', 'aakruti', 'GATED', '1', 'address', '1', '123456', '12.123', '12.1234', '5', '50', '250', '4', '1_SPOT', '1', '1', '1', '0', '1', '1', '1', '0', '1', '1', '0', '0', '1', '1', '2015-09-24 07:55:33', '1', '1');

INSERT INTO `home_amenities` (`id`, `sofa`, `dinning_table`, `television`, `dth`, `refrigerator`, `washing_machine`, `bed`, `modular_kitchen`, `wifi`, `gas_stove`, `crockery_set`, `wardrobes`, `study_table`, `chair`, `center_table`, `balcony`, `attached_bathroom`, `western_toilet`, `geyser`, `water_purifier`, `iron`, `ac`, `microwave`, `shoe_rack`, `side_table`, `created_at`, `updated_at`, `created_by_id`, `updated_by_id`)
VALUES
    ('1', '1', '2', '3', '4', '5', '6', '7', '1', '8', '9', '10', '11', '12', '13', '14', '1', '1', '1', '15', '16', '17', '18', '19', '20', '21', '2015-09-24 07:55:33', '2015-09-24 07:55:33', '1', '1');

INSERT INTO `flats` (`id`, `owner_id`, `bedroom`, `location_id`, `gender`, `address`, `city_id`, `pincode`, `owner_agreement_id`, `society_id`, `home_amenities_id`, `geolat`, `geolong`, `created_at`, `updated_at`, `created_by_id`, `updated_by_id`)
VALUES
    ('1', '1', '2', '1', 'MALE', 'address', '1', '123456', '1', '1', '1', '12.13', '12.12', '2015-09-24 07:55:33', '2015-09-24 07:55:33', '1', '1'),
    ('2', '1', '3', '1', 'FEMALE', 'address2', '1', '1234', '1', '1', '1', '12.13', '121.13', '2015-09-24 07:55:33', '2015-09-24 07:55:33`', '1', '1');
