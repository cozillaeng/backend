CREATE TABLE `states` (
  `id`           INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name`         VARCHAR(100)     NOT NULL,
  `display_name` VARCHAR(100)     NOT NULL,
  `is_enabled`   TINYINT(1)       NOT NULL DEFAULT 1,
  `created_at`             DATETIME                                      NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at`             DATETIME                                      NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by_id`          INT(11)                                       NOT NULL,
  `updated_by_id`          INT(11)                                       NOT NULL,

  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE `cities` (
  `id`           INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name`         VARCHAR(100)     NOT NULL,
  `display_name` VARCHAR(100)     NOT NULL,
  `state_id`     INT(10) UNSIGNED NOT NULL,
  `is_enabled`   TINYINT(1)       NOT NULL DEFAULT 1,
  `created_at`             DATETIME                                      NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at`             DATETIME                                      NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by_id`          INT(11)                                       NOT NULL,
  `updated_by_id`          INT(11)                                       NOT NULL,

  PRIMARY KEY (`id`),

  CONSTRAINT `valid_state_id` FOREIGN KEY (`state_id`) REFERENCES `states` (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



CREATE TABLE `designers` (
  `id`                     INT(10) UNSIGNED                              NOT NULL AUTO_INCREMENT,
  `website_designer_id`    INT(11)                                       NOT NULL,
  `first_name`             VARCHAR(100)                                  NOT NULL,
  `last_name`              VARCHAR(100)                                  NOT NULL DEFAULT '',
  `display_name`           VARCHAR(100),
  `email`                  VARCHAR(100)                                  NOT NULL,
  `phone_primary`          VARCHAR(20),
  `phone_secondary`        VARCHAR(20),
  `address`                VARCHAR(100),
  `city_id`                INT(10) UNSIGNED,
  `pincode`                VARCHAR(20),
  `about_me`               TEXT,
  `source`                 ENUM('LSMKP', 'DWLL', 'DEZIGNUP', 'IN_HOUSE') NOT NULL,
  `is_approved`            TINYINT(1)                                    NOT NULL DEFAULT 0,
  `is_enabled`             TINYINT(1)                                    NOT NULL DEFAULT 1,
  `profile_image_filename` VARCHAR(100),
  `created_at`             DATETIME                                      NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at`             DATETIME                                      NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by_id`          INT(11)                                       NOT NULL,
  `updated_by_id`          INT(11)                                       NOT NULL,

  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_email` (`email`),
  UNIQUE KEY `website_designer_id` (`website_designer_id`),
  KEY `valid_city_id` (`city_id`),
  CONSTRAINT `designers_ibfk_1` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;




CREATE TABLE `statuses` (
  `id`           INT(10) UNSIGNED                      NOT NULL AUTO_INCREMENT,
  `name`         VARCHAR(100)                          NOT NULL,
  `display_name` VARCHAR(100)                          NOT NULL,
  `entity_type`  ENUM('DESIGNER', 'PROJECT', 'DESIGN') NOT NULL,

  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE `customers` (
  `id`                     INT(10) UNSIGNED                              NOT NULL AUTO_INCREMENT,
  `first_name`             VARCHAR(100)                                  NOT NULL,
  `last_name`              VARCHAR(100)                                  NOT NULL DEFAULT '',
  `display_name`           VARCHAR(100),
  `email`                  VARCHAR(100)                                  NOT NULL,
  `phone_primary`          VARCHAR(20),
  `phone_secondary`        VARCHAR(20),
  `address`                VARCHAR(100),
  `city_id`                INT(10) UNSIGNED,
  `pincode`                VARCHAR(20),
  `skype_id`               VARCHAR(50),
  `created_at`             DATETIME                                      NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at`             DATETIME                                      NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by_id`          INT(11)                                       NOT NULL,
  `updated_by_id`          INT(11)                                       NOT NULL,

  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_email` (`email`),
  KEY `valid_city_id` (`city_id`),
  CONSTRAINT `city_fk_1` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;


CREATE TABLE projects (
  `id`                   INT UNSIGNED AUTO_INCREMENT			                                      NOT NULL,
  `display_name`         VARCHAR(255)                                                                 NOT NULL,
  `designer_id`          INT(10) UNSIGNED                                                             NOT NULL,
  `start_date`           DATETIME DEFAULT CURRENT_TIMESTAMP                                           NOT NULL,
  `status`               INT(10) UNSIGNED                                                             NOT NULL,
  `cover_image_filename` VARCHAR(255)                                                                 NOT NULL,
  `is_enabled`           BOOL DEFAULT FALSE                                                           NOT NULL,
  `layout_filename`      VARCHAR(255)                                                                 ,
  `property_name`        VARCHAR(100)                                                                 ,
  `preferences`            TEXT,
  `budget_min`             FLOAT UNSIGNED,
  `budget_max`             FLOAT UNSIGNED,
  `address`                VARCHAR(100),
  `city_id`                INT(10) UNSIGNED,
  `pincode`                VARCHAR(20),
  `occupancy_date`        DATETIME                                                                    NOT NULL,
  `timeline_date`        DATETIME                                                                     NOT NULL,
  `commencement_date`        DATETIME                                                                 NOT NULL,
  `next_interaction_date`        DATETIME                                                             NOT NULL,
  `last_interaction_date`        DATETIME                                                             NOT NULL,
  `mom_link`             VARCHAR(100),

  `customer_id`          INT(11) UNSIGNED DEFAULT NULL,
  `created_at`           DATETIME DEFAULT CURRENT_TIMESTAMP                                           NOT NULL,
  `updated_at`           DATETIME DEFAULT CURRENT_TIMESTAMP                                           NOT NULL,
  `created_by_id`        INT                                                                          NOT NULL,
  `updated_by_id`        INT                                                                          NOT NULL,
  primary key (`id`),
  CONSTRAINT `project_designers_fk_1` FOREIGN KEY (`designer_id`) REFERENCES `designers` (`id`),
  CONSTRAINT `project_customer_fk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`),
  CONSTRAINT `project_status_fk` FOREIGN KEY (`status`) REFERENCES `statuses` (`id`),
  CONSTRAINT `project_city_fk_1` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE designs (
  id                    INT UNSIGNED AUTO_INCREMENT PRIMARY KEY                                      NOT NULL,
  display_name          VARCHAR(255),
  project_id            INT UNSIGNED,
  design_image_filename VARCHAR(255),
  cover_image_filename  VARCHAR(255),
  `location_x`            FLOAT,
  `location_y`            FLOAT,
  `location_z`            FLOAT,
  `rotation_x`            FLOAT,
  `rotation_y`            FLOAT,
  `rotation_z`            FLOAT,
  `scale_x`               FLOAT,
  `scale_y`               FLOAT,
  `scale_z`               FLOAT,
  `visible`               BOOL DEFAULT TRUE,
  is_enabled            BOOL DEFAULT FALSE                                                           NOT NULL,
  designer_id           INT UNSIGNED,
  created_at            DATETIME DEFAULT CURRENT_TIMESTAMP                                           NOT NULL,
  updated_at            DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP               NOT NULL,
  created_by_id         INT                                                                          NOT NULL,
  updated_by_id         INT                                                                          NOT NULL,
  design_type           ENUM('TEMPLATE','CONCRETE'),
  `uuid`                VARCHAR(63),
  `selected_floor_uuid` VARCHAR(63),
  CONSTRAINT FOREIGN KEY fk_project_id(project_id) REFERENCES projects (id),
  CONSTRAINT `designs_designers_fk_1` FOREIGN KEY (`designer_id`) REFERENCES `designers` (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE `product_categories` (
  `id`            INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name`          VARCHAR(100)     NOT NULL,
  `display_name`  VARCHAR(100)     NOT NULL,
  `is_enabled`    BOOL             NOT NULL DEFAULT TRUE,
  `created_at`    DATETIME         NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at`    DATETIME         NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by_id` INT(11)          NOT NULL,
  `updated_by_id` INT(11)          NOT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE `product_category_children` (
  `id`                INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `category_id`       INT(10) UNSIGNED NOT NULL,
  `child_category_id` INT(10) UNSIGNED NOT NULL,
  `created_at`        DATETIME         NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by_id`     INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  KEY `valid_category_id` (`category_id`),
  KEY `valid_child_category_id` (`child_category_id`),
  CONSTRAINT `category_children_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `product_categories` (`id`),
  CONSTRAINT `category_children_ibfk_2` FOREIGN KEY (`child_category_id`) REFERENCES `product_categories` (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE catalogue_providers (
  `id`                    INT UNSIGNED AUTO_INCREMENT PRIMARY KEY                 NOT NULL,
  `name`               		VARCHAR(255)                                            NOT NULL,
  `display_name`          VARCHAR(255)                                            NOT NULL,
  `created_at`            DATETIME                                                NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at`            DATETIME                                                NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
  )
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;





  CREATE TABLE products (
  `id`                    INT UNSIGNED AUTO_INCREMENT PRIMARY KEY                 NOT NULL,
  `code`                  VARCHAR(255)                                            NOT NULL,
  `category_id`           INT UNSIGNED,
  CONSTRAINT `products_categoryid_fk` FOREIGN KEY (`category_id`) REFERENCES `product_categories` (`id`) ON DELETE CASCADE
  )
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

  CREATE TABLE product_components (
  `id`                    INT UNSIGNED AUTO_INCREMENT PRIMARY KEY                 NOT NULL,
  `name`                  VARCHAR(255),
  `display_name`          VARCHAR(255),
  `product_id`            INT UNSIGNED,
  CONSTRAINT `components_productid_fk` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  UNIQUE KEY `productcomponent_product_combo` (`name`,`product_id`)
  )
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



CREATE TABLE sku (
  `id`              INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `display_name`   VARCHAR(100)     NOT NULL,
  `detail_page_filename` VARCHAR(200)     NOT NULL,
  `product_id`    INT(10) UNSIGNED NOT NULL,
  top_view_image_filename VARCHAR(255),
  cover_image_filename  VARCHAR(255),
  liv_home_2d_filename  VARCHAR(255),
  liv_home_3d_filename  VARCHAR(255),
  texture_image_filename VARCHAR(255),
  backed_texture_image_filename VARCHAR(255),
  catalouge_image_filename VARCHAR(255),
  default_sku BOOL,
  width FLOAT,
  height FLOAT,
  PRIMARY KEY (`id`),
  CONSTRAINT `sku_productid_fk` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;




  CREATE TABLE product_component_options (
  `id`                    INT UNSIGNED AUTO_INCREMENT PRIMARY KEY                 NOT NULL,
  `name`                  VARCHAR(255),
  `display_name`          VARCHAR(255),
  `swatch_image_url`      VARCHAR(255),
  `product_component_id`  INT UNSIGNED,
  `texture_url`           VARCHAR(255),
  `baked_texture_url`     VARCHAR(255),
  CONSTRAINT `options_componentid_fk` FOREIGN KEY (`product_component_id`) REFERENCES `product_components` (`id`) ON DELETE CASCADE,
  UNIQUE KEY `options_combo` (`product_component_id`,`name`)
  )
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;


  CREATE TABLE sku_component_options_map (
  `id`                  INT UNSIGNED  AUTO_INCREMENT PRIMARY KEY                 NOT NULL,
  `sku_id`              INT UNSIGNED,
  `component_option_id` INT UNSIGNED,
  CONSTRAINT `map_componentoptionid_fk` FOREIGN KEY (`component_option_id`) REFERENCES `product_component_options` (`id`) ON DELETE CASCADE,
  CONSTRAINT `map_skuid_fk` FOREIGN KEY (`sku_id`) REFERENCES `sku` (`id`) ON DELETE CASCADE
    )
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



CREATE TABLE notes (
  `id`                    INT UNSIGNED AUTO_INCREMENT PRIMARY KEY                 NOT NULL,
  `description`           TEXT                                                    NOT NULL,
  `owner_id`              INT UNSIGNED                                            NOT NULL,
  `owner_type`            ENUM('DESIGN', 'PROJECT')                               NOT NULL,
  `created_at`            DATETIME                                                NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at`            DATETIME                                                NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by_id`         INT(11),
  `updated_by_id`         INT(11)
  )
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

  CREATE TABLE tasks (
  `id`                    INT UNSIGNED AUTO_INCREMENT PRIMARY KEY                 NOT NULL,
  `description`           TEXT                                                    NOT NULL,
  `owner_id`              INT UNSIGNED                                            NOT NULL,
  `owner_type`            ENUM('DESIGN', 'PROJECT')                               NOT NULL,
  `status`                ENUM('CREATED', 'COMPLETED', 'DELETED')                 NOT NULL,
  `created_at`             DATETIME                                      NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at`             DATETIME                                      NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by_id`          INT(11)                                       NOT NULL,
  `updated_by_id`          INT(11)                                       NOT NULL

  )
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;


  CREATE TABLE time_lines (
  `id`                    INT UNSIGNED AUTO_INCREMENT PRIMARY KEY                 NOT NULL,
  `description`           TEXT                                                    NOT NULL,
  `owner_id`              INT UNSIGNED                                            NOT NULL,
  `owner_type`            ENUM('DESIGN', 'PROJECT', 'NOTE')                       NOT NULL,
  `created_at`            DATETIME                                                NOT NULL DEFAULT CURRENT_TIMESTAMP
  )
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;


  CREATE TABLE doors (
  `id`                    INT UNSIGNED AUTO_INCREMENT PRIMARY KEY                 NOT NULL,
  `floor_id`              INT UNSIGNED                                            NOT NULL,
  `sku_id`                INT UNSIGNED,
  `location_x`            FLOAT                                                   NOT NULL,
  `location_y`            FLOAT                                                   NOT NULL,
  `location_z`            FLOAT                                                   NOT NULL,
  `rotation_x`            FLOAT                                                   NOT NULL,
  `rotation_y`            FLOAT                                                   NOT NULL,
  `rotation_z`            FLOAT                                                   NOT NULL,
  `scale_x`               FLOAT                                                   NOT NULL,
  `scale_y`               FLOAT                                                   NOT NULL,
  `scale_z`               FLOAT                                                   NOT NULL,
  `created_at`            DATETIME                                                NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at`            DATETIME                                                NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `uuid`                  VARCHAR(63),
  `wall_uuid`             VARCHAR(63),
  `name`                  VARCHAR(255),
  `visible`               BOOL DEFAULT TRUE,
  `width`                 FLOAT,
  `height`                FLOAT,
  `offsetX`               FLOAT,
  `offsetY`               FLOAT,
  `image_path`             VARCHAR(255)

 )
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

  CREATE TABLE windows (
  `id`                    INT UNSIGNED AUTO_INCREMENT PRIMARY KEY                 NOT NULL,
  `floor_id`              INT UNSIGNED                                            NOT NULL,
  `sku_id`                INT UNSIGNED,
  `position_x`            FLOAT                                                   NOT NULL,
  `position_y`            FLOAT                                                   NOT NULL,
  `position_z`            FLOAT                                                   NOT NULL,
  `rotation_x`            FLOAT                                                   NOT NULL,
  `rotation_y`            FLOAT                                                   NOT NULL,
  `rotation_z`            FLOAT                                                   NOT NULL,
  `scale_x`               FLOAT                                                   NOT NULL,
  `scale_y`               FLOAT                                                   NOT NULL,
  `scale_z`               FLOAT                                                   NOT NULL,
  `created_at`            DATETIME                                                NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at`            DATETIME                                                NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `uuid`                  VARCHAR(63)
  )
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;


CREATE TABLE albums (
  `id`                    INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `display_name`          VARCHAR(255) NOT NULL,
  `description`           TEXT NOT NULL,
  `owner_id`              INT(10) unsigned NOT NULL,
  `owner_type`            ENUM('DESIGN','PROJECT') NOT NULL,
  `created_at`            DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ALBUMS_UNIQUENESS` (`display_name`,`owner_id`,`owner_type`),
  KEY `ALBUMS_SI` (`display_name`,`owner_id`,`owner_type`)
  )
  ENGINE=InnoDB
  DEFAULT CHARSET=utf8;

CREATE TABLE images (
  `id`                    INT UNSIGNED AUTO_INCREMENT PRIMARY KEY                 NOT NULL,
  `display_name`          VARCHAR(255),
  `description`           TEXT,
  `owner_id`              INT UNSIGNED                                            NOT NULL,
  `owner_type`            ENUM('ALBUM','DESIGN','ROOM')                           NOT NULL,
  `file_name`             VARCHAR(255)                                            NOT NULL,
  `created_at`            DATETIME                                                NOT NULL DEFAULT CURRENT_TIMESTAMP,
  KEY `file_name_unique` (`file_name`)
  )
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;


  CREATE TABLE sku_to_provider_mappings (
  `id`                    INT UNSIGNED AUTO_INCREMENT PRIMARY KEY                 NOT NULL,
  `sku_id`                INT UNSIGNED                                            NOT NULL,
  `provider_id`           INT UNSIGNED                                            NOT NULL,
  CONSTRAINT `provider_skuid_fk` FOREIGN KEY (`sku_id`) REFERENCES `sku` (`id`) ON DELETE CASCADE,
  CONSTRAINT `sku_providerid_fk` FOREIGN KEY (`provider_id`) REFERENCES `catalogue_providers` (`id`) ON DELETE CASCADE
  
    )
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

  CREATE TABLE design_versions (                
  `design_id`             INT UNSIGNED                                            NOT NULL,
  `version_id`            INT UNSIGNED                                            NOT NULL AUTO_INCREMENT,
  `design_data`           TEXT                                                    NOT NULL,
  `created_at`            DATETIME                                                NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY(design_id,version_id),
  CONSTRAINT `versions_designid_fk` FOREIGN KEY (`design_id`) REFERENCES `designs` (`id`) ON DELETE CASCADE
  )
  ENGINE = MyISAM
  DEFAULT CHARSET = utf8;

CREATE TABLE locations (
  `id`                     INT(10) UNSIGNED                              NOT NULL AUTO_INCREMENT,
  `name`                   VARCHAR(100)                                  NOT NULL,
  `display_name`           VARCHAR(100)                                  NOT NULL,
  `is_enabled`            BOOL NOT NULL DEFAULT TRUE,
  `city_id`                INT(10) UNSIGNED                              NOT NULL,
  `created_at`             DATETIME                                      NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at`             DATETIME                                      NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by_id`          INT(11)                                       NOT NULL,
  `updated_by_id`          INT(11)                                       NOT NULL,

  PRIMARY KEY (`id`),
  KEY `valid_city_id_10` (`city_id`),
  CONSTRAINT `city_fk_10` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;


 CREATE TABLE owners (
  `id`                     INT(10) UNSIGNED                              NOT NULL AUTO_INCREMENT,
  `first_name`             VARCHAR(100)                                  NOT NULL,
  `last_name`              VARCHAR(100)                                  NOT NULL DEFAULT '',
  `date_of_birth`          DATETIME                                      NOT NULL,
  `profile_pic`            VARCHAR(100),
  `email`                  VARCHAR(100)                                  NOT NULL,
  `phone_primary`          VARCHAR(20),
  `phone_secondary`        VARCHAR(20),
  `facebook_profile`       VARCHAR(100),
  `linkedin_profile`       VARCHAR(100),
  `twitter_profile`        VARCHAR(100),
  `pancard`                VARCHAR(20),
  `bank_acc_no`            VARCHAR(50),
  `bank_ifsc_code`         VARCHAR(20),
  `company_name`           VARCHAR(50),
  `designation`            VARCHAR(50),
  `gender`                 ENUM('MALE', 'FEMALE') NOT NULL,
  `permanent_address`      VARCHAR(100),
  `permanent_city_id`      INT(10) UNSIGNED,
  `permanent_pincode`      VARCHAR(20),
  `created_at`             DATETIME                                      NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at`             DATETIME                                      NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by_id`          INT(11)                                       NOT NULL,
  `updated_by_id`          INT(11)                                       NOT NULL,

  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_email` (`email`),
  KEY `valid_city_id_2` (`permanent_city_id`),
  CONSTRAINT `city_fk_33` FOREIGN KEY (`permanent_city_id`) REFERENCES `cities` (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

  CREATE TABLE flats (
  `id`                     INT(10) UNSIGNED                              NOT NULL AUTO_INCREMENT,
  `owner_id`               INT(10) UNSIGNED,
  `bedroom`                ENUM('1 BHK','1.5 BHK','2 BHK','2.5 BHK','3 BHK','3.5 BHK','4 BHK','4.5 BHK','5 BHK','5.5 BHK','6 BHK') NOT NULL,
  `location_id`            INT(10) UNSIGNED,
  `gender`                 ENUM('MALE', 'FEMALE') NOT NULL,
  `address`                VARCHAR(100),
  `city_id`                INT(10) UNSIGNED,
  `pincode`                VARCHAR(20),
  `owner_agreement_id`     INT(10) UNSIGNED,
  `society_id`             INT(10) UNSIGNED,
  `home_amenities_id`      INT(10) UNSIGNED,
  `geolat`                 FLOAT,
  `geolong`                FLOAT,
  `created_at`             DATETIME                                      NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at`             DATETIME                                      NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by_id`          INT(11)                                       NOT NULL,
  `updated_by_id`          INT(11)                                       NOT NULL,

  PRIMARY KEY (`id`),
  KEY `valid_city_id_3` (`city_id`),
  KEY `valid_owner_id_1` (`owner_id`),
  KEY `valid_location_id_1` (`location_id`),
  KEY `valid_home_amenities_id_1` (`home_amenities_id`),
  CONSTRAINT `city_fk_44` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`),
  CONSTRAINT `owner_fk_44` FOREIGN KEY (`owner_id`) REFERENCES `owners` (`id`),
  CONSTRAINT `location_fk_1` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`),
  CONSTRAINT `home_amenities_fk_1` FOREIGN KEY (`home_amenities_id`) REFERENCES `home_amenities` (`id`)

)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;


  CREATE TABLE tenants (
  `id`                     INT(10) UNSIGNED                              NOT NULL AUTO_INCREMENT,
  `first_name`             VARCHAR(100)                                  NOT NULL,
  `last_name`              VARCHAR(100)                                  NOT NULL DEFAULT '',
  `date_of_birth`          DATETIME                                      NOT NULL,
  `profile_pic`            VARCHAR(255),
  `city_id`                INT(10) UNSIGNED,
  `location_id`            INT(10) UNSIGNED,
  `email`                  VARCHAR(100)                                  NOT NULL,
  `phone_primary`          VARCHAR(20),
  `phone_secondary`        VARCHAR(20),
  `facebook_profile`       VARCHAR(100),
  `linkedin_profile`       VARCHAR(100),
  `twitter_profile`        VARCHAR(100),
  `pancard`                VARCHAR(255),
  `bank_acc_no`            VARCHAR(50),
  `bank_ifsc_code`         VARCHAR(20),
  `company_name`           VARCHAR(50),
  `designation`            VARCHAR(50),
  `agreement_id`           INT(10) UNSIGNED,
  `move_in_date`           DATETIME                                      NOT NULL,
  `flat_id`                INT(10) UNSIGNED,
  `gender`                 ENUM('MALE', 'FEMALE') NOT NULL,

  `permanent_address`      VARCHAR(100),
  `permanent_city_id`      INT(10) UNSIGNED,
  `permanent_pincode`      VARCHAR(20),
  `created_at`             DATETIME                                      NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at`             DATETIME                                      NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by_id`          INT(11)                                       NOT NULL,
  `updated_by_id`          INT(11)                                       NOT NULL,

  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_email` (`email`),
  KEY `valid_city_id_1` (`city_id`),
  KEY `valid_city_id_2` (`permanent_city_id`),
  KEY `valid_flat_id_1` (`flat_id`),
  KEY `valid_location_id_4` (`location_id`),
  CONSTRAINT `city_fk_11` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`),
  CONSTRAINT `city_fk_22` FOREIGN KEY (`permanent_city_id`) REFERENCES `cities` (`id`),
  CONSTRAINT `flat_fk_2` FOREIGN KEY (`flat_id`) REFERENCES `flats` (`id`),
  CONSTRAINT `location_fk_4` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`)

)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

 CREATE TABLE societies (
  `id`                     INT(10) UNSIGNED                              NOT NULL AUTO_INCREMENT,
  `name`                   VARCHAR(50),
  `type`                   ENUM('GATED','NORMAL') NOT NULL,
  `location_id`            INT(10) UNSIGNED,
  `address`                VARCHAR(100),
  `city_id`                INT(10) UNSIGNED,
  `pincode`                VARCHAR(20),
  `geolat`                 FLOAT,
  `geolong`                FLOAT,
  `no_of_wings`            INT(10),
  `flats_in_a_wing`        INT(10),
  `total_flats`            INT(10),
  `floors_in_a_wing`       INT(10),
  `security_type`          ENUM('1_SPOT','2_SPOT'),
  `security_cameras`       BOOL DEFAULT FALSE,

  `security`               BOOL DEFAULT FALSE,
  `lift`                   BOOL DEFAULT FALSE,
  `swimming_pool`          BOOL DEFAULT FALSE,
  `gym`                    BOOL DEFAULT FALSE,
  `badminton`              BOOL DEFAULT FALSE,
  `playing_area`           BOOL DEFAULT FALSE,
  `club_house`             BOOL DEFAULT FALSE,
  `power_backup`           BOOL DEFAULT FALSE,
  `lawn_tennis`            BOOL DEFAULT FALSE,
  `table_tennis`           BOOL DEFAULT FALSE,
  `basket_ball`            BOOL DEFAULT FALSE,
  `gas_pipeline`           BOOL DEFAULT FALSE,
  `created_at`             DATETIME                                      NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at`             DATETIME                                      NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by_id`          INT(11)                                       NOT NULL,
  `updated_by_id`          INT(11)                                       NOT NULL,

  PRIMARY KEY (`id`),
  KEY `valid_city_id_4` (`city_id`),
  KEY `valid_location_id_3` (`location_id`),
  CONSTRAINT `city_fk_5` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`),
  CONSTRAINT `location_fk_3` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`)

)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE furniture_categories (
  `id`                     INT(10) UNSIGNED                                NOT NULL AUTO_INCREMENT,
  `type`                   VARCHAR(20),
  `created_at`             DATETIME                                      NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at`             DATETIME                                      NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by_id`          INT(11)                                       NOT NULL,
  `updated_by_id`          INT(11)                                       NOT NULL,

  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE rented_furnitures (
  `id`                    INT(10) UNSIGNED                                NOT NULL AUTO_INCREMENT,
  `seller_id`             INT(10) UNSIGNED,
  `category_id`           INT(10) UNSIGNED,
  `date_of_rent`          DATETIME                                        NOT NULL,
  `rent`                  FLOAT,
  `deposit`               FLOAT,
  `lock_in_period`        FLOAT,
  `agreement_id`          INT(10) UNSIGNED,
  `short_description`     VARCHAR(50),
  `long_description`      VARCHAR(100),
  `cover_image`           VARCHAR(255),
  `brand`                 VARCHAR(20),
  `company`               VARCHAR(20),
  `created_at`             DATETIME                                      NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at`             DATETIME                                      NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by_id`          INT(11)                                       NOT NULL,
  `updated_by_id`          INT(11)                                       NOT NULL,


  PRIMARY KEY (`id`),
  KEY `furniture_categories_id_1` (`category_id`),
  CONSTRAINT `furniture_categories_fk_1` FOREIGN KEY (`category_id`) REFERENCES `furniture_categories` (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;


CREATE TABLE purchased_furnitures (
  `id`                    INT(10) UNSIGNED                                NOT NULL AUTO_INCREMENT,
  `seller_id`             INT(10) UNSIGNED,
  `category_id`           INT(10) UNSIGNED,
  `date_of_purchase`      DATETIME                                        NOT NULL,
  `cost`                  FLOAT,
  `warranty`              FLOAT,
  `short_description`     VARCHAR(50),
  `long_description`      VARCHAR(100),
  `cover_image`           VARCHAR(255),
  `brand`                 VARCHAR(20),
  `company`               VARCHAR(20),
  `invoice_copy_filename` VARCHAR(255),
  `created_at`             DATETIME                                      NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at`             DATETIME                                      NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by_id`          INT(11)                                       NOT NULL,
  `updated_by_id`          INT(11)                                       NOT NULL,


  PRIMARY KEY (`id`),
  KEY `furniture_categories_id_2` (`category_id`),
  CONSTRAINT `furniture_categories_fk_2` FOREIGN KEY (`category_id`) REFERENCES `furniture_categories` (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



CREATE TABLE owners_furnitures (
  `id`                    INT(10) UNSIGNED                                NOT NULL AUTO_INCREMENT,
  `owner_id`              INT(10) UNSIGNED,
  `category_id`           INT(10) UNSIGNED,
  `date_of_purchase`      DATETIME                                        NOT NULL,
  `cost`                  FLOAT,
  `warranty`              FLOAT,
  `short_description`     VARCHAR(50),
  `long_description`      VARCHAR(100),
  `cover_image`           VARCHAR(255),
  `brand`                 VARCHAR(20),
  `company`               VARCHAR(20),
  `invoice_copy_filename` VARCHAR(255),
  `created_at`             DATETIME                                      NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at`             DATETIME                                      NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by_id`          INT(11)                                       NOT NULL,
  `updated_by_id`          INT(11)                                       NOT NULL,


  PRIMARY KEY (`id`),
  KEY `furniture_categories_id_2` (`category_id`),
  KEY  `valid_owner_id_2` (`owner_id`),
  CONSTRAINT `furniture_categories_fk_3` FOREIGN KEY (`category_id`) REFERENCES `furniture_categories` (`id`),
  CONSTRAINT `owner_fk_2` FOREIGN KEY (`owner_id`) REFERENCES `owners` (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE furnitures (
 `id`                      INT(10) UNSIGNED                                  NOT NULL AUTO_INCREMENT,
 `purchased_furniture_id`  INT(10) UNSIGNED,
 `rented_furniture_id`     INT(10) UNSIGNED,
 `owners_furniture_id`     INT(10) UNSIGNED,
 `created_at`             DATETIME                                      NOT NULL DEFAULT CURRENT_TIMESTAMP,
 `updated_at`             DATETIME                                      NOT NULL DEFAULT CURRENT_TIMESTAMP,
 `created_by_id`          INT(11)                                       NOT NULL,
 `updated_by_id`          INT(11)                                       NOT NULL,


 PRIMARY KEY (`id`),
  KEY `purchased_furniture_id_1` (`purchased_furniture_id`),
  KEY  `rented_furniture_id_1` (`rented_furniture_id`),
  KEY  `owners_furniture_id_1` (`owners_furniture_id`),

  CONSTRAINT `purchased_furniture_fk_1` FOREIGN KEY (`purchased_furniture_id`) REFERENCES `purchased_furnitures` (`id`),
  CONSTRAINT `rented_furniture_fk_1` FOREIGN KEY (`rented_furniture_id`) REFERENCES `rented_furnitures` (`id`),
  CONSTRAINT `owners_furniture_fk_1` FOREIGN KEY (`owners_furniture_id`) REFERENCES `owners_furnitures` (`id`)

)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE furniture_sets (
  `id`                      INT(10) UNSIGNED                                  NOT NULL AUTO_INCREMENT,
  `furniture_id_1`          INT(10) UNSIGNED,
  `furniture_id_2`          INT(10) UNSIGNED,
  `furniture_id_3`          INT(10) UNSIGNED,
  `furniture_id_4`          INT(10) UNSIGNED,
  `furniture_id_5`          INT(10) UNSIGNED,
  `furniture_id_6`          INT(10) UNSIGNED,
  `furniture_id_7`          INT(10) UNSIGNED,
  `furniture_id_8`          INT(10) UNSIGNED,
  `furniture_id_9`          INT(10) UNSIGNED,
  `furniture_id_10`          INT(10) UNSIGNED,
  `created_at`             DATETIME                                      NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at`             DATETIME                                      NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by_id`          INT(11)                                       NOT NULL,
  `updated_by_id`          INT(11)                                       NOT NULL,


   PRIMARY KEY (`id`)
)

  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE home_amenities (
  `id`                    INT(10) UNSIGNED                                  NOT NULL AUTO_INCREMENT,
  `sofa`                  INT(10) UNSIGNED                                  NOT NULL DEFAULT 0,
  `dinning_table`         INT(10) UNSIGNED                                  NOT NULL DEFAULT 0,
  `television`            INT(10) UNSIGNED                                  NOT NULL DEFAULT 0,
  `dth`                   INT(10) UNSIGNED                                  NOT NULL DEFAULT 0,
  `refrigerator`          INT(10) UNSIGNED                                  NOT NULL DEFAULT 0,
  `washing_machine`       INT(10) UNSIGNED                                  NOT NULL DEFAULT 0,
  `bed`                   INT(10) UNSIGNED                                  NOT NULL DEFAULT 0,
  `modular_kitchen`       BOOL DEFAULT FALSE,
  `wifi`                  INT(10) UNSIGNED                                  NOT NULL DEFAULT 0,
  `gas_stove`             INT(10) UNSIGNED                                  NOT NULL DEFAULT 0,
  `crockery_set`          INT(10) UNSIGNED                                  NOT NULL DEFAULT 0,
  `wardrobes`             INT(10) UNSIGNED                                  NOT NULL DEFAULT 0,
  `study_table`           INT(10) UNSIGNED                                  NOT NULL DEFAULT 0,
  `chair`                 INT(10) UNSIGNED                                  NOT NULL DEFAULT 0,
  `center_table`          INT(10) UNSIGNED                                  NOT NULL DEFAULT 0,
  `balcony`               BOOL DEFAULT FALSE,
  `attached_bathroom`     BOOL DEFAULT FALSE,
  `western_toilet`        BOOL DEFAULT FALSE,
  `geyser`                INT(10) UNSIGNED                                  NOT NULL DEFAULT 0,
  `water_purifier`        INT(10) UNSIGNED                                  NOT NULL DEFAULT 0,
  `iron`                  INT(10) UNSIGNED                                  NOT NULL DEFAULT 0,
  `ac`                    INT(10) UNSIGNED                                  NOT NULL DEFAULT 0,
  `microwave`             INT(10) UNSIGNED                                  NOT NULL DEFAULT 0,
  `shoe_rack`             INT(10) UNSIGNED                                  NOT NULL DEFAULT 0,
  `side_table`            INT(10) UNSIGNED                                  NOT NULL DEFAULT 0,
  `created_at`             DATETIME                                      NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at`             DATETIME                                      NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by_id`          INT(11)                                       NOT NULL,
  `updated_by_id`          INT(11)                                       NOT NULL,

  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;


CREATE TABLE rooms (
 `id`                             INT(10) UNSIGNED                                  NOT NULL AUTO_INCREMENT,
 `flat_id`                        INT(10) UNSIGNED,
 `master_room`                    BOOL DEFAULT FALSE,
 `max_occupancy`                  INT(10) UNSIGNED,
 `single_occupancy_price`         FLOAT,
 `double_occupancy_price`         FLOAT,
 `single_available_occupancy`     INT(10) UNSIGNED,
 `double_available_occupancy`     INT(10) UNSIGNED,
 `tenant_id_1`                    INT(10) UNSIGNED,
 `tenant_id_2`                    INT(10) UNSIGNED,
 `created_at`                     DATETIME                                      NOT NULL DEFAULT CURRENT_TIMESTAMP,
 `updated_at`                     DATETIME                                      NOT NULL DEFAULT CURRENT_TIMESTAMP,
 `created_by_id`                  INT(11)                                       NOT NULL,
 `updated_by_id`                  INT(11)                                       NOT NULL,


 PRIMARY KEY (`id`),
  KEY `verified_flat_id_1` (`flat_id`),
  KEY  `verified_tenant_id_1` (`tenant_id_1`),
  KEY  `verified_tenant_id_2` (`tenant_id_2`),

  CONSTRAINT `verified_flat_fk_1` FOREIGN KEY (`flat_id`) REFERENCES `flats` (`id`),
  CONSTRAINT `verified_tenant_fk_1` FOREIGN KEY (`tenant_id_1`) REFERENCES `tenants` (`id`),
  CONSTRAINT `verified_tenant_fk_2` FOREIGN KEY (`tenant_id_2`) REFERENCES `tenants` (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

