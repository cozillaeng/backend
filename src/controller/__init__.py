# Web APIs
import controller.api.root
import controller.api.designer
import controller.api.design
import controller.api.product_category
import controller.api.city
import controller.api.project
import controller.api.design
import controller.api.customer
import controller.api.note
import controller.api.timeline
import controller.api.product_sku
import controller.api.albums
import controller.api.task
import controller.api.product
import controller.api.tenant
import controller.api.owner
import controller.api.flat
import controller.api.society
import controller.api.location
import controller.api.city
import controller.api.state
import controller.api.home_amenities
import controller.api.society
import controller.api.room

# Handlers
import controller.handlers
