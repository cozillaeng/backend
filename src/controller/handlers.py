import logging

from flask import request

from app import flask
from model.common import HttpResponse
from util.json_util import json_response


@flask.errorhandler(404)
def not_found(code):
    logging.info("Page not found: %s" % code.code)
    return json_response(HttpResponse(code=code.code, message="Not found")), code.code


@flask.errorhandler(Exception)
def not_found(e):
    code = 500
    logging.error("Internal error: %s" % e)
    logging.exception(e)
    return json_response(HttpResponse(code=code, message="Internal error")), code


@flask.before_request
def before_request():
    logging.debug("Before request called for %s" % request.path)


@flask.after_request
def after_request(response):
    logging.debug("After request called")
    return response
