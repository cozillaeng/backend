import logging

from flask import request, url_for

from app import flask, controller
from client.database import Db
from exception import NotFoundException, InvalidInputException
from model.designer import Designer
from model.project import Project
from services.designer import DesignerService
from util.json_util import json_response
from util.api_util import ApiFilters
from util.common import Parser

WS_ID_PREFIX = "website_designer_id:"
EMAIL_ID_PREFIX = "email:"


@flask.route("/v1/designers", methods=["GET"])
@controller.api_controller()
def v1_get_all_designers():
    filters = ApiFilters(request.args.get("filters"))
    include = Parser.str_csv_to_list(request.args.get("include"))

    start = Parser.int(request.args.get("start"), 0)
    count = Parser.int(request.args.get("count"), 50)
    is_approved = Parser.bool(filters.get("approved"))
    source = Parser.str_upper(filters.get("source"))

    page = DesignerService().get_page(start=start, count=count, is_approved=is_approved, source=source)
    return json_response(page, include)


@flask.route("/v1/designers/<id>", methods=["GET"])
@controller.api_controller()
def v1_get_designer(id):
    include = Parser.str_csv_to_list(request.args.get("include"))

    with Db.get() as db:
        if id.startswith(WS_ID_PREFIX):
            ws_designer_id = int(id[len(WS_ID_PREFIX):])
            designer = Designer.get_by_website_designer_id(db, ws_designer_id)
        elif id.startswith(EMAIL_ID_PREFIX):
            email = int(id[len(EMAIL_ID_PREFIX):])
            designer = Designer.get_by_email(db, email)
        else:
            designer = Designer.get(db, id)

        if not designer:
            raise NotFoundException()
    return json_response(designer, include)


@flask.route("/v1/designers/<id>/projects", methods=["GET"])
@controller.api_controller()
def v1_get_designer_projects(id):
    include = Parser.str_csv_to_list(request.args.get("include"))
    projects = []
    with Db.get() as db:
        designer = Designer.get(db, id)
        if not designer:
            raise NotFoundException()
        projects = Project.get_by_designer_id(db, id)
        if not projects:
            projects = []
    return json_response(projects, include)


@flask.route("/v1/designers", methods=["POST"])
@controller.api_controller()
def v1_add_designer():
    try:
        designer_dict = request.get_json(force=True)
        designer = Designer().from_json_dict(designer_dict)
    except:
        raise InvalidInputException("Error in processing input json")

    designer = DesignerService().add(designer)
    return json_response(designer)


@flask.route("/v1/designers/<id>", methods=["DELETE"])
@controller.api_controller()
def v1_delete_designer(id):
    status = DesignerService().delete(id)
    return json_response(status)


@flask.route("/v1/designers/<id>", methods=["PUT"])
@controller.api_controller()
def v1_update_designer(id):
    try:
        designer_dict = request.get_json(force=True)
        designer = Designer().from_json_dict(designer_dict)
    except:
        raise InvalidInputException("Error in processing input json")

    designer = DesignerService().update(id, designer)
    return json_response(designer)
