__author__ = 'shubham'

from app import flask, controller
from flask import request, url_for
from services.task import TaskService
from util.json_util import json_response
from util.api_util import ApiFilters
from util.common import Parser
from model.task import Task
from model.task import Task
from exception import NotFoundException, InvalidInputException


@flask.route("/v1/tasks", methods=["GET"])
@controller.api_controller()
def v1_get_all_tasks():

    filters = ApiFilters(request.args.get("filters"))
    start = Parser.int(request.args.get("start"), 0)
    count = Parser.int(request.args.get("count"), 5)
    owner_id = Parser.int(filters.get("owner_id"))
    owner_type = Parser.str_upper(filters.get("owner_type"))
    status = Parser.str_upper(filters.get("status"))

    page = TaskService().get_page(start=start, count=count, owner_id=owner_id, status=status, owner_type=owner_type)
    page.gen_page_urls(url_for("v1_get_all_tasks"))
    if page.is_prev_page:
        page.prev_page_url = url_for("v1_get_all_tasks", start=page.prev_page_start, count=page.prev_page_count,
                                     owner_id=owner_id, status=status, owner_type=owner_type)
    if page.is_next_page:
        page.next_page_url = url_for("v1_get_all_tasks", start=page.next_page_start, count=page.next_page_count,
                                     owner_id=owner_id, status=status, owner_type=owner_type)
    return json_response(page)


@flask.route("/v1/tasks/<id>", methods=["GET"])
@controller.api_controller()
def v1_get_task(id):
    include = Parser.str_csv_to_list(request.args.get("include"))

    task = TaskService().get(id)
    return json_response(task, include=include)

@flask.route("/v1/tasks/<id>", methods=["DELETE"])
@controller.api_controller()
def v1_delete_task(id):
    TaskService().delete(id)
    response = {"Result": "OK"}
    return json_response(response, status=202)

@flask.route("/v1/tasks", methods=["POST"])
@controller.api_controller()
def v1_add_task():
    try:
        task_dict = request.get_json(force=True)
        task = Task().from_json_dict(task_dict)
    except:
        raise InvalidInputException("Error in processing input json")

    task = TaskService().add(task)
    return json_response(task)

@flask.route("/v1/tasks/<id>", methods=["PUT"])
@controller.api_controller()
def v1_update_task(id):
    try:
        task_dict = request.get_json(force=True)
        task = Task().from_json_dict(task_dict)
    except:
        raise InvalidInputException("Error in processing input json")

    task = TaskService().update(id, task)
    return json_response(task)
