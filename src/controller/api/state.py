import logging

from flask import request, url_for

from app import flask, controller
from client.database import Db
from exception import NotFoundException, InvalidInputException
from model.designer import Designer
from model.project import Project
from model.state import State
from services.designer import DesignerService
from services.state import StateService
from util.json_util import json_response
from util.api_util import ApiFilters
from util.common import Parser



@flask.route("/v1/states", methods=["GET"])
@controller.api_controller()
def v1_get_all_states():
    filters = ApiFilters(request.args.get("filters"))
    include = Parser.str_csv_to_list(request.args.get("include"))

    start = Parser.int(request.args.get("start"), 0)
    count = Parser.int(request.args.get("count"), 50)

    page = StateService().get_page(start=start, count=count)
    return json_response(page, include)

@flask.route("/v1/states/<int:id>", methods=["GET"])
@controller.api_controller()
def v1_get_state(id):
    filters = ApiFilters(request.args.get("filters"))
    include = Parser.str_csv_to_list(request.args.get("include"))

    start = Parser.int(request.args.get("start"), 0)
    count = Parser.int(request.args.get("count"), 50)

    with Db.get() as db:
        state = State.get(db, id)
        if not state:
            raise NotFoundException()
    return json_response(state, include=include)


@flask.route("/v1/states", methods=["POST"])
@controller.api_controller()
def v1_create_state():
    try:
        state_dict = request.get_json(force=True)
        state = State().from_json_dict(state_dict)
    except:
        raise InvalidInputException("Error in processing input json")

    state = StateService().add(state)
    return json_response(state)

@flask.route("/v1/states/<id>", methods=["PUT"])
@controller.api_controller()
def v1_update_state(id):
    try:
        state_dict = request.get_json(force=True)
        state = State().from_json_dict(state_dict)
    except:
        raise InvalidInputException("Error in processing input json")

    state = StateService().update(id, state)

    return json_response(state)

