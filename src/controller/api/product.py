from flask import request
from app import flask, controller
from client.database import Db
from exception import NotFoundException
from util.api_util import ApiFilters
from util.common import Parser
from util.json_util import json_response
from services.product import ProductService
from services.product_sku import ProductSkuService

@flask.route("/v1/products/<id>", methods=["GET"])
@controller.api_controller()
def v1_get_product(id):

    product = ProductService().get_product(id)
    return json_response(product)

@flask.route("/v1/products/<id>/skus", methods=["GET"])
@controller.api_controller()
def v1_get_filtered_sku(id):
	filters = ApiFilters(request.args.get("filters"))
	sku_id = ProductService().get_filtered_skus(id,filters)
	product_sku = ProductSkuService().get_product_sku(sku_id)
	return json_response(product_sku)
