import logging

from flask import request, url_for

from app import flask, controller
from client.database import Db
from exception import NotFoundException, InvalidInputException
from model.designer import Designer
from model.project import Project
from model.city import City
from services.designer import DesignerService
from services.city import CityService
from util.json_util import json_response
from util.api_util import ApiFilters
from util.common import Parser



@flask.route("/v1/cities", methods=["GET"])
@controller.api_controller()
def v1_get_all_cities():
    filters = ApiFilters(request.args.get("filters"))
    include = Parser.str_csv_to_list(request.args.get("include"))

    start = Parser.int(request.args.get("start"), 0)
    count = Parser.int(request.args.get("count"), 50)

    page = CityService().get_page(start=start, count=count)
    return json_response(page, include)

@flask.route("/v1/cities/<int:id>", methods=["GET"])
@controller.api_controller()
def v1_get_cities(id):
    filters = ApiFilters(request.args.get("filters"))
    include = Parser.str_csv_to_list(request.args.get("include"))

    start = Parser.int(request.args.get("start"), 0)
    count = Parser.int(request.args.get("count"), 50)

    with Db.get() as db:
        cities = City.get(db, id)
        if not cities:
            raise NotFoundException()
    return json_response(cities, include=include)


@flask.route("/v1/cities", methods=["POST"])
@controller.api_controller()
def v1_create_cities():
    try:
        cities_dict = request.get_json(force=True)
        cities = City().from_json_dict(cities_dict)
    except:
        raise InvalidInputException("Error in processing input json")

    cities = CityService().add(cities)
    return json_response(cities)

@flask.route("/v1/cities/<id>", methods=["PUT"])
@controller.api_controller()
def v1_update_cities(id):
    try:
        cities_dict = request.get_json(force=True)
        cities = City().from_json_dict(cities_dict)
    except:
        raise InvalidInputException("Error in processing input json")

    cities = CityService().update(id, cities)

    return json_response(cities)

