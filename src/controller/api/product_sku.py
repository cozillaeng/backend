from flask import request
from app import flask, controller
from client.database import Db
from exception import NotFoundException
from services.product_sku import ProductSkuService
from util.api_util import ApiFilters
from util.common import Parser
from util.json_util import json_response


@flask.route("/v1/skus", methods=["GET"])
@controller.api_controller()
def v1_get_skus():
    filters = ApiFilters(request.args.get("filters"))

    include = Parser.str_csv_to_list(request.args.get("include"))
    if not include:
        include = []

    start = Parser.int(request.args.get("start"), 0)
    count = Parser.int(request.args.get("count"), 50)
    product_id = Parser.int(filters.get("product_id"))
    is_placeholder = Parser.bool(filters.get("is_placeholder"))
    if is_placeholder:
        include.append("is_placeholder")

    # if there is no placeholder, call catalog api to fetch data
    if not include:
        return json_response([], include=include)
    else:
        if "is_placeholder" not in include:
            return json_response([], include=include)

    with Db.get() as db:
        skus = ProductSkuService().get_page(start=start, count=count, product_id=product_id)
        if not skus:
            raise NotFoundException()
    return json_response(skus, include=include)

@flask.route("/v1/skus/<id>", methods=["GET"])
@controller.api_controller()
def v1_get_sku(id):

    product_sku = ProductSkuService().get_product_sku(id)
    return json_response(product_sku)

