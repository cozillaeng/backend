import logging

from flask import request, url_for

from app import flask, controller
from client.database import Db
from exception import NotFoundException, InvalidInputException
from model.designer import Designer
from model.project import Project
from model.tenant import Tenant
from services.designer import DesignerService
from services.tenant import TenantService
from util.json_util import json_response
from util.api_util import ApiFilters
from util.common import Parser



@flask.route("/v1/tenants", methods=["GET"])
@controller.api_controller()
def v1_get_all_tenants():
    filters = ApiFilters(request.args.get("filters"))
    include = Parser.str_csv_to_list(request.args.get("include"))

    start = Parser.int(request.args.get("start"), 0)
    count = Parser.int(request.args.get("count"), 50)

    page = TenantService().get_page(start=start, count=count)
    return json_response(page, include)

@flask.route("/v1/tenants/<int:id>", methods=["GET"])
@controller.api_controller()
def v1_get_tenant(id):
    filters = ApiFilters(request.args.get("filters"))
    include = Parser.str_csv_to_list(request.args.get("include"))

    start = Parser.int(request.args.get("start"), 0)
    count = Parser.int(request.args.get("count"), 50)

    with Db.get() as db:
        tenant = Tenant.get(db, id)
        if not tenant:
            raise NotFoundException()
    return json_response(tenant, include=include)


@flask.route("/v1/tenants", methods=["POST"])
@controller.api_controller()
def v1_create_tenant():
    try:
        tenant_dict = request.get_json(force=True)
        tenant = Tenant().from_json_dict(tenant_dict)
    except:
        raise InvalidInputException("Error in processing input json")

    tenant = TenantService().add(tenant)
    return json_response(tenant)

@flask.route("/v1/tenants/<id>", methods=["PUT"])
@controller.api_controller()
def v1_update_tenant(id):
    try:
        tenant_dict = request.get_json(force=True)
        tenant = Tenant().from_json_dict(tenant_dict)
    except:
        raise InvalidInputException("Error in processing input json")

    tenant = TenantService().update(id, tenant)

    return json_response(tenant)
