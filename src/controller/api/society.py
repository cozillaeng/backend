import logging

from flask import request, url_for

from app import flask, controller
from client.database import Db
from exception import NotFoundException, InvalidInputException
from model.designer import Designer
from model.project import Project
from model.society import Society
from services.designer import DesignerService
from services.society import SocietyService
from util.json_util import json_response
from util.api_util import ApiFilters
from util.common import Parser

import ast


@flask.route("/v1/societies", methods=["GET"])
@controller.api_controller()
def v1_get_all_societies():
    filters = ApiFilters(request.args.get("filters"))
    include = Parser.str_csv_to_list(request.args.get("include"))

    start = Parser.int(request.args.get("start"), 0)
    count = Parser.int(request.args.get("count"), 50)

    page = SocietyService().get_page(start=start, count=count)
    return json_response(page, include)

@flask.route("/v1/societies/<int:id>", methods=["GET"])
@controller.api_controller()
def v1_get_society(id):
    filters = ApiFilters(request.args.get("filters"))
    include = Parser.str_csv_to_list(request.args.get("include"))

    start = Parser.int(request.args.get("start"), 0)
    count = Parser.int(request.args.get("count"), 50)

    with Db.get() as db:
        society = Society.get(db, id)
        if not society:
            raise NotFoundException()
    return json_response(society, include=include)


@flask.route("/v1/societies", methods=["POST"])
@controller.api_controller()
def v1_create_society():
    try:
        society_dict = request.get_json(force=True)
        # society_dict = ast.literal_eval(society_dict)
        society = Society().from_json_dict(society_dict)
    except:
        raise InvalidInputException("Error in processing input json")

    society = SocietyService().add(society)
    return json_response(society)

@flask.route("/v1/societies/<id>", methods=["PUT"])
@controller.api_controller()
def v1_update_society(id):
    try:
        society_dict = request.get_json(force=True)

        society = Society().from_json_dict(society_dict)
    except:
        raise InvalidInputException("Error in processing input json")

    society = SocietyService().update(id, society)

    return json_response(society)

@flask.route("/v1/socities/<id>", methods=["DELETE"])
@controller.api_controller()
def v1_delete_socities(id):
    SocietyService().delete(id)
    response = {"Result": "OK"}
    return json_response(response, status=202)