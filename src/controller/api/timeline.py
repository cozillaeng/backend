__author__ = 'ramakrishnag'

from app import flask, controller
from flask import request, url_for
from services.timeline import TimeLineService
from util.json_util import json_response
from util.api_util import ApiFilters
from util.common import Parser
from model.timeline import TimeLine
from exception import NotFoundException, InvalidInputException


@flask.route("/v1/timelines", methods=["GET"])
@controller.api_controller()
def v1_get_all_timelines():

    filters = ApiFilters(request.args.get("filters"))
    start = Parser.int(request.args.get("start"), 0)
    count = Parser.int(request.args.get("count"), 5)
    owner_id = Parser.int(filters.get("owner_id"))
    owner_type = Parser.str_upper(filters.get("owner_type"))

    page = TimeLineService().get_page(start=start, count=count, owner_id=owner_id, owner_type=owner_type)
    return json_response(page)


@flask.route("/v1/timelines/<id>", methods=["GET"])
@controller.api_controller()
def v1_get_timeline(id):
    include = Parser.str_csv_to_list(request.args.get("include"))

    timeline = TimeLineService().get(id)
    return json_response(timeline, include=include)

@flask.route("/v1/timelines/<id>", methods=["DELETE"])
@controller.api_controller()
def v1_delete_timeline(id):
    status = TimeLineService().delete(id)
    return json_response(status)

@flask.route("/v1/timelines", methods=["POST"])
@controller.api_controller()
def v1_add_timeline():
    try:
        timeline_dict = request.get_json(force=True)
        timeline = TimeLine().from_json_dict(timeline_dict)
    except:
        raise InvalidInputException("Error in processing input json")

    timeline = TimeLineService().add(timeline)
    return json_response(timeline)
