from flask import request

from app import flask, controller
from client.database import Db
from exception import NotFoundException
from model.product_category import ProductCategory
from services.category import CategoryService
from util.json_util import json_response
from util.api_util import ApiFilters
from util.common import Parser


@flask.route("/v1/product-category-tree", methods=["GET"])
@controller.api_controller()
def v1_get_category_tree():
    filters = ApiFilters(request.args.get("filters"))
    is_enabled = Parser.bool(filters.get("is_enabled"))
    include = Parser.str_csv_to_list(request.args.get("include"))

    categories = CategoryService().get_tree(is_enabled=is_enabled)
    return json_response(categories, include=include)


@flask.route("/v1/product-categories", methods=["GET"])
@controller.api_controller()
def v1_get_all_categories():
    filters = ApiFilters(request.args.get("filters"))
    is_enabled = Parser.bool(filters.get("is_enabled"))
    include = Parser.str_csv_to_list(request.args.get("include"))

    categories = CategoryService().get_all(is_enabled=is_enabled)
    return json_response(categories, include=include)


@flask.route("/v1/product-categories/<int:id>", methods=["GET"])
@controller.api_controller()
def v1_get_category(id):
    include = Parser.str_csv_to_list(request.args.get("include"))

    with Db.get() as db:
        category = ProductCategory.get(db, id)
        if not category:
            raise NotFoundException()
    return json_response(category, include=include)


@flask.route("/v1/product-categories/<int:id>/children", methods=["GET"])
@controller.api_controller()
def v1_get_category_children(id):
    include = Parser.str_csv_to_list(request.args.get("include"))

    categories = CategoryService().get_children(id)
    return json_response(categories, include=include)


@flask.route("/v1/product-categories/<int:id>/products", methods=["GET"])
@controller.api_controller()
def v1_get_category_products(id):
    include = Parser.str_csv_to_list(request.args.get("include"))

    categories = CategoryService().get_products(id)
    return json_response(categories, include=include)