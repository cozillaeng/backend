import logging

from flask import request, url_for

from app import flask, controller
from client.database import Db
from exception import NotFoundException, InvalidInputException
from model.designer import Designer
from model.project import Project
from model.customer import Customer
from services.designer import DesignerService
from services.customer import CustomerService
from util.json_util import json_response
from util.api_util import ApiFilters
from util.common import Parser



@flask.route("/v1/customers", methods=["GET"])
@controller.api_controller()
def v1_get_all_customers():
    filters = ApiFilters(request.args.get("filters"))
    include = Parser.str_csv_to_list(request.args.get("include"))

    start = Parser.int(request.args.get("start"), 0)
    count = Parser.int(request.args.get("count"), 50)

    page = CustomerService().get_page( start=start, count=count)
    return json_response(page, include)



@flask.route("/v1/customers/<int:id>", methods=["GET"])
@controller.api_controller()
def v1_get_customer(id):
    filters = ApiFilters(request.args.get("filters"))
    include = Parser.str_csv_to_list(request.args.get("include"))

    start = Parser.int(request.args.get("start"), 0)
    count = Parser.int(request.args.get("count"), 50)

    with Db.get() as db:
        customer = Customer.get(db, id)
        if not customer:
            raise NotFoundException()
    return json_response(customer, include=include)


@flask.route("/v1/customers", methods=["POST"])
@controller.api_controller()
def v1_create_customer():

    try:
        customer_dict = request.get_json(force=True)
        customer = Customer().from_json_dict(customer_dict)
    except:
        raise InvalidInputException("Error in processing input json")

    customer = CustomerService().add(customer)
    return json_response(customer)

@flask.route("/v1/customers/<id>", methods=["PUT"])
@controller.api_controller()
def v1_update_customer(id):
    try:
        customer_dict = request.get_json(force=True)
        customer = Customer().from_json_dict(customer_dict)
    except:
        raise InvalidInputException("Error in processing input json")

    customer = CustomerService().update(id, customer)

    return json_response(customer)
