import logging

from flask import request, url_for

from app import flask, controller
from client.database import Db
from exception import NotFoundException, InvalidInputException
from model.designer import Designer
from model.project import Project
from model.room import Room
from services.designer import DesignerService
from services.room import RoomService
from util.json_util import json_response
from util.api_util import ApiFilters
from util.common import Parser


@flask.route("/v1/rooms", methods=["GET"])
@controller.api_controller()
def v1_get_all_rooms():
    filters = ApiFilters(request.args.get("filters"))
    include = Parser.str_csv_to_list(request.args.get("include"))

    start = Parser.int(request.args.get("start"), 0)
    count = Parser.int(request.args.get("count"), 50)
    flat_id = Parser.int(filters.get("flat_id"))
    is_enabled = Parser.bool(filters.get("is_enabled"))

    page = RoomService().get_page(start=start, count=count, flat_id=flat_id, is_enabled=is_enabled)
    return json_response(page, include)

@flask.route("/v1/rooms/<int:id>", methods=["GET"])
@controller.api_controller()
def v1_get_room(id):
    filters = ApiFilters(request.args.get("filters"))
    include = Parser.str_csv_to_list(request.args.get("include"))

    start = Parser.int(request.args.get("start"), 0)
    count = Parser.int(request.args.get("count"), 50)

    with Db.get() as db:
        room = Room.get(db, id)
        if not room:
            raise NotFoundException()
    return json_response(room, include=include)


@flask.route("/v1/rooms", methods=["POST"])
@controller.api_controller()
def v1_create_room():
    try:
        room_dict = request.get_json(force=True)
        room = Room().from_json_dict(room_dict)
    except:
        raise InvalidInputException("Error in processing input json")

    room = RoomService().add(room)
    return json_response(room)

@flask.route("/v1/rooms/<id>", methods=["PUT"])
@controller.api_controller()
def v1_update_room(id):
    try:
        room_dict = request.get_json(force=True)
        room = Room().from_json_dict(room_dict)
    except:
        raise InvalidInputException("Error in processing input json")

    room = RoomService().update(id, room)

    return json_response(room)

@flask.route("/v1/rooms/<id>", methods=["DELETE"])
@controller.api_controller()
def v1_delete_rooms(id):
    RoomService().delete(id)
    response = {"Result": "OK"}
    return json_response(response, status=202)