__author__ = 'ramakrishnag'

from app import flask, controller
from flask import request, url_for
from services.note import NoteService
from util.json_util import json_response
from util.api_util import ApiFilters
from util.common import Parser
from model.note import Note
from exception import NotFoundException, InvalidInputException


@flask.route("/v1/notes", methods=["GET"])
@controller.api_controller()
def v1_get_all_notes():

    filters = ApiFilters(request.args.get("filters"))
    start = Parser.int(request.args.get("start"), 0)
    count = Parser.int(request.args.get("count"), 5)
    owner_id = Parser.int(filters.get("owner_id"))
    owner_type = Parser.str_upper(filters.get("owner_type"))

    page = NoteService().get_page(start=start, count=count, owner_id=owner_id, owner_type=owner_type)
    return json_response(page)


@flask.route("/v1/notes/<id>", methods=["GET"])
@controller.api_controller()
def v1_get_note(id):
    include = Parser.str_csv_to_list(request.args.get("include"))

    note = NoteService().get(id)
    return json_response(note, include=include)

@flask.route("/v1/notes/<id>", methods=["DELETE"])
@controller.api_controller()
def v1_delete_note(id):
    NoteService().delete(id)
    response = {"Result": "OK"}
    return json_response(response, status=202)

@flask.route("/v1/notes", methods=["POST"])
@controller.api_controller()
def v1_add_note():
    try:
        note_dict = request.get_json(force=True)
        note = Note().from_json_dict(note_dict)
    except:
        raise InvalidInputException("Error in processing input json")

    note = NoteService().add(note)
    return json_response(note)
