import logging

from flask import request, url_for

from app import flask, controller
from client.database import Db
from exception import NotFoundException, InvalidInputException
from model.designer import Designer
from model.project import Project
from model.home_amenities import HomeAmenities
from services.designer import DesignerService
from services.home_amenities import HomeAmenitiesService
from util.json_util import json_response
from util.api_util import ApiFilters
from util.common import Parser



@flask.route("/v1/home_amenities", methods=["GET"])
@controller.api_controller()
def v1_get_all_home_amenities():
    filters = ApiFilters(request.args.get("filters"))
    include = Parser.str_csv_to_list(request.args.get("include"))

    start = Parser.int(request.args.get("start"), 0)
    count = Parser.int(request.args.get("count"), 50)

    page = HomeAmenitiesService().get_page(start=start, count=count)
    return json_response(page, include)

@flask.route("/v1/home_amenities/<int:id>", methods=["GET"])
@controller.api_controller()
def v1_get_home_amenities(id):
    filters = ApiFilters(request.args.get("filters"))
    include = Parser.str_csv_to_list(request.args.get("include"))

    start = Parser.int(request.args.get("start"), 0)
    count = Parser.int(request.args.get("count"), 50)

    with Db.get() as db:
        home_amenities = HomeAmenities.get(db, id)
        if not home_amenities:
            raise NotFoundException()
    return json_response(home_amenities, include=include)


@flask.route("/v1/home_amenities", methods=["POST"])
@controller.api_controller()
def v1_create_home_amenities():
    try:
        home_amenities_dict = request.get_json(force=True)
        home_amenities = HomeAmenities().from_json_dict(home_amenities_dict)
    except:
        raise InvalidInputException("Error in processing input json")

    home_amenities = HomeAmenitiesService().add(home_amenities)
    return json_response(home_amenities)

@flask.route("/v1/home_amenities/<id>", methods=["PUT"])
@controller.api_controller()
def v1_update_home_amenities(id):
    try:
        home_amenities_dict = request.get_json(force=True)
        home_amenities = HomeAmenities().from_json_dict(home_amenities_dict)
    except:
        raise InvalidInputException("Error in processing input json")

    home_amenities = HomeAmenitiesService().update(id, home_amenities)

    return json_response(home_amenities)

@flask.route("/v1/home_amenities/<id>", methods=["DELETE"])
@controller.api_controller()
def v1_delete_home_amenities(id):
    HomeAmenitiesService().delete(id)
    response = {"Result": "OK"}
    return json_response(response, status=202)