from flask import request
from app import flask, controller
from client.database import Db
from exception import NotFoundException, InvalidInputException
from model.project import Project
from services.project import ProjectService
from model.design import Design
from util.api_util import ApiFilters
from util.common import Parser
from util.json_util import json_response


@flask.route("/v1/projects", methods=["GET"])
@controller.api_controller()
def v1_get_projects():
    filters = ApiFilters(request.args.get("filters"))
    include = Parser.str_csv_to_list(request.args.get("include"))

    start = Parser.int(request.args.get("start"), 0)
    count = Parser.int(request.args.get("count"), 50)
    designer_id = Parser.str_upper(filters.get("designer_id"))

    with Db.get() as db:
        project = ProjectService().get_page(start=start, count=count, designer_id=designer_id)
        if not project:
            raise NotFoundException()
    return json_response(project, include=include)

@flask.route("/v1/projects/<int:id>", methods=["GET"])
@controller.api_controller()
def v1_get_project(id):
    filters = ApiFilters(request.args.get("filters"))
    include = Parser.str_csv_to_list(request.args.get("include"))

    start = Parser.int(request.args.get("start"), 0)
    count = Parser.int(request.args.get("count"), 50)

    with Db.get() as db:
        project = Project.get(db, id)
        if not project:
            raise NotFoundException("Project not found")
    return json_response(project, include=include)

@flask.route("/v1/projects/<id>/designs", methods=["GET"])
@controller.api_controller()
def v1_get_project_designs(id):
    include = Parser.str_csv_to_list(request.args.get("include"))
    with Db.get() as db:
        project = Project.get(db, id)
        if not project:
            raise NotFoundException()
        projects = Design.get_by_project_id(db, id)
        if not projects:
            projects = []
    return json_response(projects, include)

@flask.route("/v1/projects", methods=["POST"])
@controller.api_controller()
def v1_create_project():
    try:
        project_dict = request.get_json(force=True)
        project = Project().from_json_dict(project_dict)
    except:
        raise InvalidInputException("Error in processing input json")

    project = ProjectService().add(project)
    return json_response(project)

@flask.route("/v1/projects/<id>", methods=["PUT"])
@controller.api_controller()
def v1_update_project(id):
    try:
        project_dict = request.get_json(force=True)
        project = Project().from_json_dict(project_dict)
    except:
        raise InvalidInputException("Error in processing input json")

    project = ProjectService().update(id,project)
    return json_response(project)
