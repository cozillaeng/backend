from model.images import Images
from services.images import ImageService
from util.image_url_util import DesignImageURL

__author__ = 'ramakrishnag'

from app import flask, controller
from flask import request
from services.design import DesignService
from util.json_util import json_response
from util.api_util import ApiFilters
from util.common import Parser
from model.design import Design
from exception import InvalidInputException, NotFoundException, InvalidStateException


@flask.route("/v1/designs", methods=["GET"])
@controller.api_controller()
def v1_get_all_designs():

    print "Inside v1_get_all_designs"
    filters = ApiFilters(request.args.get("filters"))

    start = Parser.int(request.args.get("start"), 0)
    count = Parser.int(request.args.get("count"), 5)
    is_enabled = Parser.bool(filters.get("is_enabled"))
    project_id = Parser.int(filters.get("project_id"))
    designer_id = Parser.int(filters.get("designer_id"))
    created_by_id = Parser.str_upper(filters.get("created_by_id"))

    page = DesignService().get_page(start=start, count=count, is_enabled=is_enabled,
                                    created_by_id=created_by_id, project_id=project_id, designer_id=designer_id)

    return json_response(page)


@flask.route("/v1/designs/<id>", methods=["GET"])
@controller.api_controller()
def v1_get_design(id):
    include = Parser.str_csv_to_list(request.args.get("include"))
    designer_id = Parser.int(request.args.get("designerid"), 0)
    project_id = Parser.int(request.args.get("projectid"), 0)
    accessible = DesignService().check_design_permission(id,designer_id,project_id)
    if not accessible:
        response = {"Result": "Permission denied ,you are not authorized to access the design"}
        return json_response(response, status=401)

    design = DesignService().get(id)
    return json_response(design, include)

@flask.route("/v1/designs/<id>", methods=["DELETE"])
@controller.api_controller()
def v1_delete_design(id):
    DesignService().delete(id)
    response = {"Result": "OK"}
    return json_response(response, status=202)

@flask.route("/v1/designs", methods=["POST"])
@controller.api_controller()
def v1_add_design():
    try:
        design_dict = request.get_json(force=True)
        design = Design().from_json_dict(design_dict)
    except:
        raise InvalidInputException("Error in processing input json")

    #TODO , have to remove all the defaults once the tool integrated
    if not design.design_type:
        design.design_type = "CONCRETE"
    if not design.designer_id:
        design.designer_id = 1
    if not design.project_id:
        design.project_id = 1
    if not design.created_by_id:
        design.created_by_id = 1
    if not design.updated_by_id:
        design.updated_by_id = 1

    accessible = DesignService().check_design_permission(designer_id=design.designer_id,project_id=design.project_id)
    if not accessible:
        response = {"Result": "Permission denied ,you are not authorized to access the design"}
        return json_response(response, status=401)

    design = DesignService().add(design)
    return json_response(design)

@flask.route("/v1/designs/<id>", methods=["PUT"])
@controller.api_controller()
def v1_update_design(id):
    try:
        design_dict = request.get_json(force=True)
    except:
        raise InvalidInputException("Error in processing input json")

    design = DesignService().update(id, design_dict)
    return json_response(design)

#@flask.route("/v1/designs/sku", methods=["POST"])
#@controller.api_controller()
#def v1_add_design_sku():
#    include = ["sku_mappings","rooms"]
#    try:
#        floor_sku_dict = request.get_json(force=True)
#        floor_sku = FloorSkuMap().from_json_dict(floor_sku_dict)
#    except:
#        raise InvalidInputException("Error in processing input json")
#
#    design = DesignService().add_design_sku(floor_sku)
#    return json_response(design,include=include)

#@flask.route("/v1/designs/sku/<id>", methods=["DELETE"])
#@controller.api_controller()
#def v1_delete_design_sku(id):
#    include = ["sku_mappings","rooms"]
#    design = DesignService().delete_design_sku(id)
#    return json_response(design,include=include)

@flask.route("/v1/designs/<id>/clone", methods=["GET"])
@controller.api_controller()
def v1_clone_design(id):
    new_design = DesignService().clone_design(id)
    return json_response(new_design)

@flask.route("/v1/designs/<id>/addversion", methods=["GET"])
@controller.api_controller()
def v1_add_version(id):
    new_design = DesignService().add_version(id)
    response = {"status":"success"}
    return json_response(response, status=202)


@flask.route("/v1/designs/<design_id>/images", methods=["POST"])
@controller.api_controller()
def v1_add_image(design_id):
    design_image = request.get_json(force=True)
    designer_id = Parser.int(design_image["designer_id"], 0)
    project_id = Parser.int(design_image["project_id"], 0)
    accessible = DesignService().check_design_permission(designer_id=designer_id, project_id=project_id)
    if not accessible:
        response = {"Result": "Permission denied ,you are not authorized to access the design"}
        return json_response(response, status=401)
    DesignService().get(design_id)
    image_url_context = DesignImageURL(design_id)
    try:
        design_image["owner_id"] = design_id
        design_image["owner_type"] = "DESIGN"
        images = Images().from_json_dict(design_image, image_url_context)
    except NotFoundException as e:
        raise e
    except:
        raise InvalidInputException("Error in processing input json")
    images = ImageService().add(images)
    images_json = images.to_json_dict(image_url_context)
    if images_json["owner_type"] != "DESIGN":
        raise InvalidStateException("Expecting owner_type as DESIGN but found " + images_json["owner_type"])
    images_json["design"] = {"id": images_json["owner_id"]}
    if "owner_type" in images_json: del images_json["owner_type"]
    if "owner_id" in images_json: del images_json["owner_id"]
    return json_response(images_json)


