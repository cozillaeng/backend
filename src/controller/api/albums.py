from flask import request

from app import flask, controller

from exception import InvalidInputException, NotFoundException, InvalidStateException
from model.albums import Album, ALBUM_IMAGES_OWNER_TYPE
from model.images import Images
from services.albums import AlbumService
from services.images import ImageService
from services.project import ProjectService
from util.common import Parser
from util.image_url_util import ProjectAlbumsImageUrl
from util.json_util import json_response


# Author : Govind
# To be deleted as we don't want backend to have any form based api. Keeping
# it commented for few days in case its required.
# @flask.route("/uploadpic", methods=["POST"])
# @controller.api_controller()
# def upload_albums():
#     print json.dumps(request.form)
#     print request.headers
#     content_type = Parser.str_lower(request.headers.get("Content-Type"))
#     if 'id' in request.form:
#         id = request.form['id']
#     if 'type' in request.form:
#         type = request.form['type']
#     if 'original_name' in request.form:
#         name = request.form['original_name']
#
#     if 'file' in request.files:
#         upload_file = request.files['file']
#         data = upload_file.read()
#         s3_conf = Conf.get("amazon_s3")
#         s3_client = AmazonS3Client(s3_conf["key_id"], s3_conf["key_secret"])
#         image_name = str(int(time.time() * 1000000)) + str(random.randint(10000, 99999))
#         image_bucket = s3_client.get_bucket(s3_conf["bucket_images"])
#         if name:
#             ext = name.split(".")[-1]
#             image_name = image_name + "." + ext
#         key = "/tmp/images/" + image_name
#         s3_client.put_str(image_bucket, key=key, data=data, headers={"Content-Type": content_type})
#         print dump_json({"image_path": key})
#         return Response(response=dump_json({"image_path": key}))
#
#     raise InvalidInputException("No file found to upload")
ALBUM_PREFIX = "album:"


@flask.route("/v1/projects/<project_id>/albums", methods=["POST"])
@controller.api_controller()
def v1_create_album(project_id):
    ProjectService().get(project_id)
    try:
        album_dict = request.get_json(force=True)
        album_dict["owner_id"] = int(project_id)
        album_dict["owner_type"] = "PROJECT"
        album = Album().from_json_dict(album_dict)
    except:
        raise InvalidInputException("Error in processing input json")

    album = AlbumService().add(album)
    album_json = album.to_json_dict()
    album_response_dict = filter_attributes_from_json_dict(album_json, "owner_type", "owner_id")
    return json_response(album_response_dict)


def filter_attributes_from_json_dict(json_dict, *attributes):
    for attribute in attributes:
        if attribute in json_dict:
            del json_dict[attribute]
    return json_dict


@flask.route("/v1/projects/<project_id>/albums/<album_id>", methods=["GET"])
@controller.api_controller()
def v1_get_album(project_id, album_id):
    include = Parser.str_csv_to_list(request.args.get("include"), [])
    ProjectService().get(project_id)
    if album_id.startswith(ALBUM_PREFIX):
        album_name = album_id[len(ALBUM_PREFIX):]
        album = AlbumService().get_album_by_name(album_name, project_id, "PROJECT")
    else:
        album = AlbumService().get(album_id, project_id, "PROJECT")

    include.append("images")
    album_json = album.to_json_dict(ProjectAlbumsImageUrl(project_id, str(album.id)), include)
    return json_response(filter_attributes_from_json_dict(album_json, "owner_type", "owner_id"))


@flask.route("/v1/projects/<project_id>/albums", methods=["GET"])
@controller.api_controller()
def v1_get_all_album(project_id):
    filters = Parser.str_csv_to_list(request.args.get("filters"))
    include = Parser.str_csv_to_list(request.args.get("include"))
    start = Parser.int(request.args.get("start"), 0)
    count = Parser.int(request.args.get("count"), 5)
    ProjectService().get(project_id)
    page = AlbumService().get_page(start=start, count=count, owner_id=project_id, owner_type="PROJECT")
    return json_response(page, include)

@flask.route("/v1/projects/<project_id>/albums/<album_id>", methods=["POST"])
@controller.api_controller()
def v1_add_image_url(project_id, album_id):
    ProjectService().get(project_id)
    album = AlbumService().get(album_id, project_id, "PROJECT")
    image_url_context = ProjectAlbumsImageUrl(project_id, album_id)
    try:
        album_dict = request.get_json(force=True)
        album_dict["owner_id"] = album_id
        album_dict["owner_type"] = "ALBUM"
        images = Images().from_json_dict(album_dict, image_url_context)
    except NotFoundException as e:
        raise e
    except:
        raise InvalidInputException("Error in processing input json")
    images = ImageService().add(images)
    images_json = images.to_json_dict(image_url_context)
    if images_json["owner_type"] != ALBUM_IMAGES_OWNER_TYPE:
        raise InvalidStateException("Expecting owner_type as album but found " + images_json["owner_type"])
    images_json["album"] = {"id": images_json["owner_id"]}
    if "owner_type" in images_json: del images_json["owner_type"]
    if "owner_id" in images_json: del images_json["owner_id"]
    return json_response(images_json)


