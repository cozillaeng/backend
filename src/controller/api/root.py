from io import BufferedReader
import time
import random

from flask import request, Response

from app import flask, controller
from client.aws import AmazonS3Client
from conf import Conf
from exception import InvalidInputException
from util.common import Parser
from util.json_util import dump_json


@flask.route('/')
@flask.route('/v1')
@controller.api_controller()
def home_page():
    pass


@flask.route("/v1/tmp/images", methods=["POST"])
@controller.api_controller()
def upload_image():
    valid_content_types = ["image/jpeg", "image/png"]
    content_type = Parser.str_lower(request.headers.get("Content-Type"))
    if not content_type or content_type not in valid_content_types:
        raise InvalidInputException(
            "Empty or invalid value for header 'Content-Type'. Must be in '" + ", ".join(valid_content_types) + "'")

    if content_type == "image/jpeg":
        image_ext = "jpg"
    elif content_type == "image/png":
        image_ext = "png"
    else:
        image_ext = "tmp"

    image_data = request.data
    if not image_data:
        raise InvalidInputException("Uploaded data is empty")
    image_name = str(int(time.time() * 1000000)) + str(random.randint(10000, 99999))

    s3_conf = Conf.get("amazon_s3")
    s3_client = AmazonS3Client(s3_conf["key_id"], s3_conf["key_secret"])
    image_bucket = s3_client.get_bucket(s3_conf["bucket_images"])
    file_name = image_name + "." + image_ext
    key = "/tmp/images/" + file_name
    s3_client.put_str(image_bucket, key=key, data=image_data, headers={"Content-Type": content_type})
    return Response(response=dump_json({"image_name": file_name}))


@flask.route("/v1/tmp/images/<path:filename>", methods=["GET"])
@controller.api_controller()
def download_image(filename):
    s3_conf = Conf.get("amazon_s3")
    s3_client = AmazonS3Client(s3_conf["key_id"], s3_conf["key_secret"])
    image_bucket = s3_client.get_bucket(s3_conf["bucket_images"])
    key = "/tmp/images/" + filename
    data = s3_client.download_as_str(image_bucket, key=key)
    if filename.endswith("jpg"):
        content_type = "image/jpg"
    elif filename.endswith("png"):
        content_type = "image/png"
    else:
        content_type = "attachment; filename=" + filename
    return Response(response=data, headers={"Content-Type": content_type})

# @flask.route('/api1')
# @controller.api_controller()
# def api1():
#     url = "http://" + Conf.get("app_host") + "/api2"
#     logging.info("API Url: %s" % url)
#     r = requests.get(url, timeout=1000)
#     r.raise_for_status()
#     return Response(response=r.text, mimetype=MIME_TYPE_JSON)
#
#
# @flask.route('/api2')
# @controller.api_controller()
# def api2():
#     url = "http://" + Conf.get("app_host") + "/api3"
#     logging.info("API Url: %s" % url)
#     r = requests.get(url, timeout=1000)
#     r.raise_for_status()
#     return Response(response=r.text, mimetype=MIME_TYPE_JSON)
#
#
# @flask.route('/api3')
# @controller.api_controller()
# def api3():
#     return json.dumps({"message": "Success API3"})
