import logging

import ast
from flask import request, url_for

from app import flask, controller
from client.database import Db
from exception import NotFoundException, InvalidInputException
from model.designer import Designer
from model.project import Project
from model.flat import Flat
from services.designer import DesignerService
from services.flat import FlatService
from util.json_util import json_response
from util.api_util import ApiFilters
from util.common import Parser



@flask.route("/v1/flats", methods=["GET"])
@controller.api_controller()
def v1_get_all_flats():
    filters = ApiFilters(request.args.get("filters"))
    include = Parser.str_csv_to_list(request.args.get("include"))

    start = Parser.int(request.args.get("start"), 0)
    count = Parser.int(request.args.get("count"), 50)

    page = FlatService().get_page(start=start, count=count)
    return json_response(page, include)

@flask.route("/v1/flats/<int:id>", methods=["GET"])
@controller.api_controller()
def v1_get_flat(id):
    filters = ApiFilters(request.args.get("filters"))
    include = Parser.str_csv_to_list(request.args.get("include"))

    start = Parser.int(request.args.get("start"), 0)
    count = Parser.int(request.args.get("count"), 50)

    with Db.get() as db:
        flat = Flat.get(db, id)
        if not flat:
            raise NotFoundException()
    return json_response(flat, include=include)


@flask.route("/v1/flats", methods=["POST"])
@controller.api_controller()
def v1_create_flat():
    try:
        flat_dict = request.get_json(force=True)
        # flat_dict = ast.literal_eval(flat_dict)
        flat = Flat().from_json_dict(flat_dict)
    except:
        raise InvalidInputException("Error in processing input json")

    flat = FlatService().add(flat)
    return json_response(flat)

@flask.route("/v1/flats/<id>", methods=["PUT"])
@controller.api_controller()
def v1_update_flat(id):
    try:
        flat_dict = request.get_json(force=True)
        flat = Flat().from_json_dict(flat_dict)
    except:
        raise InvalidInputException("Error in processing input json")

    flat = FlatService().update(id, flat)

    return json_response(flat)
