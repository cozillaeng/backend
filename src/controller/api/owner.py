import logging
import ast
from flask import request, url_for

from app import flask, controller
from client.database import Db
from exception import NotFoundException, InvalidInputException
from model.designer import Designer
from model.project import Project
from model.owner import Owner
from services.designer import DesignerService
from services.owner import OwnerService
from util.json_util import json_response
from util.api_util import ApiFilters
from util.common import Parser



@flask.route("/v1/owners", methods=["GET"])
@controller.api_controller()
def v1_get_all_owners():
    filters = ApiFilters(request.args.get("filters"))
    include = Parser.str_csv_to_list(request.args.get("include"))

    start = Parser.int(request.args.get("start"), 0)
    count = Parser.int(request.args.get("count"), 50)

    page = OwnerService().get_page(start=start, count=count)
    return json_response(page, include)

@flask.route("/v1/owners/<int:id>", methods=["GET"])
@controller.api_controller()
def v1_get_owner(id):
    filters = ApiFilters(request.args.get("filters"))
    include = Parser.str_csv_to_list(request.args.get("include"))

    start = Parser.int(request.args.get("start"), 0)
    count = Parser.int(request.args.get("count"), 50)

    with Db.get() as db:
        owner = Owner.get(db, id)
        if not owner:
            raise NotFoundException()
    return json_response(owner, include=include)


@flask.route("/v1/owners", methods=["POST"])
@controller.api_controller()
def v1_create_owner():
    try:
        owner_dict = request.get_json(force=True)
        # owner_dict = ast.literal_eval(owner_dict)
        owner = Owner().from_json_dict(owner_dict)
    except:
        raise InvalidInputException("Error in processing input json")

    owner = OwnerService().add(owner)
    return json_response(owner)

@flask.route("/v1/owners/<id>", methods=["PUT"])
@controller.api_controller()
def v1_update_owner(id):
    try:
        owner_dict = request.get_json(force=True)
        owner = Owner().from_json_dict(owner_dict)
    except:
        raise InvalidInputException("Error in processing input json")

    owner = OwnerService().update(id, owner)

    return json_response(owner)
