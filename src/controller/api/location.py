import logging

from flask import request, url_for

from app import flask, controller
from client.database import Db
from exception import NotFoundException, InvalidInputException
from model.designer import Designer
from model.project import Project
from model.location import Location
from services.designer import DesignerService
from services.location import LocationService
from util.json_util import json_response
from util.api_util import ApiFilters
from util.common import Parser



@flask.route("/v1/locations", methods=["GET"])
@controller.api_controller()
def v1_get_all_locations():
    filters = ApiFilters(request.args.get("filters"))
    include = Parser.str_csv_to_list(request.args.get("include"))

    start = Parser.int(request.args.get("start"), 0)
    count = Parser.int(request.args.get("count"), 50)
    city_id = Parser.int(filters.get("city_id"))
    is_enabled = Parser.bool(filters.get("is_enabled"))


    page = LocationService().get_page(start=start, count=count, city_id=city_id, is_enabled=is_enabled)
    return json_response(page, include)

@flask.route("/v1/locations/<int:id>", methods=["GET"])
@controller.api_controller()
def v1_get_location(id):
    filters = ApiFilters(request.args.get("filters"))
    include = Parser.str_csv_to_list(request.args.get("include"))

    start = Parser.int(request.args.get("start"), 0)
    count = Parser.int(request.args.get("count"), 50)

    with Db.get() as db:
        location = Location.get(db, id)
        if not location:
            raise NotFoundException()
    return json_response(location, include=include)


@flask.route("/v1/locations", methods=["POST"])
@controller.api_controller()
def v1_create_location():
    try:
        location_dict = request.get_json(force=True)
        location = Location().from_json_dict(location_dict)
    except:
        raise InvalidInputException("Error in processing input json")

    location = LocationService().add(location)
    return json_response(location)

@flask.route("/v1/locations/<id>", methods=["PUT"])
@controller.api_controller()
def v1_update_location(id):
    try:
        location_dict = request.get_json(force=True)
        location = Location().from_json_dict(location_dict)
    except:
        raise InvalidInputException("Error in processing input json")

    location = LocationService().update(id, location)

    return json_response(location)


