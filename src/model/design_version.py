__author__ = 'ramakrishnag'

import datetime
from sqlalchemy import Column, String, DateTime, Integer, Float
from model.common import ModelBase
from util.common import DictUtil
from model.design import Design
from model.project import Project


class DesignVersion(ModelBase):
    __tablename__ = "design_versions"

    design_id = Column(Integer,nullable=False)
    version_id = Column(Integer,nullable=False)
    design_data = Column(String)
    created_at = Column(DateTime, nullable=False, default=datetime.datetime.now())
    
    def to_dict(self):
        obj_dict = super(self.__class__, self).to_dict()
        if self.design_id:
            obj_dict["design_id"] = self.design_id
        if self.version_id:
            obj_dict["version_id"] = self.version_id
        if self.design_data:
            obj_dict["design_data"] = self.design_data
        if self.created_at:
            obj_dict["created_at"] = self.created_at
        
        return obj_dict

    def from_dict(self, obj_dict):
        super(self.__class__, self).from_dict(obj_dict)
        if "design_id" in obj_dict:
            self.design_id = obj_dict["design_id"]
        if "version_id" in obj_dict:
            self.version_id = obj_dict["version_id"]
        if "design_data" in obj_dict:
            self.design_data = obj_dict["design_data"]
        
        return self

    def to_json_dict(self, include=None):
        if not include:
            include = []
        obj_dict = self.to_dict()

        return obj_dict

    def from_json_dict(self, obj_dict):
        self.from_dict(obj_dict)
        
        return self
