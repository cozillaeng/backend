from sqlalchemy import Column, String, Integer, ForeignKey, Boolean
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

from util.common import DictUtil


class JSONSerializable(object):
    def to_json_dict(self, include=None):
        return {}


class HttpResponse(JSONSerializable):
    def __init__(self, code=0, message="Success"):
        self.code = code
        self.message = message

    def to_json_dict(self, include=None):
        return {
            "_code": self.code,
            "_message": self.message,
            "_link": "http://bouncer.livspace.com/error_codes/" + str(self.code)
        }


class Page(JSONSerializable):
    def __init__(self):
        self.items = None
        self.count = 0
        self.total_count = 0

        self.is_prev_page = False
        self.prev_page_start = None
        self.prev_page_count = None
        self.prev_page_url = None

        self.is_next_page = False
        self.next_page_start = None
        self.next_page_count = None
        self.next_page_url = None

    def gen_page_data(self, start, count):
        if start > 0:
            self.is_prev_page = True
            if start >= count:
                self.prev_page_start = start - count
                self.prev_page_count = count
            else:
                self.prev_page_start = 0
                self.prev_page_count = start
        else:
            self.is_prev_page = False

        if len(self.items) > count:
            self.is_next_page = True
            if start + count <= self.total_count - count:
                self.next_page_start = start + count
                self.next_page_count = count
            else:
                self.next_page_start = start + count
                self.next_page_count = self.total_count - self.next_page_start
            self.items = self.items[:-1]
        else:
            self.is_next_page = False
        self.count = len(self.items)

    def gen_page_urls(self, baseurl, params=None):
        clean_params = DictUtil.delete_null_values(params)
        if not clean_params: clean_params = {}

        if self.is_prev_page:
            clean_params["start"] = self.prev_page_start
            clean_params["count"] = self.prev_page_count
            self.prev_page_url = "%s?%s" % (baseurl, DictUtil.join(clean_params))

        if self.is_next_page:
            clean_params["start"] = self.next_page_start
            clean_params["count"] = self.next_page_count
            self.next_page_url = "%s?%s" % (baseurl, DictUtil.join(clean_params))

    def to_json_dict(self, include=None):
        if not include: include = []

        json_dict = super(self.__class__, self).to_json_dict(include)
        json_dict["count"] = self.count
        json_dict["items"] = []
        for item in self.items:
            json_dict["items"].append(item.to_json_dict(include))
        json_dict["total_count"] = self.total_count
        json_dict["is_prev"] = self.is_prev_page
        if self.is_prev_page:
            json_dict["_prev"] = self.prev_page_url
            # json_dict["prev_page_start"] = self.prev_page_start
            # json_dict["prev_page_count"] = self.prev_page_count
        json_dict["is_next"] = self.is_next_page
        if self.is_next_page:
            json_dict["_next"] = self.next_page_url
            # json_dict["next_page_start"] = self.next_page_start
            # json_dict["next_page_count"] = self.next_page_count
        return json_dict


class Model(JSONSerializable):
    id = Column(Integer, primary_key=True, nullable=False)

    @classmethod
    def _query(cls, query, start=None, count=None, order_by=None):
        if start:
            query = query.offset(start)
        if count:
            query = query.limit(count)
        if order_by is not None:
            query = query.order_by(order_by)
        return query

    @classmethod
    def get_all(cls, db, order_by=None, start=None, count=None):
        return cls._query(db.query(cls), order_by, start, count).all()

    @classmethod
    def get(cls, db, id):
        return db.query(cls).get(id)

    @classmethod
    def delete(cls, db, id):
        db.query(cls).filter(cls.id == id).delete()

    def from_dict(self, obj_dict):
        if "id" in obj_dict:
            self.id = obj_dict["id"]
        return self

    def to_dict(self):
        if self.id:
            return {"id": self.id}
        else:
            return {}

    def to_json_dict(self, include=None):
        return self.to_dict()


ModelBase = declarative_base(cls=Model)

