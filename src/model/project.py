import datetime
from sqlalchemy import Column, String, Boolean, Enum, DateTime, Integer, ForeignKey, Text, Float
from sqlalchemy.orm import relationship
from model.designer import Designer
from model.customer import Customer
from model.common import ModelBase
from model.city import City

class Project(ModelBase):
    __tablename__ = "projects"

    designer_id = Column(Integer, ForeignKey(Designer.id), nullable=False)
    customer_id = Column(Integer, ForeignKey(Customer.id))
    cover_image_filename = Column(String(50), nullable=False)
    layout_filename = Column(String(50), nullable=False)
    display_name = Column(String(100), nullable=False)
    start_date = Column(String(100))
    status = Column(String(50), nullable=False)

    property_name = Column(String(100))
    preferences = Column(Text)
    budget_min = Column(Float)
    budget_max = Column(Float)

    address = Column(String(100))
    city_id = Column(Integer, ForeignKey(City.id), nullable=False)
    pincode = Column(String(10))
    occupancy_date = Column(DateTime, nullable=False)
    timeline_date = Column(DateTime, nullable=False)
    commencement_date = Column(DateTime, nullable=False)
    next_interaction_date = Column(DateTime, nullable=False)
    last_interaction_date = Column(DateTime, nullable=False)
    mom_link = Column(String(100))

    city = relationship(City, lazy="joined")
    designer = relationship(Designer, lazy="joined")
    customer = relationship(Customer, lazy="joined")


    created_at = Column(DateTime, nullable=False, default=datetime.datetime.now())
    updated_at = Column(DateTime, nullable=False, default=datetime.datetime.now())
    created_by_id = Column(Integer, nullable=False)
    updated_by_id = Column(Integer, nullable=False)

    def to_dict(self):
        obj_dict = super(self.__class__, self).to_dict()
        obj_dict["designer_id"] = self.designer_id
        obj_dict["cover_image_filename"] = self.cover_image_filename
        obj_dict["layout_filename"] = self.layout_filename
        obj_dict["display_name"] = self.display_name
        obj_dict["start_date"] = self.start_date
        obj_dict["status"] = self.status
        obj_dict["customer_id"] = self.customer_id
        obj_dict["property_name"] = self.property_name
        obj_dict["preferences"] = self.preferences
        obj_dict["budget_min"] = self.budget_min
        obj_dict["budget_max"] = self.budget_max

        obj_dict["address"] = self.address
        obj_dict["city_id"] = self.city_id
        obj_dict["pincode"] = self.pincode
        obj_dict["occupancy_date"] = self.occupancy_date
        obj_dict["timeline_date"] = self.timeline_date
        obj_dict["commencement_date"] = self.commencement_date
        obj_dict["next_interaction_date"] = self.next_interaction_date
        obj_dict["last_interaction_date"] = self.last_interaction_date
        obj_dict["mom_link"] = self.mom_link



        obj_dict["created_at"] = self.created_at
        obj_dict["updated_at"] = self.updated_at
        obj_dict["created_by_id"] = self.created_by_id
        obj_dict["updated_by_id"] = self.updated_by_id
        return obj_dict

    def from_dict(self, obj_dict):
        super(self.__class__, self).from_dict(obj_dict)
        if "designer_id" in obj_dict:
            self.designer_id = obj_dict["designer_id"]
        if "cover_image_filename" in obj_dict:
            self.cover_image_filename = obj_dict["cover_image_filename"]
        if "layout_filename" in obj_dict:
            self.layout_filename = obj_dict["layout_filename"]
        if "display_name" in obj_dict:
            self.display_name = obj_dict["display_name"]
        if "start_date" in obj_dict:
            self.start_date = obj_dict["start_date"]
        if "status" in obj_dict:
            self.status = obj_dict["status"]
        if "customer_id" in obj_dict:
            self.customer_id = obj_dict["customer_id"]
        if "property_name"  in obj_dict:
            self.property_name = obj_dict["property_name"]
        if "preferences"  in obj_dict:
            self.preferences = obj_dict["preferences"]
        if "budget_min"  in obj_dict:
            self.budget_min = obj_dict["budget_min"]
        if "budget_max"  in obj_dict:
            self.budget_max = obj_dict["budget_max"]

        if "address" in obj_dict:
            self.address = obj_dict["address"]
        if "city_id" in obj_dict:
            self.city_id = obj_dict["city_id"]
        if "pincode" in obj_dict:
            self.pincode = obj_dict["pincode"]
        if "occupancy_date" in obj_dict:
            self.occupancy_date = obj_dict["occupancy_date"]
        if "timeline_date" in obj_dict:
            self.timeline_date = obj_dict["timeline_date"]
        if "commencement_date" in obj_dict:
            self.commencement_date = obj_dict["commencement_date"]
        if "next_interaction_date" in obj_dict:
            self.next_interaction_date = obj_dict["next_interaction_date"]
        if "last_interaction_date" in obj_dict:
            self.last_interaction_date = obj_dict["last_interaction_date"]
        if "mom_link" in obj_dict:
            self.mom_link = obj_dict["mom_link"]

        if "created_at" in obj_dict:
            self.created_at = obj_dict["created_at"]
        if "updated_at" in obj_dict:
            self.updated_at = obj_dict["updated_at"]
        if "created_by_id" in obj_dict:
            self.created_by_id = obj_dict["created_by_id"]
        if "updated_by_id" in obj_dict:
            self.updated_by_id = obj_dict["updated_by_id"]
        return self

    def from_json_dict(self, obj_dict):
        self.from_dict(obj_dict)
        if "designer" in obj_dict:
            self.designer_id = Designer().from_dict(obj_dict["designer"]).id
        if "customer" in obj_dict:
            self.customer_id = Customer().from_dict(obj_dict["customer"]).id
        if "city" in obj_dict:
            self.city_id = City().from_dict(obj_dict["city"]).id
        return self

    def to_json_dict(self, include=None):
        if not include: include = []

        obj_dict = self.to_dict()

        if "designer" in include and self.designer:
            obj_dict["designer"] = self.designer.to_dict()
        else:
            obj_dict["designer"] = {"id": self.designer_id}
        if "customer" in include and self.customer:
            obj_dict["customer"] = self.customer.to_dict()
        else:
            obj_dict["customer"] = {"id": self.customer_id}
        if "city" in include and self.city:
            obj_dict["city"] = self.city.to_dict()
        else:
            obj_dict["city"] = {"id": self.city_id}

        obj_dict["created_at"] = self.created_at
        obj_dict["updated_at"] = self.updated_at
        obj_dict["created_by_id"] = self.created_by_id
        obj_dict["updated_by_id"] = self.updated_by_id

        return obj_dict

    @classmethod
    def get_all(cls, db, start=0, count=25, designer_id=None, order_by=None):
        query = db.query(cls)
        if designer_id is not None:
            query = query.filter(cls.designer_id == designer_id)
        if not order_by:
            order_by = cls.id.desc()
        return query.order_by(order_by).offset(start).limit(count).all()

    @classmethod
    def get_all_count(cls, db, designer_id=None):
        query = db.query(cls)
        if designer_id is not None:
            query = query.filter(cls.designer_id == designer_id)
        return query.count()

    @classmethod
    def get_by_designer_id(cls, db, designer_id):
        return db.query(cls).filter(cls.designer_id == designer_id).all()

