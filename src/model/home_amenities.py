import datetime
from sqlalchemy import Column, String, Boolean, Enum, DateTime, Integer, ForeignKey, Text, Float
from sqlalchemy.orm import relationship
from model.common import ModelBase
from model.designer import Designer
from model.common import ModelBase
from model.city import City



class HomeAmenities(ModelBase):
    __tablename__ = "home_amenities"

    sofa = Column(Integer)
    dinning_table = Column(Integer)
    television = Column(Integer)
    dth = Column(Integer)
    refrigerator = Column(Integer)
    washing_machine = Column(Integer)
    bed = Column(Integer)
    modular_kitchen = Column(Boolean, default= False)
    wifi = Column(Integer)
    gas_stove = Column(Integer)
    crockery_set = Column(Integer)
    wardrobes = Column(Integer)
    study_table = Column(Integer)
    chair = Column(Integer)
    center_table = Column(Integer)
    balcony = Column(Boolean, default= False)
    attached_bathroom = Column(Boolean, default= False)
    western_toilet = Column(Boolean, default= False)
    geyser = Column(Integer)
    water_purifier = Column(Integer)
    iron = Column(Integer)
    ac = Column(Integer)
    microwave = Column(Integer)
    shoe_rack = Column(Integer)
    side_table = Column(Integer)

    created_at = Column(DateTime, nullable=False, default=datetime.datetime.now())
    updated_at = Column(DateTime, nullable=False, default=datetime.datetime.now())
    created_by_id = Column(Integer, nullable=False)
    updated_by_id = Column(Integer, nullable=False)

    def to_dict(self):
        obj_dict = super(self.__class__, self).to_dict()
        if self.sofa:
            obj_dict["sofa"] = self.sofa
        if self.dinning_table:
            obj_dict["dinning_table"] = self.dinning_table
        if self.television:
            obj_dict["television"] = self.television
        if self.dth:
            obj_dict["dth"] = self.dth
        if self.refrigerator:
            obj_dict["refrigerator"] = self.refrigerator
        if self.washing_machine:
            obj_dict["washing_machine"] = self.washing_machine
        if self.bed:
            obj_dict["bed"] = self.bed
        if self.modular_kitchen:
            obj_dict["modular_kitchen"] = self.modular_kitchen
        if self.wifi:
            obj_dict["wifi"] = self.wifi
        if self.gas_stove:
            obj_dict["gas_stove"] = self.gas_stove
        if self.crockery_set:
            obj_dict["crockery_set"] = self.crockery_set
        if self.wardrobes:
            obj_dict["wardrobes"] = self.wardrobes
        if self.study_table:
            obj_dict["study_table"] = self.study_table
        if self.chair:
            obj_dict["chair"] = self.chair
        if self.center_table:
            obj_dict["center_table"] = self.center_table
        if self.balcony:
            obj_dict["balcony"] = self.balcony
        if self.attached_bathroom:
            obj_dict["attached_bathroom"] = self.attached_bathroom
        if self.western_toilet:
            obj_dict["western_toilet"] = self.western_toilet
        if self.geyser:
            obj_dict["geyser"] = self.geyser
        if self.water_purifier:
            obj_dict["water_purifier"] = self.water_purifier
        if self.iron:
            obj_dict["iron"] = self.iron
        if self.ac:
            obj_dict["ac"] = self.ac
        if self.microwave:
            obj_dict["microwave"] = self.microwave
        if self.shoe_rack:
            obj_dict["shoe_rack"] = self.shoe_rack
        if self.side_table:
            obj_dict["side_table"] = self.side_table

        if self.created_at:
            obj_dict["created_at"] = self.created_at
        if self.updated_at:
            obj_dict["updated_at"] = self.updated_at
        if self.created_by_id:
            obj_dict["created_by_id"] = self.created_by_id
        if self.updated_by_id:
            obj_dict["updated_by_id"] = self.updated_by_id
        return obj_dict

    def from_dict(self, obj_dict):
        super(self.__class__, self).from_dict(obj_dict)

        if "sofa" in obj_dict:
            self.sofa = obj_dict["sofa"]
        if "dinning_table" in obj_dict:
            self.dinning_table = obj_dict["dinning_table"]
        if "television" in obj_dict:
            self.television = obj_dict["television"]
        if "dth" in obj_dict:
            self.dth = obj_dict["dth"]
        if "refrigerator" in obj_dict:
            self.refrigerator = obj_dict["refrigerator"]
        if "washing_machine" in obj_dict:
            self.washing_machine = obj_dict["washing_machine"]
        if "bed" in obj_dict:
            self.bed = obj_dict["bed"]
        if "modular_kitchen" in obj_dict:
            self.modular_kitchen = obj_dict["modular_kitchen"]
        if "wifi" in obj_dict:
            self.wifi = obj_dict["wifi"]
        if "gas_stove" in obj_dict:
            self.gas_stove = obj_dict["gas_stove"]
        if "crockery_set" in obj_dict:
            self.crockery_set = obj_dict["crockery_set"]
        if "wardrobes" in obj_dict:
            self.wardrobes = obj_dict["wardrobes"]
        if "study_table" in obj_dict:
            self.study_table = obj_dict["study_table"]
        if "chair" in obj_dict:
            self.chair = obj_dict["chair"]
        if "center_table" in obj_dict:
            self.center_table = obj_dict["center_table"]
        if "balcony" in obj_dict:
            self.balcony = obj_dict["balcony"]
        if "attached_bathroom" in obj_dict:
            self.attached_bathroom = obj_dict["attached_bathroom"]
        if "western_toilet" in obj_dict:
            self.western_toilet = obj_dict["western_toilet"]
        if "geyser" in obj_dict:
            self.geyser = obj_dict["geyser"]
        if "water_purifier" in obj_dict:
            self.water_purifier = obj_dict["water_purifier"]
        if "iron" in obj_dict:
            self.iron = obj_dict["iron"]
        if "ac" in obj_dict:
            self.ac = obj_dict["ac"]
        if "microwave" in obj_dict:
            self.microwave = obj_dict["microwave"]
        if "shoe_rack" in obj_dict:
            self.shoe_rack = obj_dict["shoe_rack"]
        if "side_table" in obj_dict:
            self.side_table = obj_dict["side_table"]

        if "created_at" in obj_dict:
            self.created_at = obj_dict["created_at"]
        if "updated_at" in obj_dict:
            self.updated_at = obj_dict["updated_at"]
        if "created_by_id" in obj_dict:
            self.created_by_id = obj_dict["created_by_id"]
        if "updated_by_id" in obj_dict:
            self.updated_by_id = obj_dict["updated_by_id"]
        return self

    def from_json_dict(self, obj_dict):
        self.from_dict(obj_dict)
        return self

    def to_json_dict(self, include=None):
        if not include: include = []

        obj_dict = self.to_dict()

        obj_dict["created_at"] = self.created_at
        obj_dict["updated_at"] = self.updated_at
        obj_dict["created_by_id"] = self.created_by_id
        obj_dict["updated_by_id"] = self.updated_by_id

        return obj_dict

    @classmethod
    def get_all(cls, db, start=0, count=25, order_by=None):
        query = db.query(cls)
        if not order_by:
            order_by = cls.id.desc()
        return query.order_by(order_by).offset(start).limit(count).all()

    @classmethod
    def get_all_count(cls, db):
        query = db.query(cls)
        return query.count()
