import datetime

from sqlalchemy import String, Integer, DateTime, ForeignKey, Boolean
from sqlalchemy import Column
from sqlalchemy.orm import relationship, reconstructor

from model.common import ModelBase
from util.common import Parser


class ProductCategory(ModelBase):
    __tablename__ = "product_categories"

    name = Column(String(50), nullable=False)
    display_name = Column(String(50), nullable=False)
    is_enabled = Column(Boolean, nullable=False, default=True)
    created_at = Column(DateTime, nullable=False, default=datetime.datetime.now())
    updated_at = Column(DateTime, nullable=False, default=datetime.datetime.now())
    created_by_id = Column(Integer, nullable=False)
    updated_by_id = Column(Integer, nullable=False)

    # def __init__(self):
    #     self.parent_category_id = None
    #     self.level = 0
    #     self.children = []

    # This will be called by SQLAlchemy as constructor
    # @reconstructor
    # def orm_init(self):
    #     self.parent_category_id = None
    #     self.level = 0
    #     self.children = []

    def to_dict(self):
        obj_dict = super(self.__class__, self).to_dict()
        obj_dict["name"] = self.name
        obj_dict["display_name"] = self.display_name
        obj_dict["is_enabled"] = self.is_enabled
        if hasattr(self, "parent_category_id"):
            obj_dict["parent_category_id"] = self.parent_category_id
        if hasattr(self, "level"):
            obj_dict["level"] = self.level

        obj_dict["created_at"] = self.created_at
        obj_dict["updated_at"] = self.updated_at
        obj_dict["created_by_id"] = self.created_by_id
        obj_dict["updated_by_id"] = self.updated_by_id
        return obj_dict

    def from_dict(self, obj_dict):
        if "name" in obj_dict:
            self.name = Parser.str_lower(obj_dict["name"])
        if "display_name" in obj_dict:
            self.display_name = Parser.str(obj_dict["display_name"])
        if "parent_category_id" in obj_dict:
            self.parent_category_id = Parser.int(obj_dict["parent_category_id"])
        if "level" in obj_dict:
            self.level = Parser.int(obj_dict["level"])
        if "is_enabled" in obj_dict:
            self.is_enabled = Parser.bool(obj_dict["is_enabled"], default=False)

        if "created_at" in obj_dict:
            self.created_at = Parser.datetime(obj_dict["created_at"])
        if "updated_at" in obj_dict:
            self.updated_at = Parser.datetime(obj_dict["updated_at"])
        if "created_by_id" in obj_dict:
            self.created_by_id = Parser.int(obj_dict["created_by_id"])
        if "updated_by_id" in obj_dict:
            self.updated_by_id = Parser.int(obj_dict["updated_by_id"])
        return self

    def to_json_dict(self, include=None):
        if include is None: include = []
        obj_dict = self.to_dict()

        if hasattr(self, "children") and self.children:
            obj_dict["children"] = []
            for child in self.children:
                obj_dict["children"].append(child.to_json_dict())

        # if self.parent_category_id:
        #     if "parent_category" in include:
        #         obj_dict["parent_category"] = self.parent_category.to_dict()
        #     else:
        #         obj_dict["parent_category"] = {"id": self.parent_category_id, "_loaded": False, "_link": ""}
        #     if "parent_category_id" in obj_dict: del obj_dict["parent_category_id"]

        return obj_dict

    @classmethod
    def get_all(cls, db, is_enabled=None):
        query = db.query(cls)
        if is_enabled is not None:
            query = query.filter(cls.is_enabled == is_enabled)
        return query.all()

    @classmethod
    def get_by_ids(cls, db, ids):
        return db.query(cls).filter(cls.id.in_(ids)).all()


class ProductCategoryChild(ModelBase):
    __tablename__ = "product_category_children"

    category_id = Column(Integer, nullable=False)
    child_category_id = Column(Integer, nullable=False)
    # child_level = Column(Integer, nullable=False)
    created_at = Column(DateTime, nullable=False)
    created_by_id = Column(DateTime, nullable=False)

    @classmethod
    def get_all_by_category(cls, db, category_id):
        return db.query(cls).filter(cls.category_id == category_id).all()


class ExternalCategory(ModelBase):
    __tablename__ = "external_categories"

    external_id = Column(Integer, nullable=False)
    display_name = Column(String(100), nullable=False)
    source = Column(Integer, nullable=False)


class CatalogueProvider(ModelBase):
    __tablename__ = "catalogue_providers"

    name = Column(String(100), nullable=False)
    display_name = Column(String(100), nullable=False)
    created_at = Column(DateTime, nullable=False, default=datetime.datetime.now())
    updated_at = Column(DateTime, nullable=False, default=datetime.datetime.now())
    created_by_id = Column(String(100), nullable=False)
    updated_by_id = Column(String(100), nullable=False)
