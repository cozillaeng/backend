import datetime
from sqlalchemy import Column, String, Boolean, Enum, DateTime, Integer, ForeignKey, Text
from sqlalchemy.orm import relationship
from model.common import ModelBase
from model.city import City


class Designer(ModelBase):
    __tablename__ = "designers"

    website_designer_id = Column(Integer, nullable=False)
    first_name = Column(String(50), nullable=False)
    last_name = Column(String(50), nullable=False)
    display_name = Column(String(100), nullable=False)
    email = Column(String(100), nullable=False)
    phone_primary = Column(String(20))
    phone_secondary = Column(String(20))
    address = Column(String(100))
    city_id = Column(Integer, ForeignKey(City.id), nullable=False)
    pincode = Column(String(10))
    about_me = Column(Text)
    source = Column(Enum("LSMKP", "DWLL", "DEZIGNUP", "IN_HOUSE"), nullable=False)
    is_approved = Column(Boolean, nullable=False, default=False)
    is_enabled = Column(Boolean, nullable=False, default=False)
    profile_image_filename = Column(String(100))

    city = relationship(City, lazy="joined")
    created_at = Column(DateTime, nullable=False, default=datetime.datetime.now())
    updated_at = Column(DateTime, nullable=False, default=datetime.datetime.now())
    created_by_id = Column(Integer, nullable=False)
    updated_by_id = Column(Integer, nullable=False)

    def to_dict(self):
        obj_dict = super(self.__class__, self).to_dict()
        if self.website_designer_id:
            obj_dict["website_designer_id"] = self.website_designer_id
        if self.first_name:
            obj_dict["first_name"] = self.first_name
        if self.last_name:
            obj_dict["last_name"] = self.last_name
        if self.display_name:
            obj_dict["display_name"] = self.display_name
        if self.email:
            obj_dict["email"] = self.email
        if self.phone_primary:
            obj_dict["phone_primary"] = self.phone_primary
        if self.phone_secondary:
            obj_dict["phone_secondary"] = self.phone_secondary
        if self.address:
            obj_dict["address"] = self.address
        if self.city_id:
            obj_dict["city_id"] = self.city_id
        if self.pincode:
            obj_dict["pincode"] = self.pincode
        if self.source:
            obj_dict["source"] = self.source
        if self.is_approved:
            obj_dict["is_approved"] = self.is_approved
        if self.is_enabled:
            obj_dict["is_enabled"] = self.is_enabled
        if self.profile_image_filename:
            obj_dict["profile_image_filename"] = self.profile_image_filename
        if self.about_me:
            obj_dict["about_me"] = self.about_me

        if self.created_at:
            obj_dict["created_at"] = self.created_at
        if self.updated_at:
            obj_dict["updated_at"] = self.updated_at
        if self.created_at:
            obj_dict["created_by_id"] = self.created_by_id
        if self.updated_at:
            obj_dict["updated_by_id"] = self.updated_by_id
        return obj_dict

    def from_dict(self, obj_dict):
        super(self.__class__, self).from_dict(obj_dict)
        if "website_designer_id" in obj_dict:
            self.website_designer_id = obj_dict["website_designer_id"]
        if "first_name" in obj_dict:
            self.first_name = obj_dict["first_name"]
        if "last_name" in obj_dict:
            self.last_name = obj_dict["last_name"]
        if "email" in obj_dict:
            self.email = obj_dict["email"]
        if "phone_primary" in obj_dict:
            self.phone_primary = obj_dict["phone_primary"]
        if "phone_secondary" in obj_dict:
            self.phone_secondary = obj_dict["phone_secondary"]
        if "address" in obj_dict:
            self.address = obj_dict["address"]
        if "city_id" in obj_dict:
            self.city_id = obj_dict["city_id"]
        if "pincode" in obj_dict:
            self.pincode = obj_dict["pincode"]
        if "about_me" in obj_dict:
            self.about_me = obj_dict["about_me"]
        if "source" in obj_dict:
            self.source = obj_dict["source"]
        if "is_approved" in obj_dict:
            self.is_approved = obj_dict["is_approved"]
        if "is_enabled" in obj_dict:
            self.is_enabled = obj_dict["is_enabled"]
        if "profile_image_filename" in obj_dict:
            self.profile_image_filename = obj_dict["profile_image_filename"]
        if "about_me" in obj_dict:
            self.about_me = obj_dict["about_me"]

        if "created_at" in obj_dict:
            self.created_at = obj_dict["created_at"]
        if "updated_at" in obj_dict:
            self.updated_at = obj_dict["updated_at"]
        if "created_by_id" in obj_dict:
            self.created_by_id = obj_dict["created_by_id"]
        if "updated_by_id" in obj_dict:
            self.updated_by_id = obj_dict["updated_by_id"]
        return self

    def from_json_dict(self, obj_dict):
        self.from_dict(obj_dict)
        if "city" in obj_dict:
            self.city_id = City().from_dict(obj_dict["city"]).id
        return self

    def to_json_dict(self, include=None):
        if not include: include = []

        obj_dict = self.to_dict()
        if self.profile_image_filename:
            obj_dict["profile_image_url"] = self.profile_image_filename;
            #    obj_dict["profile_image_url"] = "/images/designers/profile_images/" + self.profile_image_filename

        if "city" in include and self.city:
            obj_dict["city"] = self.city.to_dict()
        else:
            obj_dict["city"] = {"id": self.city_id}

        obj_dict["created_at"] = self.created_at
        obj_dict["updated_at"] = self.updated_at
        obj_dict["created_by_id"] = self.created_by_id
        obj_dict["updated_by_id"] = self.updated_by_id

        if "city_id" in obj_dict: del obj_dict["city_id"]
        return obj_dict

    @classmethod
    def get_all(cls, db, start=0, count=25, is_approved=None, source=None, order_by=None):
        query = db.query(cls)
        if is_approved is not None:
            query.filter(cls.is_approved == is_approved)
        if source:
            query.filter(cls.source == source)
        if not order_by:
            order_by = cls.id.desc()
        return query.order_by(order_by).offset(start).limit(count).all()

    @classmethod
    def get_all_count(cls, db, is_approved=None, source=None):
        query = db.query(cls)
        if is_approved is not None:
            query.filter(cls.is_approved == is_approved)
        if source:
            query.filter(cls.source == source)
        return query.count()

    @classmethod
    def get_by_website_designer_id(cls, db, id):
        return db.query(cls).filter(cls.website_designer_id == id).first()

    @classmethod
    def get_by_email(cls, db, email):
        return db.query(cls).filter(cls.email == email).first()
