from sqlalchemy import String, Integer, ForeignKey, Boolean
from sqlalchemy import Column
from sqlalchemy.orm import relationship

from model.common import ModelBase
from util.common import Parser
from model.product_component import ProductComponent
from model.product_sku import ProductSku

class Product(ModelBase):
    __tablename__ = "products"
    
    code = Column(String(256), nullable=False)
    product_components = relationship(ProductComponent, lazy="joined", backref="product",cascade="all, delete, delete-orphan")
    category_id = Column(Integer, ForeignKey("product_categories.id"), nullable=False)
    skus = relationship(ProductSku, lazy="subquery", backref="product",cascade="all, delete, delete-orphan")
    
    def to_dict(self):
        obj_dict = super(self.__class__, self).to_dict()
        if self.code:
            obj_dict["code"] = self.code
        if self.category_id:
            obj_dict["category_id"] = self.category_id

        return obj_dict

    def from_dict(self, obj_dict):
        if "code" in obj_dict:
            self.code = obj_dict["code"]
        if "category_id" in obj_dict:
            self.category_id = obj_dict["category_id"]
        
        return self

    def to_json_dict(self, include=None):
        if not include:
            include = []

        obj_dict = self.to_dict()

        if self.product_components:
            obj_dict["components"] = []
            for component in self.product_components:
                obj_dict["components"].append(component.to_json_dict())  

        if self.skus:
            for sku in self.skus:
                if sku.default_sku:
                    obj_dict["default_sku"] = sku.to_json_dict()
        
        return obj_dict

    def from_json_dict(self, obj_dict):
        self.from_dict(obj_dict)
