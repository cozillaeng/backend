from sqlalchemy import String, Integer, ForeignKey, Boolean, Float
from sqlalchemy import Column
from sqlalchemy.orm import relationship

from model.common import ModelBase
from model.product_category import ProductCategory
from util.common import Parser
from model.sku_provider_map import SkuProviderMap
from model.componentoption_sku_map import ComponentOptionSkuMap
#from model.product import Product


class ProductSku(ModelBase):
    name = "ProductSKU"

    __tablename__ = "sku"
    detail_page_filename = Column(String(256), nullable=False)
    display_name = Column(String(50), nullable=False)
    product_id = Column(Integer, ForeignKey("products.id"), nullable=False)
    top_view_image_filename = Column(String(256), nullable=False)
    cover_image_filename = Column(String(256), nullable=False)
    #category = relationship(ProductCategory, lazy="joined")
    liv_home_2d_filename = Column(String(256), nullable=True)
    liv_home_3d_filename = Column(String(256), nullable=True)
    #catalogue_provider_id = Column(Integer, nullable=False)
    texture_image_filename = Column(String(256))
    backed_texture_image_filename = Column(String(256))
    catalouge_image_filename = Column(String(256))

    providers = relationship(SkuProviderMap,  lazy="joined", backref="productcategory",cascade="all, delete, delete-orphan")
    component_options = relationship(ComponentOptionSkuMap,  lazy="joined", backref="sku",cascade="all, delete, delete-orphan")
    default_sku = Column(Boolean)
    width = Column(Float)
    height = Column(Float)
    
    #TODO URL should be configurable option
    images_url = "https://s3.amazonaws.com/livspace-launchpad-images-local/skus/"
    
    product_code = None
    price = None
    available = None
    sku_code = None
    is_placeholder = False

    def to_dict(self):
        obj_dict = {}
        obj_dict["id"] = self.id
        if self.detail_page_filename:
            obj_dict["detail_page_filename"] = self.images_url + str(self.id) + "/" + self.detail_page_filename
        obj_dict["display_name"] = self.display_name
        if self.top_view_image_filename:
            obj_dict["top_view_image_filename"] = self.images_url + str(self.id) + "/" + self.top_view_image_filename
        if self.cover_image_filename:
            obj_dict["cover_image_filename"] = self.images_url + str(self.id) + "/" + self.cover_image_filename
        #obj_dict["catalogue_provider_id"] = self.catalogue_provider_id
        obj_dict["product_code"] = "productcode_" + str(self.id)
        obj_dict["sku_code"] = "skucode_" + str(self.id)
        if self.catalouge_image_filename:
            obj_dict["catalouge_image_filename"] = self.images_url + str(self.id) + "/" + self.catalouge_image_filename
        if self.product_id:
            obj_dict["product_id"] = self.product_id
        if self.default_sku:
            obj_dict["default_sku"] = self.default_sku
        if self.width:
            obj_dict["width"] = self.width
        if self.height:
            obj_dict["height"] = self.height

        return obj_dict

    def from_dict(self, obj_dict):
        if "sku_code" in obj_dict:
            self.sku_code = Parser.str(obj_dict["sku_code"])
        if "detail_page_filename" in obj_dict:
            self.detail_page_url = Parser.int(obj_dict["detail_page_filename"])
        if "display_name" in obj_dict:
            self.display_name = Parser.str(obj_dict["display_name"])
        if "price" in obj_dict:
            self.price = Parser.bool(obj_dict["price"], default=False)
        if "available" in obj_dict:
            self.available = Parser.bool(obj_dict["available"], default=False)
        if "product_code" in obj_dict:
            self.product_code = Parser.str(obj_dict["product_code"])
        if "cover_image_filename" in obj_dict:
            self.cover_image_filename = Parser.bool(obj_dict["cover_image_filename"], default=False)
        if "top_view_image_filename" in obj_dict:
            self.top_view_image_filename = Parser.str(obj_dict["top_view_image_filename"])
        #if "catalogue_provider_id" in obj_dict:
        #    self.catalogue_provider_id = Parser.int(obj_dict["catalogue_provider_id"])
        if "catalouge_image_filename" in obj_dict:
            self.catalouge_image_filename = obj_dict["catalouge_image_filename"]
        if "product_id" in obj_dict:
            self.product_id = obj_dict["product_id"]
        if "default_sku" in obj_dict:
            self.default_sku = obj_dict["default_sku"]
        if "width" in obj_dict:
            self.width = obj_dict["width"]
        if "height" in obj_dict:
            self.height = obj_dict["height"]

        return self

    def to_json_dict(self, include=None):
        if not include:
            include = []

        obj_dict = self.to_dict()
        if "is_placeholder" not in include:
            obj_dict["is_placeholder"] = False
        else:
            obj_dict["is_placeholder"] = True

        if "is_placeholder" not in include:
            obj_dict["sku_code"] = self.sku_code
            obj_dict["product_code"] = self.product_code
            obj_dict["price"] = self.price
            obj_dict["available"] = self.available
            obj_dict["top_view_image_filename"] = self.top_view_image_filename

        #if "category" in include and self.category:
        #    obj_dict["category"] = self.category.to_dict()
        #else:
        #    obj_dict["category"] = {"id": self.category_id}

        obj_dict["liv_home"] = {}
        obj_dict["liv_home"]["model_2d"] = {}
        obj_dict["liv_home"]["model_3d"] = {}
        if self.liv_home_2d_filename:
            obj_dict["liv_home"]["model_2d"]["model_url"] = self.images_url + str(self.id) + "/" + self.liv_home_2d_filename
        if self.liv_home_3d_filename:
            obj_dict["liv_home"]["model_3d"]["model_url"] = self.images_url + str(self.id) + "/" + self.liv_home_3d_filename
        obj_dict["liv_home"]["model_3d"]["components"] = {}
        obj_dict["liv_home"]["model_3d"]["components"]["sh"] = {}
        if self.texture_image_filename:
            obj_dict["liv_home"]["model_3d"]["components"]["sh"]["texture_image"] = self.images_url + str(self.id) + "/" + self.texture_image_filename
        if self.backed_texture_image_filename:
            obj_dict["liv_home"]["model_3d"]["components"]["sh"]["backed_texture_image"] = self.images_url + str(self.id) + "/" + self.backed_texture_image_filename

        if self.providers:
            obj_dict["providers"] = []
            for provider in self.providers:
                obj_dict["providers"].append(provider.to_json_dict())
        if self.component_options:
            for option in self.component_options:
               if option.component_option:
                    component_option = option.component_option
                    if component_option.product_component:
                        product_component = component_option.product_component
                        obj_dict["liv_home"]["model_3d"]["components"][product_component.name] = option.to_json_dict()

        return obj_dict

    def from_json_dict(self, obj_dict):
        self.from_dict(obj_dict)
        if "category" in obj_dict:
            self.category_id = ProductCategory().from_dict(obj_dict["category"]).id

        self.liv_home_2d_filename = obj_dict["liv_home"]["model_2d"]["model_filename"]
        self.liv_home_3d_filename = obj_dict["liv_home"]["model_3d"]["model_filename"]
        self.texture_image_filename = obj_dict["liv_home"]["model_3d"]["components"]["sh"]["texture_image_filename"]
        self.backed_texture_image_filename = obj_dict["liv_home"]["model_3d"]["components"]["sh"]["backed_texture_image_filename"]

    @classmethod
    def get_all(cls, db, start=0, count=25, product_id=None, is_placeholder = False, order_by=None):
        query = db.query(cls)
        if product_id is not None:
            query = query.filter(cls.product_id == product_id)
        if not order_by:
            order_by = cls.id.desc()
        return query.order_by(order_by).offset(start).limit(count).all()

    @classmethod
    def get_all_count(cls, db, product_id=None):
        query = db.query(cls)
        if product_id is not None:
            query = query.filter(cls.product_id == product_id)
        return query.count()

