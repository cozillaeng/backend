import datetime
from sqlalchemy import Column, String, Boolean, Enum, DateTime, Integer, ForeignKey, Text, Float
from sqlalchemy.orm import relationship
from model.common import ModelBase
from model.designer import Designer
from model.common import ModelBase, City
from model.furniture_categories import FurnitureCategories
from model.owner import Owner



class OwnersFurniture(ModelBase):
    __tablename__ = "owners_furnitures"

    owner_id = Column(Integer)
    category_id = Column(Integer, ForeignKey(FurnitureCategories.id))
    date_of_purchase = Column(DateTime, nullable=False)
    cost = Column(Float, nullable=False)
    warranty = Column(Float)
    short_description = Column(String(50))
    long_description = Column(String(100))
    cover_image = Column(String(255))
    brand = Column(String(20))
    company = Column(String(20))
    invoice_copy_filename = Column(String(255))

    furniture_category = relationship(FurnitureCategories,foreign_keys='OwnersFurniture.category_id', lazy="joined")
    owner = relationship(Owner, foreign_kes='OwnersFurniture.owner_id', lazy="joined")

    created_at = Column(DateTime, nullable=False, default=datetime.datetime.now())
    updated_at = Column(DateTime, nullable=False, default=datetime.datetime.now())
    created_by_id = Column(Integer, nullable=False)
    updated_by_id = Column(Integer, nullable=False)

    def to_dict(self):
        obj_dict = super(self.__class__, self).to_dict()
        if self.owner_id:
            obj_dict["owner_id"] = self.owner_id
        if self.category_id:
            obj_dict["category_id"] = self.category_id
        if self.date_of_purchase:
            obj_dict["date_of_purchase"] = self.date_of_purchase
        if self.cost:
            obj_dict["cost"] = self.cost
        if self.warranty:
            obj_dict["warranty"] = self.warranty
        if self.short_description:
            obj_dict["short_description"] = self.short_description
        if self.long_description:
            obj_dict["long_description"] = self.long_description
        if self.cover_image:
            obj_dict["cover_image"] = self.cover_image
        if self.brand:
            obj_dict["brand"] = self.brand
        if self.company:
            obj_dict["company"] = self.company
        if self.invoice_copy_filename:
            obj_dict["invoice_copy_filename"] = self.invoice_copy_filename

        if self.created_at:
            obj_dict["created_at"] = self.created_at
        if self.updated_at:
            obj_dict["updated_at"] = self.updated_at
        if self.created_by_id:
            obj_dict["created_by_id"] = self.created_by_id
        if self.updated_by_id:
            obj_dict["updated_by_id"] = self.updated_by_id
        return obj_dict

    def from_dict(self, obj_dict):
        super(self.__class__, self).from_dict(obj_dict)

        if "owner_id" in obj_dict:
            self.owner_id = obj_dict["owner_id"]
        if "category_id" in obj_dict:
            self.category_id = obj_dict["category_id"]
        if "date_of_purchase" in obj_dict:
            self.date_of_purchase = obj_dict["date_of_purchase"]
        if "cost" in obj_dict:
            self.cost = obj_dict["cost"]
        if "warranty" in obj_dict:
            self.warranty = obj_dict["warranty"]
        if "short_description" in obj_dict:
            self.short_description = obj_dict["short_description"]
        if "long_description" in obj_dict:
            self.long_description = obj_dict["long_description"]
        if "cover_image" in obj_dict:
            self.cover_image = obj_dict["cover_image"]
        if "brand" in obj_dict:
            self.brand = obj_dict["brand"]
        if "company" in obj_dict:
            self.company = obj_dict["company"]
        if "invoice_copy_filename" in obj_dict:
            self.invoice_copy_filename = obj_dict["invoice_copy_filename"]

        if "created_at" in obj_dict:
            self.created_at = obj_dict["created_at"]
        if "updated_at" in obj_dict:
            self.updated_at = obj_dict["updated_at"]
        if "created_by_id" in obj_dict:
            self.created_by_id = obj_dict["created_by_id"]
        if "updated_by_id" in obj_dict:
            self.updated_by_id = obj_dict["updated_by_id"]
        return self

    def from_json_dict(self, obj_dict):
        self.from_dict(obj_dict)
        if "furniture_category" in obj_dict:
            self.category_id = FurnitureCategories().from_dict(obj_dict["furniture_category"]).id
        if "owner" in obj_dict:
            self.owner_id = Owner().from_dict(obj_dict["owner"]).id

        return self

    def to_json_dict(self, include=None):
        if not include: include = []

        obj_dict = self.to_dict()

        if "furniture_category" in include and self.furniture_category:
            obj_dict["furniture_category"] = self.furniture_category.to_dict()
        else:
            obj_dict["furniture_category"] = {"id": self.category_id}
        if "owner" in include and self.owner:
            obj_dict["owner"] = self.owner.to_dict()
        else:
            obj_dict["owner"] = {"id": self.owner_id}

        obj_dict["created_at"] = self.created_at
        obj_dict["updated_at"] = self.updated_at
        obj_dict["created_by_id"] = self.created_by_id
        obj_dict["updated_by_id"] = self.updated_by_id

        if "category_id" in obj_dict: del obj_dict["category_id"]
        if "owner_id" in obj_dict: del obj_dict["owner_id"]

        return obj_dict

    @classmethod
    def get_all(cls, db, start=0, count=25, order_by=None):
        query = db.query(cls)
        if not order_by:
            order_by = cls.id.desc()
        return query.order_by(order_by).offset(start).limit(count).all()

    @classmethod
    def get_all_count(cls, db):
        query = db.query(cls)
        return query.count()
