from boto.exception import S3ResponseError
from client.aws import AmazonS3Client
from conf import Conf
from exception import NotFoundException, InvalidInputException

__author__ = 'Govind'

import datetime
from sqlalchemy import Column, String, DateTime, Integer, Text, ForeignKey, Enum
from model.common import ModelBase

class Images(ModelBase):
    __tablename__ = "images"

    display_name = Column(String(255), nullable=False)
    description = Column(Text, nullable=False)
    created_at = Column(DateTime, nullable=False, default=datetime.datetime.now())
    owner_id = Column(Integer, nullable=False)
    owner_type = Column(Enum("ALBUM", "DESIGN", "ROOM"), nullable=False)
    file_name = Column(String(255), nullable=False)

    def to_dict(self):
        obj_dict = super(self.__class__, self).to_dict()
        if self.display_name:
            obj_dict["display_name"] = self.display_name
        if self.description:
            obj_dict["description"] = self.description
        if self.created_at:
            obj_dict["created_at"] = self.created_at
        if self.owner_id:
            obj_dict["owner_id"] = self.owner_id
        if self.owner_type:
            obj_dict["owner_type"] = self.owner_type
        if self.file_name:
            obj_dict["file_name"] = self.file_name

        return obj_dict

    def from_dict(self, obj_dict):
        super(self.__class__, self).from_dict(obj_dict)
        if "display_name" in obj_dict:
            self.display_name = obj_dict["display_name"]
        if "description" in obj_dict:
            self.description = obj_dict["description"]
        if "created_at" in obj_dict:
            self.created_at = obj_dict["created_at"]
        if "owner_id" in obj_dict:
            self.owner_id = obj_dict["owner_id"]
        if "owner_type" in obj_dict:
            self.owner_type = obj_dict["owner_type"]
        if "file_name" in obj_dict:
            self.file_name = obj_dict["file_name"]
        return self

    def to_json_dict(self, image_url_context, include=None):
        if not image_url_context:
            raise InvalidInputException("image_url_context not found to move image to correct bucket")
        if not include: include = []
        obj_dict = self.to_dict()
        s3_conf = Conf.get("amazon_s3")
        obj_dict["file_url"] = "https://"+s3_conf["bucket_images"]+".s3.amazonaws.com/" + image_url_context.get_s3_key_prefix() + self.file_name
        return obj_dict

    def from_json_dict(self, obj_dict, image_url_context):
        self.from_dict(obj_dict)
        if not image_url_context:
            raise InvalidInputException("image_url_context not found to move image to correct bucket")
        if "file_name" in obj_dict:
            file_name = obj_dict["file_name"]
            s3_conf = Conf.get("amazon_s3")
            s3_client = AmazonS3Client(s3_conf["key_id"], s3_conf["key_secret"])
            src_key = image_url_context.get_s3_tmp_key_prefix() + file_name
            target_key = image_url_context.get_s3_key_prefix() + file_name
            try:
                s3_client.move(s3_conf["bucket_images"], src_key, target_key)
            except S3ResponseError, e:
                if "404" in str(e):
                    raise NotFoundException("file_name: " + file_name +" is not a valid image file")
                else:
                    raise e
        return self

    @classmethod
    def get_all_images(cls, db, owner_id, owner_type, start=0, count=5, order_by=None):
        query = db.query(cls)
        if not owner_id or not owner_type:
            raise InvalidInputException("Owner_id and owner_type not found");

        query = query.filter(cls.owner_id == owner_id).filter(cls.owner_type == owner_type)

        if not order_by:
            order_by = cls.id.desc()

        return query.order_by(order_by).offset(start).limit(count).all()

    @classmethod
    def get_all_count(cls, db, owner_id, owner_type):
        if not owner_id or not owner_type:
            raise InvalidInputException("Owner_id and owner_type not found");
        query = db.query(cls)

        query = query.filter(cls.owner_id == owner_id).filter(cls.owner_type == owner_type)

        return query.count()

    @classmethod
    def get_by_filename(cls, db, file_name, owner_type):
        return db.query(cls).filter(cls.file_name == file_name).filter(cls.owner_type == owner_type).first()

