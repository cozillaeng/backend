import datetime
from sqlalchemy import Column, String, Boolean, Enum, DateTime, Integer, ForeignKey, Text, Float
from sqlalchemy.orm import relationship
from model.common import ModelBase
from model.designer import Designer
from model.common import ModelBase, City
from model.furniture_categories import FurnitureCategories



class RentedFurniture(ModelBase):
    __tablename__ = "rented_furnitures"

    seller_id = Column(Integer)
    category_id = Column(Integer, ForeignKey(FurnitureCategories.id))
    date_of_rent = Column(DateTime, nullable=False)
    rent = Column(Float, nullable=False)
    deposit = Column(Float)
    lock_in_period = Column(Float)
    agreement_id = Column(Integer)
    short_description = Column(String(50))
    long_description = Column(String(100))
    cover_image = Column(String(255))
    brand = Column(String(20))
    company = Column(String(20))

    furniture_category = relationship(FurnitureCategories,foreign_keys='RentedFurniture.category_id', lazy="joined")

    created_at = Column(DateTime, nullable=False, default=datetime.datetime.now())
    updated_at = Column(DateTime, nullable=False, default=datetime.datetime.now())
    created_by_id = Column(Integer, nullable=False)
    updated_by_id = Column(Integer, nullable=False)

    def to_dict(self):
        obj_dict = super(self.__class__, self).to_dict()
        if self.seller_id:
            obj_dict["seller_id"] = self.seller_id
        if self.category_id:
            obj_dict["category_id"] = self.category_id
        if self.date_of_rent:
            obj_dict["date_of_rent"] = self.date_of_rent
        if self.rent:
            obj_dict["rent"] = self.rent
        if self.deposit:
            obj_dict["deposit"] = self.deposit
        if self.lock_in_period:
            obj_dict["lock_in_period"] = self.lock_in_period
        if self.agreement_id:
            obj_dict["agreement_id"] = self.agreement_id
        if self.short_description:
            obj_dict["short_description"] = self.short_description
        if self.long_description:
            obj_dict["long_description"] = self.long_description
        if self.cover_image:
            obj_dict["cover_image"] = self.cover_image
        if self.brand:
            obj_dict["brand"] = self.brand
        if self.company:
            obj_dict["company"] = self.company

        if self.created_at:
            obj_dict["created_at"] = self.created_at
        if self.updated_at:
            obj_dict["updated_at"] = self.updated_at
        if self.created_by_id:
            obj_dict["created_by_id"] = self.created_by_id
        if self.updated_by_id:
            obj_dict["updated_by_id"] = self.updated_by_id
        return obj_dict

    def from_dict(self, obj_dict):
        super(self.__class__, self).from_dict(obj_dict)

        if "seller_id" in obj_dict :
            self.seller_id = obj_dict["seller_id"]
        if "category_id" in obj_dict :
            self.category_id = obj_dict["category_id"]
        if "date_of_rent" in obj_dict :
            self.date_of_rent = obj_dict["date_of_rent"]
        if "rent" in obj_dict :
            self.rent = obj_dict["rent"]
        if "deposit" in obj_dict :
            self.deposit = obj_dict["deposit"]
        if "lock_in_period" in obj_dict :
            self.lock_in_period = obj_dict["lock_in_period"]
        if "agreement_id" in obj_dict :
            self.agreement_id = obj_dict["agreement_id"]
        if "short_description" in obj_dict :
            self.short_description = obj_dict["short_description"]
        if "long_description" in obj_dict :
            self.long_description = obj_dict["long_description"]
        if "cover_image" in obj_dict :
            self.cover_image = obj_dict["cover_image"]
        if "brand" in obj_dict :
            self.brand = obj_dict["brand"]
        if "company" in obj_dict :
            self.company = obj_dict["company"]

        if "created_at" in obj_dict:
            self.created_at = obj_dict["created_at"]
        if "updated_at" in obj_dict:
            self.updated_at = obj_dict["updated_at"]
        if "created_by_id" in obj_dict:
            self.created_by_id = obj_dict["created_by_id"]
        if "updated_by_id" in obj_dict:
            self.updated_by_id = obj_dict["updated_by_id"]
        return self

    def from_json_dict(self, obj_dict):
        self.from_dict(obj_dict)
        if "furniture_category" in obj_dict:
            self.category_id = FurnitureCategories().from_dict(obj_dict["furniture_category"]).id
        return self

    def to_json_dict(self, include=None):
        if not include: include = []

        obj_dict = self.to_dict()

        if "furniture_category" in include and self.furniture_category:
            obj_dict["furniture_category"] = self.furniture_category.to_dict()
        else:
            obj_dict["furniture_category"] = {"id": self.category_id}

        obj_dict["created_at"] = self.created_at
        obj_dict["updated_at"] = self.updated_at
        obj_dict["created_by_id"] = self.created_by_id
        obj_dict["updated_by_id"] = self.updated_by_id

        if "category_id" in obj_dict: del obj_dict["category_id"]

        return obj_dict

    @classmethod
    def get_all(cls, db, start=0, count=25, order_by=None):
        query = db.query(cls)
        if not order_by:
            order_by = cls.id.desc()
        return query.order_by(order_by).offset(start).limit(count).all()

    @classmethod
    def get_all_count(cls, db):
        query = db.query(cls)
        return query.count()
