from sqlalchemy import String, Integer, ForeignKey, Boolean
from sqlalchemy import Column
from sqlalchemy.orm import relationship

from model.common import ModelBase
from util.common import Parser

class ProductComponentOption(ModelBase):
    __tablename__ = "product_component_options"
    
    name = Column(String(256))
    display_name = Column(String(256))
    product_component_id =  Column(Integer, ForeignKey("product_components.id"))
    swatch_image_url = Column(String(256))
    texture_url = Column(String(256))
    baked_texture_url = Column(String(256))
    product_component = relationship("ProductComponent",foreign_keys=[product_component_id],lazy="joined")

    def to_dict(self):
        obj_dict = super(self.__class__, self).to_dict()
        
        if self.name:
            obj_dict["name"] = self.name
        if self.display_name:
            obj_dict["display_name"] = self.display_name
        if self.product_component_id:
            obj_dict["product_component_id"] = self.product_component_id
        if self.swatch_image_url:
            obj_dict["swatch_image_url"] = self.swatch_image_url
        if self.texture_url:
            obj_dict["texture_url"] = self.texture_url
        if self.baked_texture_url:
            obj_dict["baked_texture_url"] = self.baked_texture_url

        return obj_dict

    def from_dict(self, obj_dict):
        if "name" in obj_dict:
            self.name = obj_dict["name"]
        if "display_name" in obj_dict:
            self.display_name = obj_dict["display_name"]
        if "product_component_id" in obj_dict:
            self.product_component_id = obj_dict["product_component_id"]
        if "swatch_image_url" in obj_dict:
            self.swatch_image_url = obj_dict["swatch_image_url"]
        if "texture_url" in obj_dict:
            self.texture_url = obj_dict["texture_url"]
        if "baked_texture_url" in obj_dict:
            self.baked_texture_url = obj_dict["baked_texture_url"]
        
        return self

    def to_json_dict(self, include=None):
        if not include:
            include = []

        obj_dict = self.to_dict()
        #if self.product_component:
        #    print self.product_component.id
        #    print self.product_component.name
        
        return obj_dict

    def from_json_dict(self, obj_dict):
        self.from_dict(obj_dict)
