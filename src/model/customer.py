import datetime
from sqlalchemy import Column, String, Boolean, Enum, DateTime, Integer, ForeignKey, Text, Float
from sqlalchemy.orm import relationship
from model.common import ModelBase
from model.designer import Designer
from model.common import ModelBase
from model.city import City



class Customer(ModelBase):
    __tablename__ = "customers"

    first_name = Column(String(50), nullable=False)
    last_name = Column(String(50), nullable=False)
    display_name = Column(String(100), nullable=False)
    email = Column(String(100), nullable=False)
    phone_primary = Column(String(20))
    phone_secondary = Column(String(20))
    address = Column(String(100))
    city_id = Column(Integer, ForeignKey(City.id), nullable=False)
    pincode = Column(String(10))
    skype_id = Column(String(50))


    city = relationship(City, lazy="joined")


    created_at = Column(DateTime, nullable=False, default=datetime.datetime.now())
    updated_at = Column(DateTime, nullable=False, default=datetime.datetime.now())
    created_by_id = Column(Integer, nullable=False)
    updated_by_id = Column(Integer, nullable=False)

    def to_dict(self):
        obj_dict = super(self.__class__, self).to_dict()
        if self.first_name:
            obj_dict["first_name"] = self.first_name
        if self.last_name:
            obj_dict["last_name"] = self.last_name
        if self.display_name:
            obj_dict["display_name"] = self.display_name
        if self.email:
            obj_dict["email"] = self.email
        if self.phone_primary:
            obj_dict["phone_primary"] = self.phone_primary
        if self.phone_secondary:
            obj_dict["phone_secondary"] = self.phone_secondary
        if self.address:
            obj_dict["address"] = self.address
        if self.city_id:
            obj_dict["city_id"] = self.city_id
        if self.pincode:
            obj_dict["pincode"] = self.pincode
        if self.skype_id:
            obj_dict["skype_id"] = self.skype_id
        if self.created_at:
            obj_dict["created_at"] = self.created_at
        if self.updated_at:
            obj_dict["updated_at"] = self.updated_at
        if self.created_by_id:
            obj_dict["created_by_id"] = self.created_by_id
        if self.updated_by_id:
            obj_dict["updated_by_id"] = self.updated_by_id
        return obj_dict

    def from_dict(self, obj_dict):
        super(self.__class__, self).from_dict(obj_dict)

        if "first_name" in obj_dict:
            self.first_name = obj_dict["first_name"]
        if "last_name" in obj_dict:
            self.last_name = obj_dict["last_name"]
        if "display_name" in obj_dict:
            self.display_name = obj_dict["display_name"]
        if "email" in obj_dict:
            self.email = obj_dict["email"]
        if "phone_primary" in obj_dict:
            self.phone_primary = obj_dict["phone_primary"]
        if "phone_secondary" in obj_dict:
            self.phone_secondary = obj_dict["phone_secondary"]
        if "address" in obj_dict:
            self.address = obj_dict["address"]
        if "city_id" in obj_dict:
            self.city_id = obj_dict["city_id"]
        if "pincode" in obj_dict:
            self.pincode = obj_dict["pincode"]
        if "skype_id" in obj_dict:
            self.skype_id = obj_dict["skype_id"]

        if "created_at" in obj_dict:
            self.created_at = obj_dict["created_at"]
        if "updated_at" in obj_dict:
            self.updated_at = obj_dict["updated_at"]
        if "created_by_id" in obj_dict:
            self.created_by_id = obj_dict["created_by_id"]
        if "updated_by_id" in obj_dict:
            self.updated_by_id = obj_dict["updated_by_id"]
        return self

    def from_json_dict(self, obj_dict):
        self.from_dict(obj_dict)
        if "city" in obj_dict:
            self.city_id = City().from_dict(obj_dict["city"]).id
        return self

    def to_json_dict(self, include=None):
        if not include: include = []

        obj_dict = self.to_dict()

        if "city" in include and self.city:
            obj_dict["city"] = self.city.to_dict()
        else:
            obj_dict["city"] = {"id": self.city_id}

        obj_dict["created_at"] = self.created_at
        obj_dict["updated_at"] = self.updated_at
        obj_dict["created_by_id"] = self.created_by_id
        obj_dict["updated_by_id"] = self.updated_by_id

        if "city_id" in obj_dict: del obj_dict["city_id"]

        return obj_dict

    @classmethod
    def get_all(cls, db, start=0, count=25, order_by=None):
        query = db.query(cls)
        if not order_by:
            order_by = cls.id.desc()
        return query.order_by(order_by).offset(start).limit(count).all()

    @classmethod
    def get_all_count(cls, db):
        query = db.query(cls)
        return query.count()
