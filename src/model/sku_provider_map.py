__author__ = 'ramakrishnag'


import datetime

from sqlalchemy import Column, Integer, DateTime, Float, ForeignKey, String
from sqlalchemy.orm import relationship
from model.common import ModelBase

class SkuProviderMap(ModelBase):
    __tablename__ = 'sku_to_provider_mappings'

    sku_id = Column(Integer,ForeignKey("sku.id"),nullable=False)
    provider_id = Column(Integer,ForeignKey("catalogue_providers.id"),nullable=False)
    
    def to_dict(self):
        obj_dict = super(self.__class__, self).to_dict()
        if self.sku_id:
            obj_dict["sku_id"] = self.sku_id
        if self.provider_id:
            obj_dict["provider_id"] = self.provider_id
       
        return obj_dict

    def from_dict(self, obj_dict):
        super(self.__class__, self).from_dict(obj_dict)
        if "sku_id" in obj_dict:
            self.sku_id = obj_dict["sku_id"]
        if "provider_id" in obj_dict:
            self.provider_id = obj_dict["provider_id"]
        
        return self

    def to_json_dict(self, include=None):
        if not include: include = []
        obj_dict = self.to_dict()

        return obj_dict

    def from_json_dict(self, obj_dict):
        self.from_dict(obj_dict)

        return self
