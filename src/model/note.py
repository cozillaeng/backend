__author__ = 'ramakrishnag'

import datetime
from sqlalchemy import Column, String, DateTime, Integer, Text, Enum
from model.common import ModelBase

class Note(ModelBase):
    __tablename__ = "notes"

    description = Column(Text, nullable=False)
    created_at = Column(DateTime, nullable=False, default=datetime.datetime.now())
    updated_at = Column(DateTime, nullable=False, default=datetime.datetime.now())
    owner_id = Column(Integer,nullable=False)
    owner_type = Column(Enum("DESIGN", "PROJECT"), nullable=False)
    created_by_id = Column(Integer,nullable=False)
    updated_by_id = Column(Integer,nullable=False)

    def to_dict(self):
        obj_dict = super(self.__class__, self).to_dict()
        if self.owner_id:
            obj_dict["owner_id"] = self.owner_id
        if self.description:
            obj_dict["description"] = self.description
        if self.created_at:
            obj_dict["created_at"] = self.created_at
        if self.updated_at:
            obj_dict["updated_at"] = self.updated_at
        if self.owner_type:
            obj_dict["owner_type"] = self.owner_type
        if self.created_by_id:
            obj_dict["created_by_id"] = self.created_by_id
        if self.updated_by_id:
            obj_dict["updated_by_id"] = self.updated_by_id

        return obj_dict

    def from_dict(self, obj_dict):
        super(self.__class__, self).from_dict(obj_dict)
        if "owner_id" in obj_dict:
            self.owner_id = obj_dict["owner_id"]
        if "description" in obj_dict:
            self.description = obj_dict["description"]
        if "created_at" in obj_dict:
            self.created_at = obj_dict["created_at"]
        if "updated_at" in obj_dict:
            self.updated_at = obj_dict["updated_at"]
        if "owner_type" in obj_dict:
            self.owner_type = obj_dict["owner_type"].upper()
        if "created_by_id" in obj_dict:
            self.created_by_id = obj_dict["created_by_id"]
        if "updated_by_id" in obj_dict:
            self.updated_by_id = obj_dict["updated_by_id"]

        return self

    def to_json_dict(self, include=None):
        if not include: include = []
        obj_dict = self.to_dict()

        return obj_dict

    def from_json_dict(self, obj_dict):
        self.from_dict(obj_dict)

        return self

    @classmethod
    def get_all_notes(cls, db, start=0, count=5, owner_id=None, owner_type=None, order_by=None):
        query = db.query(cls)

        if owner_id:
            query = query.filter(cls.owner_id == owner_id)

        if owner_type:
            query = query.filter(cls.owner_type == owner_type)

        if not order_by:
            order_by = cls.id.desc()

        return query.order_by(order_by).offset(start).limit(count).all()

    @classmethod
    def get_all_count(cls, db, owner_id=None, owner_type=None):
        query = db.query(cls)

        if owner_id:
            query = query.filter(cls.owner_id == owner_id)
        if owner_type:
            query = query.filter(cls.owner_type == owner_type)

        return query.count()

