import datetime
from sqlalchemy import Column, String, Boolean, Enum, DateTime, Integer, ForeignKey, Text, Float
from sqlalchemy.orm import relationship
from model.common import ModelBase
from model.designer import Designer
from model.common import ModelBase
from model.city import City


class Owner(ModelBase):
    __tablename__ = "owners"

    first_name = Column(String(50), nullable=False)
    last_name = Column(String(50), nullable=False)
    date_of_birth = Column(DateTime, nullable=False)
    profile_pic = Column(String(50))
    email = Column(String(100), nullable=False)
    phone_primary = Column(String(20))
    phone_secondary = Column(String(20))
    facebook_profile = Column(String(100))
    linkedin_profile = Column(String(100))
    twitter_profile = Column(String(100))
    pancard = Column(String(20))
    bank_acc_no = Column(String(50))
    bank_ifsc_code = Column(String(20))
    company_name = Column(String(50))
    designation = Column(String(50))
    gender = Column(Enum("MALE", "FEMALE"), nullable=False)

    permanent_address = Column(String(100))
    permanent_pincode = Column(String(10))


    created_at = Column(DateTime, nullable=False, default=datetime.datetime.now())
    updated_at = Column(DateTime, nullable=False, default=datetime.datetime.now())
    created_by_id = Column(Integer, nullable=False)
    updated_by_id = Column(Integer, nullable=False)

    def to_dict(self):
        obj_dict = super(self.__class__, self).to_dict()
        if self.first_name :
            obj_dict["first_name"] = self.first_name
        if self.last_name :
            obj_dict["last_name"] = self.last_name
        if self.date_of_birth :
            obj_dict["date_of_birth"] = self.date_of_birth
        if self.profile_pic :
            obj_dict["profile_pic"] = self.profile_pic
        if self.email :
            obj_dict["email"] = self.email
        if self.phone_primary :
            obj_dict["phone_primary"] = self.phone_primary
        if self.phone_secondary :
            obj_dict["phone_secondary"] = self.phone_secondary
        if self.facebook_profile :
            obj_dict["facebook_profile"] = self.facebook_profile
        if self.linkedin_profile :
            obj_dict["linkedin_profile"] = self.linkedin_profile
        if self.twitter_profile :
            obj_dict["twitter_profile"] = self.twitter_profile
        if self.pancard :
            obj_dict["pancard"] = self.pancard
        if self.bank_acc_no :
            obj_dict["bank_acc_no"] = self.bank_acc_no
        if self.bank_ifsc_code :
            obj_dict["bank_ifsc_code"] = self.bank_ifsc_code
        if self.company_name :
            obj_dict["company_name"] = self.company_name
        if self.designation :
            obj_dict["designation"] = self.designation
        if self.gender :
            obj_dict["gender"] = self.gender
        if self.permanent_address :
            obj_dict["permanent_address"] = self.permanent_address

        if self.permanent_pincode :
            obj_dict["permanent_pincode"] = self.permanent_pincode

        if self.created_at:
            obj_dict["created_at"] = self.created_at
        if self.updated_at:
            obj_dict["updated_at"] = self.updated_at
        if self.created_by_id:
            obj_dict["created_by_id"] = self.created_by_id
        if self.updated_by_id:
            obj_dict["updated_by_id"] = self.updated_by_id
        return obj_dict

    def from_dict(self, obj_dict):
        super(self.__class__, self).from_dict(obj_dict)

        if "first_name" in obj_dict:
            self.first_name = obj_dict["first_name"]
        if "last_name" in obj_dict:
            self.last_name = obj_dict["last_name"]
        if "date_of_birth" in obj_dict:
            self.date_of_birth = obj_dict["date_of_birth"]
        if "profile_pic" in obj_dict:
            self.profile_pic = obj_dict["profile_pic"]
        if "email" in obj_dict:
            self.email = obj_dict["email"]
        if "phone_primary" in obj_dict:
            self.phone_primary = obj_dict["phone_primary"]
        if "phone_secondary" in obj_dict:
            self.phone_secondary = obj_dict["phone_secondary"]
        if "facebook_profile" in obj_dict:
            self.facebook_profile = obj_dict["facebook_profile"]
        if "linkedin_profile" in obj_dict:
            self.linkedin_profile = obj_dict["linkedin_profile"]
        if "twitter_profile" in obj_dict:
            self.twitter_profile = obj_dict["twitter_profile"]
        if "pancard" in obj_dict:
            self.pancard = obj_dict["pancard"]
        if "bank_acc_no" in obj_dict:
            self.bank_acc_no = obj_dict["bank_acc_no"]
        if "bank_ifsc_code" in obj_dict:
            self.bank_ifsc_code = obj_dict["bank_ifsc_code"]
        if "company_name" in obj_dict:
            self.company_name = obj_dict["company_name"]
        if "designation" in obj_dict:
            self.designation = obj_dict["designation"]
        if "gender" in obj_dict:
            self.gender = obj_dict["gender"]
        if "permanent_address" in obj_dict:
            self.permanent_address = obj_dict["permanent_address"]

        if "permanent_pincode" in obj_dict:
            self.permanent_pincode = obj_dict["permanent_pincode"]
        if "created_at" in obj_dict:
            self.created_at = obj_dict["created_at"]
        if "updated_at" in obj_dict:
            self.updated_at = obj_dict["updated_at"]
        if "created_by_id" in obj_dict:
            self.created_by_id = obj_dict["created_by_id"]
        if "updated_by_id" in obj_dict:
            self.updated_by_id = obj_dict["updated_by_id"]
        return self

    def from_json_dict(self, obj_dict):
        self.from_dict(obj_dict)

        return self

    def to_json_dict(self, include=None):
        if not include: include = []

        obj_dict = self.to_dict()


        obj_dict["created_at"] = self.created_at
        obj_dict["updated_at"] = self.updated_at
        obj_dict["created_by_id"] = self.created_by_id
        obj_dict["updated_by_id"] = self.updated_by_id


        return obj_dict

    @classmethod
    def get_all(cls, db, start=0, count=25, order_by=None):
        query = db.query(cls)
        if not order_by:
            order_by = cls.id.desc()
        return query.order_by(order_by).offset(start).limit(count).all()

    @classmethod
    def get_all_count(cls, db):
        query = db.query(cls)
        return query.count()
