from sqlalchemy import String, Integer, ForeignKey, Boolean
from sqlalchemy import Column
from sqlalchemy.orm import relationship

from model.common import ModelBase
from util.common import Parser

from model.product_component_option import ProductComponentOption

class ProductComponent(ModelBase):
    __tablename__ = "product_components"
    
    name = Column(String(256))
    display_name = Column(String(256))
    product_id =  Column(Integer, ForeignKey("products.id"), nullable=False)
    product_component_options = relationship(ProductComponentOption, lazy="joined",cascade="all, delete, delete-orphan")

    def to_dict(self):
        obj_dict = super(self.__class__, self).to_dict()
        
        if self.name:
            obj_dict["name"] = self.name
        if self.display_name:
            obj_dict["display_name"] = self.display_name
        if self.product_id:
            obj_dict["product_id"] = self.product_id

        return obj_dict

    def from_dict(self, obj_dict):
        if "name" in obj_dict:
            self.name = obj_dict["name"]
        if "display_name" in obj_dict:
            self.display_name = obj_dict["display_name"]
        if "product_id" in obj_dict:
            self.product_id = obj_dict["product_id"]
        
        return self

    def to_json_dict(self, include=None):
        if not include:
            include = []

        obj_dict = self.to_dict()

        if self.product_component_options:
            obj_dict["options"] = []
            for option in self.product_component_options:
                obj_dict["options"].append(option.to_json_dict())
        
        return obj_dict

    def from_json_dict(self, obj_dict):
        self.from_dict(obj_dict)
