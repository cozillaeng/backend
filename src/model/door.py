__author__ = 'ramakrishnag'

import datetime
from sqlalchemy import Column, String, DateTime, Integer, ForeignKey, Float, Boolean
from sqlalchemy.orm import relationship
from model.common import ModelBase


class Door(ModelBase):
    __tablename__ = "doors"

    floor_id = Column(Integer, ForeignKey("floors.id"), nullable=False)
    sku_id = Column(Integer,nullable=False)
    location_x = Column(Float,nullable=False)
    location_y = Column(Float,nullable=False)
    location_z = Column(Float,nullable=False)
    rotation_x = Column(Float,nullable=False)
    rotation_y = Column(Float,nullable=False)
    rotation_z = Column(Float,nullable=False)
    scale_x = Column(Float,nullable=False)
    scale_y = Column(Float,nullable=False)
    scale_z = Column(Float,nullable=False)
    created_at = Column(DateTime, nullable=False, default=datetime.datetime.now())
    updated_at = Column(DateTime, nullable=False, default=datetime.datetime.now())
    uuid = Column(String(64))
    wall_uuid = Column(String(64))
    name = Column(String(256))
    visible = Column(Boolean)
    width = Column(Float)
    height = Column(Float)
    offsetX = Column(Float)
    offsetY = Column(Float)
    image_path = Column(String(256))

    def to_dict(self):
        obj_dict = super(self.__class__, self).to_dict()
        if self.floor_id:
            obj_dict["floor_id"] = self.floor_id
        if self.sku_id:
            obj_dict["sku_id"] = self.sku_id
        if self.created_at:
            obj_dict["created_at"] = self.created_at
        if self.updated_at:
            obj_dict["updated_at"] = self.updated_at
        if self.uuid:
            obj_dict["uuid"] = self.uuid
        if self.wall_uuid:
            obj_dict["wall_uuid"] = self.wall_uuid
        if self.name:
            obj_dict["name"] = self.name
        if self.visible:
            obj_dict["visible"] = self.visible
        if self.width:
            obj_dict["width"] = self.width
        if self.height:
            obj_dict["height"] = self.height
        if self.offsetX:
            obj_dict["offsetX"] = self.offsetX
        if self.offsetY:
            obj_dict["offsetY"] = self.offsetY
        if self.image_path:
            obj_dict["image_path"] = self.image_path
        obj_dict["type"] = "Door"

        return obj_dict

    def from_dict(self, obj_dict):
        super(self.__class__, self).from_dict(obj_dict)
        if "floor_id" in obj_dict:
            self.floor_id = obj_dict["floor_id"]
        if "sku_id" in obj_dict:
            self.sku_id = obj_dict["sku_id"]
        if "uuid" in obj_dict:
            self.uuid = obj_dict["uuid"]
        if "wall_uuid" in obj_dict:
            self.wall_uuid = obj_dict["wall_uuid"]
        if "name" in obj_dict:
            self.name = obj_dict["name"]
        if "visible" in obj_dict:
            self.visible = obj_dict["visible"]
        if "width" in obj_dict:
            self.width = obj_dict["width"]
        if "height" in obj_dict:
            self.height = obj_dict["height"]
        if "offsetX" in obj_dict:
            self.offsetX = obj_dict["offsetX"]
        if "offsetY" in obj_dict:
            self.offsetY = obj_dict["offsetY"]
        if "image_path" in obj_dict:
            self.image_path = obj_dict["image_path"]

        return self

    def to_json_dict(self, include=None):
        if not include: include = []
        obj_dict = self.to_dict()

        location = {}
        location["x"] = self.location_x
        location["y"] = self.location_y
        location["z"] = self.location_z
        obj_dict["location"] = location

        rotation = {}
        rotation["x"] = self.rotation_x
        rotation["y"] = self.rotation_y
        rotation["z"] = self.rotation_z
        obj_dict["rotation"] = rotation

        scale = {}
        scale["x"] = self.scale_x
        scale["y"] = self.scale_y
        scale["z"] = self.scale_z
        obj_dict["scale"] = scale

        return obj_dict

    def from_json_dict(self, obj_dict):
        self.from_dict(obj_dict)

        if "location" in obj_dict:
            location = obj_dict["location"]
            self.location_x = location["x"]
            self.location_y = location["y"]
            self.location_z = location["z"]

        if "rotation" in obj_dict:
            rotation = obj_dict["rotation"]
            self.rotation_x = rotation["x"]
            self.rotation_y = rotation["y"]
            self.rotation_z = rotation["z"]

        if "scale" in obj_dict:
            scale = obj_dict["scale"]
            self.scale_x = scale["x"]
            self.scale_y = scale["y"]
            self.scale_z = scale["z"]

        return self

    def clone(self,source):
        source_dict = source.to_json_dict()
        source_dict["id"] = None
        source_dict["floor_id"] = None
        source_dict["created_at"] = None
        source_dict["updated_at"] = None
        self.from_json_dict(source_dict)
        return self
