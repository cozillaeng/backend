import datetime
from sqlalchemy import Column, String, Boolean, Enum, DateTime, Integer, ForeignKey, Text, Float
from sqlalchemy.orm import relationship
from model.common import ModelBase
from model.designer import Designer
from model.common import ModelBase, City
from model.owner import Owner
from model.owners_furniture import OwnersFurniture
from model.purchased_furniture import PurchasedFurniture
from rented_furniture import RentedFurniture


class Furniture(ModelBase):
    __tablename__ = "owners_furnitures"

    purchased_furniture_id = Column(Integer, ForeignKey(PurchasedFurniture.id))
    rented_furniture_id = Column(Integer, ForeignKey(RentedFurniture.id))
    owners_furniture_id = Column(Integer, ForeignKey(OwnersFurniture.id))

    purchased_furniture = relationship(PurchasedFurniture, foreign_keys='Furniture.purchased_furniture_id', lazy="joined")
    rented_furniture = relationship(RentedFurniture, foreign_keys='Furniture.rented_furniture_id', lazy="joined")
    owners_furniture = relationship(OwnersFurniture, foreign_keys='Furniture.owners_furniture_id', lazy="joined")

    created_at = Column(DateTime, nullable=False, default=datetime.datetime.now())
    updated_at = Column(DateTime, nullable=False, default=datetime.datetime.now())
    created_by_id = Column(Integer, nullable=False)
    updated_by_id = Column(Integer, nullable=False)

    def to_dict(self):
        obj_dict = super(self.__class__, self).to_dict()
        if self.purchased_furniture_id:
            obj_dict["purchased_furniture_id"] = self.purchased_furniture_id
        if self.rented_furniture_id:
            obj_dict["rented_furniture_id"] = self.rented_furniture_id
        if self.owners_furniture:
            obj_dict["owners_furniture"] = self.owners_furniture

        if self.created_at:
            obj_dict["created_at"] = self.created_at
        if self.updated_at:
            obj_dict["updated_at"] = self.updated_at
        if self.created_by_id:
            obj_dict["created_by_id"] = self.created_by_id
        if self.updated_by_id:
            obj_dict["updated_by_id"] = self.updated_by_id
        return obj_dict

    def from_dict(self, obj_dict):
        super(self.__class__, self).from_dict(obj_dict)

        if "purchased_furniture_id" in obj_dict:
            self.purchased_furniture_id = obj_dict["purchased_furniture_id"]
        if "rented_furniture_id" in obj_dict:
            self.rented_furniture_id = obj_dict["rented_furniture_id"]
        if "owners_furniture" in obj_dict:
            self.owners_furniture = obj_dict["owners_furniture"]

        if "created_at" in obj_dict:
            self.created_at = obj_dict["created_at"]
        if "updated_at" in obj_dict:
            self.updated_at = obj_dict["updated_at"]
        if "created_by_id" in obj_dict:
            self.created_by_id = obj_dict["created_by_id"]
        if "updated_by_id" in obj_dict:
            self.updated_by_id = obj_dict["updated_by_id"]
        return self

    def from_json_dict(self, obj_dict):
        self.from_dict(obj_dict)
        if "purchased_furniture" in obj_dict:
            self.purchased_furniture_id = PurchasedFurniture().from_dict(obj_dict["purchased_furniture"]).id
        if "rented_furniture" in obj_dict:
            self.rented_furniture_id = RentedFurniture().from_dict(obj_dict["rented_furniture"]).id
        if "owners_furniture" in obj_dict:
            self.owners_furniture_id = OwnersFurniture().from_dict(obj_dict["owners_furniture"]).id
        return self

    def to_json_dict(self, include=None):
        if not include: include = []

        obj_dict = self.to_dict()

        if "purchased_furniture" in include and self.purchased_furniture:
            obj_dict["purchased_furniture"] = self.purchased_furniture.to_dict()
        else:
            obj_dict["purchased_furniture"] = {"id": self.purchased_furniture_id}
        if "rented_furniture" in include and self.rented_furniture:
            obj_dict["rented_furniture"] = self.rented_furniture.to_dict()
        else:
            obj_dict["rented_furniture"] = {"id": self.rented_furniture_id}
        if "owners_furniture" in include and self.owners_furniture:
            obj_dict["owners_furniture"] = self.owners_furniture.to_dict()
        else:
            obj_dict["owners_furniture"] = {"id": self.owners_furniture_id}

        obj_dict["created_at"] = self.created_at
        obj_dict["updated_at"] = self.updated_at
        obj_dict["created_by_id"] = self.created_by_id
        obj_dict["updated_by_id"] = self.updated_by_id

        if "purchased_furniture_id" in obj_dict: del obj_dict["purchased_furniture_id"]
        if "rented_furniture_id" in obj_dict: del obj_dict["rented_furniture_id"]
        if "owners_furniture_id" in obj_dict: del obj_dict["owners_furniture_id"]

        return obj_dict

    @classmethod
    def get_all(cls, db, start=0, count=25, order_by=None):
        query = db.query(cls)
        if not order_by:
            order_by = cls.id.desc()
        return query.order_by(order_by).offset(start).limit(count).all()

    @classmethod
    def get_all_count(cls, db):
        query = db.query(cls)
        return query.count()
