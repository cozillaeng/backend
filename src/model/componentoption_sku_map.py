__author__ = 'ramakrishnag'


import datetime

from sqlalchemy import Column, Integer, DateTime, Float, ForeignKey, String
from sqlalchemy.orm import relationship
from model.common import ModelBase
from model.product_component_option import ProductComponentOption

class ComponentOptionSkuMap(ModelBase):
    __tablename__ = 'sku_component_options_map'

    sku_id = Column(Integer,ForeignKey("sku.id"),nullable=False)
    component_option_id = Column(Integer,ForeignKey("product_component_options.id"),nullable=False)
    component_option = relationship("ProductComponentOption", lazy='joined',foreign_keys=[component_option_id])

    #product_component = relationship("ProductComponent",foreign_keys=[product_component_id],lazy="joined")
    
    def to_dict(self):
        obj_dict = super(self.__class__, self).to_dict()
        if self.sku_id:
            obj_dict["sku_id"] = self.sku_id
        if self.component_option_id:
            obj_dict["component_option_id"] = self.component_option_id
       
        return obj_dict

    def from_dict(self, obj_dict):
        super(self.__class__, self).from_dict(obj_dict)
        if "sku_id" in obj_dict:
            self.sku_id = obj_dict["sku_id"]
        if "component_option_id" in obj_dict:
            self.component_option_id = obj_dict["component_option_id"]
        
        return self

    def to_json_dict(self, include=None):
        if not include: include = []
        obj_dict = self.to_dict()
        if self.component_option:
            obj_dict["option"] = self.component_option.to_json_dict()
        return obj_dict

    def from_json_dict(self, obj_dict):
        self.from_dict(obj_dict)

        return self
