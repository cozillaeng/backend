__author__ = 'ramakrishnag'

import datetime
from sqlalchemy import Column, String, DateTime, Integer, ForeignKey, Float
from model.common import ModelBase


class Window(ModelBase):
    __tablename__ = "windows"

    floor_id = Column(Integer, ForeignKey("floors.id"), nullable=False)
    sku_id = Column(Integer,nullable=False)
    position_x = Column(Float,nullable=False)
    position_y = Column(Float,nullable=False)
    position_z = Column(Float,nullable=False)
    rotation_x = Column(Float,nullable=False)
    rotation_y = Column(Float,nullable=False)
    rotation_z = Column(Float,nullable=False)
    scale_x = Column(Float,nullable=False)
    scale_y = Column(Float,nullable=False)
    scale_z = Column(Float,nullable=False)
    created_at = Column(DateTime, nullable=False, default=datetime.datetime.now())
    updated_at = Column(DateTime, nullable=False, default=datetime.datetime.now())
    uuid = Column(String(64))

    def to_dict(self):
        obj_dict = super(self.__class__, self).to_dict()
        if self.floor_id:
            obj_dict["floor_id"] = self.floor_id
        if self.sku_id:
            obj_dict["sku_id"] = self.sku_id
        if self.created_at:
            obj_dict["created_at"] = self.created_at
        if self.updated_at:
            obj_dict["updated_at"] = self.updated_at
        if self.uuid:
            obj_dict["uuid"] = self.uuid

        return obj_dict

    def from_dict(self, obj_dict):
        super(self.__class__, self).from_dict(obj_dict)
        if "floor_id" in obj_dict:
            self.floor_id = obj_dict["floor_id"]
        if "sku_id" in obj_dict:
            self.sku_id = obj_dict["sku_id"]
        if "uuid" in obj_dict:
            self.uuid = obj_dict["uuid"]

        return self

    def to_json_dict(self, include=None):
        if not include:
            include = []
        obj_dict = self.to_dict()

        position = {}
        position["x"] = self.position_x
        position["y"] = self.position_y
        position["z"] = self.position_z
        obj_dict["position"] = position

        rotation = {}
        rotation["x"] = self.rotation_x
        rotation["y"] = self.rotation_y
        rotation["z"] = self.rotation_z
        obj_dict["rotation"] = rotation

        scale = {}
        scale["x"] = self.scale_x
        scale["y"] = self.scale_y
        scale["z"] = self.scale_z
        obj_dict["scale"] = scale

        return obj_dict

    def from_json_dict(self, obj_dict):
        self.from_dict(obj_dict)

        if obj_dict["position"]:
            position = obj_dict["position"]
            self.position_x = position["x"]
            self.position_y = position["y"]
            self.position_z = position["z"]

        if obj_dict["rotation"]:
            rotation = obj_dict["rotation"]
            self.rotation_x = rotation["x"]
            self.rotation_y = rotation["y"]
            self.rotation_z = rotation["z"]

        if obj_dict["scale"]:
            scale = obj_dict["scale"]
            self.scale_x = scale["x"]
            self.scale_y = scale["y"]
            self.scale_z = scale["z"]

        return self

    def clone(self,source):
        source_dict = source.to_json_dict()
        source_dict["id"] = None
        source_dict["floor_id"] = None
        source_dict["created_at"] = None
        source_dict["updated_at"] = None
        self.from_json_dict(source_dict)
        return self
