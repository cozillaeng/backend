import datetime
from sqlalchemy import Column, String, Boolean, Enum, DateTime, Integer, ForeignKey, Text, Float
from sqlalchemy.orm import relationship
from model.common import ModelBase
from model.common import ModelBase

class State(ModelBase):
    __tablename__ = "states"

    name = Column(String(100), nullable=False)
    display_name = Column(String(100), nullable=False)
    is_enabled = Column(Boolean, nullable=False, default=True)

    created_at = Column(DateTime, nullable=False, default=datetime.datetime.now())
    updated_at = Column(DateTime, nullable=False, default=datetime.datetime.now())
    created_by_id = Column(Integer, nullable=False)
    updated_by_id = Column(Integer, nullable=False)

    def to_dict(self):
        obj_dict = super(self.__class__, self).to_dict()

        if self.name:
            obj_dict["name"] = self.name
        if self.display_name:
            obj_dict["display_name"] = self.display_name
        if self.is_enabled:
            obj_dict["is_enabled"] = self.is_enabled

        if self.created_at:
            obj_dict["created_at"] = self.created_at
        if self.updated_at:
            obj_dict["updated_at"] = self.updated_at
        if self.created_by_id:
            obj_dict["created_by_id"] = self.created_by_id
        if self.updated_by_id:
            obj_dict["updated_by_id"] = self.updated_by_id
        return obj_dict

    def from_dict(self, obj_dict):
        super(self.__class__, self).from_dict(obj_dict)

        if "name" in obj_dict:
            self.name = obj_dict["name"]
        if "display_name" in obj_dict:
            self.display_name = obj_dict["display_name"]
        if "is_enabled" in obj_dict:
            self.is_enabled = obj_dict["is_enabled"]

        if "created_at" in obj_dict:
            self.created_at = obj_dict["created_at"]
        if "updated_at" in obj_dict:
            self.updated_at = obj_dict["updated_at"]
        if "created_by_id" in obj_dict:
            self.created_by_id = obj_dict["created_by_id"]
        if "updated_by_id" in obj_dict:
            self.updated_by_id = obj_dict["updated_by_id"]
        return self

    def from_json_dict(self, obj_dict):
        self.from_dict(obj_dict)
        return self

    def to_json_dict(self, include=None):
        if not include: include = []

        obj_dict = self.to_dict()

        obj_dict["created_at"] = self.created_at
        obj_dict["updated_at"] = self.updated_at
        obj_dict["created_by_id"] = self.created_by_id
        obj_dict["updated_by_id"] = self.updated_by_id

        return obj_dict

    @classmethod
    def get_all(cls, db, start=0, count=25, order_by=None):
        query = db.query(cls)
        if not order_by:
            order_by = cls.id.desc()
        return query.order_by(order_by).offset(start).limit(count).all()

    @classmethod
    def get_all_count(cls, db):
        query = db.query(cls)
        return query.count()
