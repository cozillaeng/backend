import datetime
from sqlalchemy import Column, String, Boolean, Enum, DateTime, Integer, ForeignKey, Text, Float
from sqlalchemy.orm import relationship
from model.common import ModelBase
from model.designer import Designer
from model.common import ModelBase
from model.tenant import Tenant
from model.flat import Flat



class Room(ModelBase):
    __tablename__ = "rooms"

    flat_id = Column(Integer, ForeignKey(Flat.id),  nullable=False)
    master_room = Column(Boolean, default=False)

    attached_bathroom = Column(Boolean, default=False)
    balcony = Column(Boolean, default=False)
    bathroom_type = Column(Enum("Western", "Indian"))
    max_occupancy = Column(Enum("Single", "Double"))

    single_occupancy_price = Column(Float)
    double_occupancy_price = Column(Float)
    single_available_occupancy = Column(Integer)
    double_available_occupancy = Column(Integer)
    tenant_id_1 = Column(Integer, ForeignKey(Tenant.id),  nullable=False)
    tenant_id_2 = Column(Integer, ForeignKey(Tenant.id),  nullable=False)

    flat = relationship(Flat, foreign_keys='Room.flat_id', lazy='subquery')
    tenant_1 = relationship(Tenant, foreign_keys='Room.tenant_id_1', lazy="subquery")
    tenant_2 = relationship(Tenant, foreign_keys='Room.tenant_id_2', lazy="subquery")

    created_at = Column(DateTime, nullable=False, default=datetime.datetime.now())
    updated_at = Column(DateTime, nullable=False, default=datetime.datetime.now())
    created_by_id = Column(Integer, nullable=False)
    updated_by_id = Column(Integer, nullable=False)

    def to_dict(self):
        obj_dict = super(self.__class__, self).to_dict()
        if self.flat_id:
            obj_dict["flat_id"] = self.flat_id
        if self.master_room:
            obj_dict["master_room"] = self.master_room
        if self.attached_bathroom:
            obj_dict["attached_bathroom"] = self.attached_bathroom
        if self.balcony:
            obj_dict["balcony"] = self.balcony
        if self.bathroom_type:
            obj_dict["bathroom_type"] = self.bathroom_type
        if self.max_occupancy:
            obj_dict["max_occupancy"] = self.max_occupancy
        if self.max_occupancy:
            obj_dict["max_occupancy"] = self.max_occupancy
        if self.single_occupancy_price:
            obj_dict["single_occupancy_price"] = self.single_occupancy_price
        if self.double_occupancy_price:
            obj_dict["double_occupancy_price"] = self.double_occupancy_price
        if self.single_available_occupancy:
            obj_dict["single_available_occupancy"] = self.single_available_occupancy
        if self.double_available_occupancy:
            obj_dict["double_available_occupancy"] = self.double_available_occupancy
        if self.tenant_id_1:
            obj_dict["tenant_id_1"] = self.tenant_id_1
        if self.tenant_id_2:
            obj_dict["tenant_id_2"] = self.tenant_id_2

        if self.created_at:
            obj_dict["created_at"] = self.created_at
        if self.updated_at:
            obj_dict["updated_at"] = self.updated_at
        if self.created_by_id:
            obj_dict["created_by_id"] = self.created_by_id
        if self.updated_by_id:
            obj_dict["updated_by_id"] = self.updated_by_id
        return obj_dict

    def from_dict(self, obj_dict):
        super(self.__class__, self).from_dict(obj_dict)

        if "flat_id" in obj_dict:
            self.flat_id = obj_dict["flat_id"]
        if "master_room" in obj_dict:
            self.master_room = obj_dict["master_room"]
        if "attached_bathroom" in obj_dict:
            self.attached_bathroom = obj_dict["attached_bathroom"]
        if "balcony" in obj_dict:
            self.balcony = obj_dict["balcony"]
        if "bathroom_type" in obj_dict:
            self.bathroom_type = obj_dict["bathroom_type"]
        if "max_occupancy" in obj_dict:
            self.max_occupancy = obj_dict["max_occupancy"]
        if "max_occupancy" in obj_dict:
            self.max_occupancy = obj_dict["max_occupancy"]
        if "single_occupancy_price" in obj_dict:
            self.single_occupancy_price = obj_dict["single_occupancy_price"]
        if "double_occupancy_price" in obj_dict:
            self.double_occupancy_price = obj_dict["double_occupancy_price"]
        if "single_available_occupancy" in obj_dict:
            self.single_available_occupancy = obj_dict["single_available_occupancy"]
        if "double_available_occupancy" in obj_dict:
            self.double_available_occupancy = obj_dict["double_available_occupancy"]
        if "tenant_id_1" in obj_dict:
            self.tenant_id_1 = obj_dict["tenant_id_1"]
        if "tenant_id_2" in obj_dict:
            self.tenant_id_2 = obj_dict["tenant_id_2"]

        if "created_at" in obj_dict:
            self.created_at = obj_dict["created_at"]
        if "updated_at" in obj_dict:
            self.updated_at = obj_dict["updated_at"]
        if "created_by_id" in obj_dict:
            self.created_by_id = obj_dict["created_by_id"]
        if "updated_by_id" in obj_dict:
            self.updated_by_id = obj_dict["updated_by_id"]
        return self

    def from_json_dict(self, obj_dict):
        self.from_dict(obj_dict)
        if "flat" in obj_dict:
            self.flat_id = Flat().from_dict(obj_dict["flat"]).id
        if "tenant_1" in obj_dict:
            self.tenant_id_1 = Tenant().from_dict(obj_dict["tenant_1"]).id
        if "tenant_2" in obj_dict:
            self.tenant_id_2 = Tenant().from_dict(obj_dict["tenant_2"]).id
        return self

    def to_json_dict(self, include=None):
        if not include: include = []

        obj_dict = self.to_dict()

        if "flat" in include and self.flat:
            obj_dict["flat"] = self.flat.to_dict()
        else:
            obj_dict["flat"] = {"id": self.flat_id}

        if "tenant_1" in include and self.tenant_1:
            obj_dict["tenant_1"] = self.tenant_1.to_dict()
        else:
            obj_dict["tenant_1"] = {"id": self.tenant_id_1}
        if "tenant_2" in include and self.tenant_2:
            obj_dict["tenant_2"] = self.tenant_2.to_dict()
        else:
            obj_dict["tenant_2"] = {"id": self.tenant_id_2}

        obj_dict["created_at"] = self.created_at
        obj_dict["updated_at"] = self.updated_at
        obj_dict["created_by_id"] = self.created_by_id
        obj_dict["updated_by_id"] = self.updated_by_id

        if "flat_id" in obj_dict: del obj_dict["flat_id"]
        if "tenant_id_1" in obj_dict: del obj_dict["tenant_id_1"]
        if "tenant_id_2" in obj_dict: del obj_dict["tenant_id_2"]

        return obj_dict

    @classmethod
    def get_all(cls, db, start=0, count=25, flat_id=None, is_enabled=None, order_by=None):
        query = db.query(cls)
        if is_enabled is not None:
            query=query.filter(cls.is_enabled == is_enabled)
        if flat_id:
            query = query.filter(cls.flat_id == flat_id)
        if not order_by:
            order_by = cls.id.desc()
        return query.order_by(order_by).offset(start).limit(count).all()

    @classmethod
    def get_all_count(cls, db, flat_id=None, is_enabled=None):
        query = db.query(cls)
        if is_enabled is not None:
            query=query.filter(cls.is_enabled == is_enabled)
        if flat_id:
            query = query.filter(cls.flat_id == flat_id)
        return query.count()
