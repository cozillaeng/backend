import datetime
from sqlalchemy import Column, String, Boolean, Enum, DateTime, Integer, ForeignKey, Text, Float
from sqlalchemy.orm import relationship
from model.common import ModelBase
from model.designer import Designer
from model.owner import Owner
from model.common import ModelBase
from model.city import City
from model.location import Location
from model.society import Society
from model.home_amenities import HomeAmenities



class Flat(ModelBase):
    __tablename__ = "flats"

    owner_id = Column(Integer, ForeignKey(Owner.id), nullable=False)
    bedroom = Column(Enum("1 BHK", "1.5 BHK", "2 BHK", "2.5 BHK", "3 BHK", "3.5 BHK", "4 BHK", "4.5 BHK", "5 BHK", "5.5 BHK", "6 BHK"))
    gender = Column(Enum("MALE", "FEMALE"), nullable=False)
    owner_agreement_id = Column(Integer)
    society_id = Column(Integer, ForeignKey(Society.id))
    home_amenities_id = Column(Integer, ForeignKey(HomeAmenities.id))
    geolat = Column(Float)
    geolong = Column(Float)

    location_id = Column(Integer, ForeignKey(Location.id), nullable=False)
    address = Column(String(100))
    city_id = Column(Integer, ForeignKey(City.id), nullable=False)
    pincode = Column(String(10))

    city = relationship(City, foreign_keys='Flat.city_id',lazy="joined")
    owner = relationship(Owner, foreign_keys='Flat.owner_id', lazy="joined")
    location = relationship(Location, foreign_keys='Flat.location_id', lazy="joined")
    society = relationship(Society, foreign_keys='Flat.society_id', lazy="joined")
    home_amenities = relationship(HomeAmenities, foreign_keys='Flat.home_amenities_id', lazy="joined")

    created_at = Column(DateTime, nullable=False, default=datetime.datetime.now())
    updated_at = Column(DateTime, nullable=False, default=datetime.datetime.now())
    created_by_id = Column(Integer, nullable=False)
    updated_by_id = Column(Integer, nullable=False)

    def to_dict(self):
        obj_dict = super(self.__class__, self).to_dict()
        if self.owner_id:
            obj_dict["owner_id"] = self.owner_id
        if self.bedroom:
            obj_dict["bedroom"] = self.bedroom
        if self.gender:
            obj_dict["gender"] = self.gender
        if self.owner_agreement_id:
            obj_dict["owner_agreement_id"] = self.owner_agreement_id
        if self.society_id:
            obj_dict["society_id"] = self.society_id
        if self.home_amenities_id:
            obj_dict["home_amenities_id"] = self.home_amenities_id
        if self.geolat:
            obj_dict["geolat"] = self.geolat
        if self.geolong:
            obj_dict["geolong"] = self.geolong
        if self.location_id:
            obj_dict["location_id"] = self.location_id
        if self.address:
            obj_dict["address"] = self.address
        if self.city_id:
            obj_dict["city_id"] = self.city_id
        if self.pincode:
            obj_dict["pincode"] = self.pincode

        if self.created_at:
            obj_dict["created_at"] = self.created_at
        if self.updated_at:
            obj_dict["updated_at"] = self.updated_at
        if self.created_by_id:
            obj_dict["created_by_id"] = self.created_by_id
        if self.updated_by_id:
            obj_dict["updated_by_id"] = self.updated_by_id
        return obj_dict

    def from_dict(self, obj_dict):
        super(self.__class__, self).from_dict(obj_dict)

        if "owner_id" in obj_dict:
            self.owner_id = obj_dict["owner_id"]
        if "bedroom" in obj_dict:
            self.bedroom = obj_dict["bedroom"]
        if "gender" in obj_dict:
            self.gender = obj_dict["gender"]
        if "owner_agreement_id" in obj_dict:
            self.owner_agreement_id = obj_dict["owner_agreement_id"]
        if "society_id" in obj_dict:
            self.society_id = obj_dict["society_id"]
        if "home_amenities_id" in obj_dict:
            self.home_amenities_id = obj_dict["home_amenities_id"]
        if "geolat" in obj_dict:
            self.geolat = obj_dict["geolat"]
        if "geolong" in obj_dict:
            self.geolong = obj_dict["geolong"]
        if "location_id" in obj_dict:
            self.location_id = obj_dict["location_id"]
        if "address" in obj_dict:
            self.address = obj_dict["address"]
        if "city_id" in obj_dict:
            self.city_id = obj_dict["city_id"]
        if "pincode" in obj_dict:
            self.pincode = obj_dict["pincode"]

        if "created_at" in obj_dict:
            self.created_at = obj_dict["created_at"]
        if "updated_at" in obj_dict:
            self.updated_at = obj_dict["updated_at"]
        if "created_by_id" in obj_dict:
            self.created_by_id = obj_dict["created_by_id"]
        if "updated_by_id" in obj_dict:
            self.updated_by_id = obj_dict["updated_by_id"]
        return self

    def from_json_dict(self, obj_dict):
        self.from_dict(obj_dict)
        if "city" in obj_dict:
            self.city_id = City().from_dict(obj_dict["city"]).id
        if "owner" in obj_dict:
            self.owner_id = Owner().from_dict(obj_dict["owner"]).id
        if "location" in obj_dict:
            self.location_id = Location().from_dict(obj_dict["location"]).id
        if "society" in obj_dict:
            self.society_id = Society().from_dict(obj_dict["society"]).id
        if "home_amenities" in obj_dict:
            self.home_amenities_id = HomeAmenities().from_dict(obj_dict["home_amenities"]).id

        return self

    def to_json_dict(self, include=None):
        if not include: include = []

        obj_dict = self.to_dict()

        if "city" in include and self.city:
            obj_dict["city"] = self.city.to_dict()
        else:
            obj_dict["city"] = {"id": self.city_id}

        if "owner" in include and self.owner:
            obj_dict["owner"] = self.owner.to_dict()
        else:
            obj_dict["owner"] = {"id": self.owner_id}

        if "location" in include and self.location:
            obj_dict["location"] = self.location.to_dict()
        else:
            obj_dict["location"] = {"id": self.location_id}

        if "society" in include and self.society:
            obj_dict["society"] = self.society.to_dict()
        else:
            obj_dict["society"] = {"id": self.society_id}

        if "home_amenities" in include and self.home_amenities:
            obj_dict["home_amenities"] = self.home_amenities.to_dict()
        else:
            obj_dict["home_amenities"] = {"id": self.home_amenities_id}


        obj_dict["created_at"] = self.created_at
        obj_dict["updated_at"] = self.updated_at
        obj_dict["created_by_id"] = self.created_by_id
        obj_dict["updated_by_id"] = self.updated_by_id

        if "city_id" in obj_dict: del obj_dict["city_id"]
        if "owner_id" in obj_dict: del obj_dict["owner_id"]
        if "location_id" in obj_dict: del obj_dict["location_id"]
        if "society_id" in obj_dict: del obj_dict["society_id"]
        if "home_amenities_id" in obj_dict: del obj_dict["home_amenities_id"]

        return obj_dict

    @classmethod
    def get_all(cls, db, start=0, count=25, order_by=None):
        query = db.query(cls)
        if not order_by:
            order_by = cls.id.desc()
        return query.order_by(order_by).offset(start).limit(count).all()

    @classmethod
    def get_all_count(cls, db):
        query = db.query(cls)
        return query.count()
