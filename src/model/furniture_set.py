import datetime
from sqlalchemy import Column, String, Boolean, Enum, DateTime, Integer, ForeignKey, Text, Float
from sqlalchemy.orm import relationship
from model.common import ModelBase
from model.designer import Designer
from model.common import ModelBase, City
from model.furniture import Furniture


class FurnitureSet(ModelBase):
    __tablename__ = "furniture_sets"

    furniture_id_1 = Column(Integer)
    furniture_id_2 = Column(Integer)
    furniture_id_3 = Column(Integer)
    furniture_id_4 = Column(Integer)
    furniture_id_5 = Column(Integer)
    furniture_id_6 = Column(Integer)
    furniture_id_7 = Column(Integer)
    furniture_id_8 = Column(Integer)
    furniture_id_9 = Column(Integer)
    furniture_id_10 = Column(Integer)

    created_at = Column(DateTime, nullable=False, default=datetime.datetime.now())
    updated_at = Column(DateTime, nullable=False, default=datetime.datetime.now())
    created_by_id = Column(Integer, nullable=False)
    updated_by_id = Column(Integer, nullable=False)

    def to_dict(self):
        obj_dict = super(self.__class__, self).to_dict()

        if self.furniture_id_1:
            obj_dict["furniture_id_1"] = self.furniture_id_1
        if self.furniture_id_2:
            obj_dict["furniture_id_2"] = self.furniture_id_2
        if self.furniture_id_3:
            obj_dict["furniture_id_3"] = self.furniture_id_3
        if self.furniture_id_4:
            obj_dict["furniture_id_4"] = self.furniture_id_4
        if self.furniture_id_5:
            obj_dict["furniture_id_5"] = self.furniture_id_5
        if self.furniture_id_6:
            obj_dict["furniture_id_6"] = self.furniture_id_6
        if self.furniture_id_7:
            obj_dict["furniture_id_7"] = self.furniture_id_7
        if self.furniture_id_8:
            obj_dict["furniture_id_8"] = self.furniture_id_8
        if self.furniture_id_9:
            obj_dict["furniture_id_9"] = self.furniture_id_9
        if self.furniture_id_10:
            obj_dict["furniture_id_10"] = self.furniture_id_10

        if self.created_at:
            obj_dict["created_at"] = self.created_at
        if self.updated_at:
            obj_dict["updated_at"] = self.updated_at
        if self.created_by_id:
            obj_dict["created_by_id"] = self.created_by_id
        if self.updated_by_id:
            obj_dict["updated_by_id"] = self.updated_by_id
        return obj_dict

    def from_dict(self, obj_dict):
        super(self.__class__, self).from_dict(obj_dict)

        if "furniture_id_1" in obj_dict:
            self.furniture_id_1 = obj_dict["furniture_id_1"]
        if "furniture_id_2" in obj_dict:
            self.furniture_id_2 = obj_dict["furniture_id_2"]
        if "furniture_id_3" in obj_dict:
            self.furniture_id_3 = obj_dict["furniture_id_3"]
        if "furniture_id_4" in obj_dict:
            self.furniture_id_4 = obj_dict["furniture_id_4"]
        if "furniture_id_5" in obj_dict:
            self.furniture_id_5 = obj_dict["furniture_id_5"]
        if "furniture_id_6" in obj_dict:
            self.furniture_id_6 = obj_dict["furniture_id_6"]
        if "furniture_id_7" in obj_dict:
            self.furniture_id_7 = obj_dict["furniture_id_7"]
        if "furniture_id_8" in obj_dict:
            self.furniture_id_8 = obj_dict["furniture_id_8"]
        if "furniture_id_9" in obj_dict:
            self.furniture_id_9 = obj_dict["furniture_id_9"]
        if "furniture_id_10" in obj_dict:
            self.furniture_id_10 = obj_dict["furniture_id_10"]


        if "created_at" in obj_dict:
            self.created_at = obj_dict["created_at"]
        if "updated_at" in obj_dict:
            self.updated_at = obj_dict["updated_at"]
        if "created_by_id" in obj_dict:
            self.created_by_id = obj_dict["created_by_id"]
        if "updated_by_id" in obj_dict:
            self.updated_by_id = obj_dict["updated_by_id"]
        return self

    def from_json_dict(self, obj_dict):
        self.from_dict(obj_dict)
        return self

    def to_json_dict(self, include=None):
        if not include: include = []

        obj_dict = self.to_dict()

        obj_dict["created_at"] = self.created_at
        obj_dict["updated_at"] = self.updated_at
        obj_dict["created_by_id"] = self.created_by_id
        obj_dict["updated_by_id"] = self.updated_by_id

        return obj_dict

    @classmethod
    def get_all(cls, db, start=0, count=25, order_by=None):
        query = db.query(cls)
        if not order_by:
            order_by = cls.id.desc()
        return query.order_by(order_by).offset(start).limit(count).all()

    @classmethod
    def get_all_count(cls, db):
        query = db.query(cls)
        return query.count()
