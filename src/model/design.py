from model.images import Images
from util.image_url_util import DesignImageURL

__author__ = 'ramakrishnag'

import datetime
from sqlalchemy import Column, String, Boolean, DateTime, Integer, ForeignKey, func, Enum, Float
from sqlalchemy.orm import relationship
from model.common import ModelBase
from util.common import DictUtil
from model.designer import Designer
from model.project import Project

class Design(ModelBase):
    __tablename__ = "designs"

    designer_id = Column(Integer,ForeignKey(Designer.id))
    display_name = Column(String(255), nullable=False)
    project_id = Column(Integer, ForeignKey(Project.id))
    created_at = Column(DateTime, nullable=False, default=datetime.datetime.now())
    updated_at = Column(DateTime, nullable=False, default=datetime.datetime.now())
    created_by_id = Column(Integer, nullable=False)
    updated_by_id = Column(Integer, nullable=False)
    is_enabled = Column(Boolean, nullable=False)
    design_image_filename = Column(String(255))
    cover_image_filename = Column(String(255))
    #designer = relationship(Designer, lazy="joined")
    #project = relationship(Project, lazy="joined")
    design_type = Column(Enum("TEMPLATE","CONCRETE"), nullable=False)
    location_x = Column(Float,nullable=False)
    location_y = Column(Float,nullable=False)
    location_z = Column(Float,nullable=False)
    rotation_x = Column(Float,nullable=False)
    rotation_y = Column(Float,nullable=False)
    rotation_z = Column(Float,nullable=False)
    scale_x = Column(Float,nullable=False)
    scale_y = Column(Float,nullable=False)
    scale_z = Column(Float,nullable=False)
    visible = Column(Boolean)
    uuid = Column(String(64))
    selected_floor_uuid = Column(String(64))
    images = relationship(Images, lazy="joined", primaryjoin="and_(Design.id == Images.owner_id,Images.owner_type=='DESIGN')",
                          foreign_keys="[Images.owner_id,Images.owner_type]")
   
    def to_dict(self):
        obj_dict = super(self.__class__, self).to_dict()
        if self.designer_id:
            obj_dict["designer_id"] = self.designer_id
        if self.display_name:
            obj_dict["display_name"] = self.display_name
        if self.project_id:
            obj_dict["project_id"] = self.project_id
        if self.is_enabled:
            obj_dict["is_enabled"] = self.is_enabled
        if self.created_at:
            obj_dict["created_at"] = self.created_at
        if self.updated_at:
            obj_dict["updated_at"] = self.updated_at
        if self.created_by_id:
            obj_dict["created_by_id"] = self.created_by_id
        if self.updated_by_id:
            obj_dict["updated_by_id"] = self.updated_by_id
        if self.design_image_filename:
            #obj_dict["design_image_filename"] = "/designs/design_images/" + self.id + "/" + self.design_image_filename
            obj_dict["design_image_filename"] = self.design_image_filename
        if self.cover_image_filename:
            #obj_dict["cover_image_filename"] = "/designs/cover_images/" + self.id + "/" + self.cover_image_filename
            obj_dict["cover_image_filename"] = self.cover_image_filename
        if self.design_type:
            obj_dict["design_type"] = self.design_type
        if self.visible:
            obj_dict["visible"] = self.visible
        if self.uuid:
            obj_dict["uuid"] = self.uuid
        if self.selected_floor_uuid:
            obj_dict["selected_floor_uuid"] = self.selected_floor_uuid
        obj_dict["type"] = "Livhome"

        return obj_dict

    def from_dict(self, obj_dict):
        super(self.__class__, self).from_dict(obj_dict)
        if "designer_id" in obj_dict:
            self.designer_id = obj_dict["designer_id"]
        if "display_name" in obj_dict:
            self.display_name = obj_dict["display_name"]
        if "project_id" in obj_dict:
            self.project_id = obj_dict["project_id"]
        if "is_enabled" in obj_dict:
            self.is_enabled = obj_dict["is_enabled"]
        if "created_by_id" in obj_dict:
            self.created_by_id = obj_dict["created_by_id"]
        if "updated_by_id" in obj_dict:
            self.updated_by_id = obj_dict["updated_by_id"]
        if "design_image_filename" in obj_dict:
            self.design_image_filename = obj_dict["design_image_filename"]
        if "cover_image_filename" in obj_dict:
            self.cover_image_filename = obj_dict["cover_image_filename"]
        if "design_type" in obj_dict:
            self.design_type = obj_dict["design_type"]
        if "visible" in obj_dict:
            self.visible = obj_dict["visible"]
        if "uuid" in obj_dict:
            self.uuid = obj_dict["uuid"]
        if "selected_floor_uuid" in obj_dict:
            self.selected_floor_uuid = obj_dict["selected_floor_uuid"]

        return self

    def to_json_dict(self, include=None):
        if not include:
            include = []
        obj_dict = self.to_dict()

        #if "designer" in include and self.designer:
        #    obj_dict["designer"] = self.designer.to_dict()
        #else:
        #    obj_dict["designer"] = {"id": self.designer_id}

        #if "project" in include and self.project:
        #    obj_dict["project"] = self.project.to_dict()
        #else:
        #    obj_dict["project"] = {"id": self.project_id}

        location = {}
        location["x"] = self.location_x
        location["y"] = self.location_y
        location["z"] = self.location_z
        obj_dict["location"] = location

        rotation = {}
        rotation["x"] = self.rotation_x
        rotation["y"] = self.rotation_y
        rotation["z"] = self.rotation_z
        obj_dict["rotation"] = rotation

        scale = {}
        scale["x"] = self.scale_x
        scale["y"] = self.scale_y
        scale["z"] = self.scale_z
        obj_dict["scale"] = scale


        if "images" in include and self.images:
            obj_dict["images"] = []
            image_url_context = DesignImageURL(self.id)
            for image in self.images:
                image_json = image.to_json_dict(image_url_context)
                image_json["design"] = {"id" : image_json["owner_id"]}
                if "owner_type" in image_json: del image_json["owner_type"]
                if "owner_id" in image_json: del image_json["owner_id"]
                obj_dict["images"].append(image_json)

        return obj_dict

    def from_json_dict(self, obj_dict):
        self.from_dict(obj_dict)
        #if "designer" in obj_dict:
        #    self.designer_id = Designer().from_dict(obj_dict["designer"]).id
        #if "project" in obj_dict:
        #    self.project_id = Project().from_dict(obj_dict["project"]).id

        if "location" in obj_dict:
            location = obj_dict["location"]
            self.location_x = location["x"]
            self.location_y = location["y"]
            self.location_z = location["z"]

        if "rotation" in obj_dict:
            rotation = obj_dict["rotation"]
            self.rotation_x = rotation["x"]
            self.rotation_y = rotation["y"]
            self.rotation_z = rotation["z"]

        if "scale" in obj_dict:
            scale = obj_dict["scale"]
            self.scale_x = scale["x"]
            self.scale_y = scale["y"]
            self.scale_z = scale["z"]


        return self

    @classmethod
    def get_all_designs(cls, db, start=0, count=5, is_enabled=None, created_by_id=None, project_id=None,
                        designer_id=None, order_by=None):
        query = db.query(cls)

        if is_enabled is not None:
            query = query.filter(cls.is_enabled == is_enabled)
        if created_by_id:
            query = query.filter(cls.created_by_id == created_by_id)
        if project_id:
            query = query.filter(cls.project_id == project_id)
        if designer_id:
            query = query.filter(cls.designer_id == designer_id)

        if not order_by:
            order_by = cls.id.desc()
        return query.order_by(order_by).offset(start).limit(count).all()

    @classmethod
    def get_all_count(cls, db, is_enabled=None, created_by_id=None, project_id=None, designer_id=None):
        query = db.query(cls)
        if is_enabled is not None:
            query = query.filter(cls.is_enabled == is_enabled)
        if created_by_id is not None:
            query = query.filter(cls.created_by_id == created_by_id)
        if project_id:
            query = query.filter(cls.project_id == project_id)
        if designer_id:
            query = query.filter(cls.designer_id == designer_id)
        return query.count()

    @classmethod
    def create(self, db, design_dict):
        DictUtil.delete_keys(design_dict, "id")
        design = Design().from_dict(design_dict)
        design.created_at = design.updated_at = datetime.datetime.now()
        db.add(db,design)
        return design

    @classmethod
    def get_all(cls, db, start=0, count=25, is_approved=None, source=None, order_by=None):
        query = db.query(cls)
        if is_approved is not None:
            query.filter(cls.is_approved == is_approved)
        if source:
            query.filter(cls.source == source)
        if not order_by:
            order_by = cls.id.desc()
        return query.order_by(order_by).offset(start).limit(count).all()

    @classmethod
    def get_by_project_id(cls, db, project_id):
        return db.query(cls).filter(cls.project_id == project_id).all()

    def clone(self,source):
        source_dict = source.to_json_dict()
        source_dict["id"] = None
        source_dict["created_at"] = None
        source_dict["updated_at"] = None
        self.from_json_dict(source_dict)

        return self
