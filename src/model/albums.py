import datetime
from sqlalchemy import Column, String, DateTime, Integer, Text, Enum
from model.common import ModelBase
from sqlalchemy.orm import relationship
from model.images import Images

__author__ = 'Govind'

ALBUM_IMAGES_OWNER_TYPE = "ALBUM"

class Album(ModelBase):
    __tablename__ = "albums"

    display_name = Column(String(255), nullable=False)
    description = Column(Text, nullable=False)
    created_at = Column(DateTime, nullable=False, default=datetime.datetime.now())
    owner_id = Column(Integer,nullable=False)
    owner_type = Column(Enum("PROJECT"), nullable=False)
    images = relationship(Images, lazy="joined", primaryjoin="and_(Album.id == Images.owner_id,Images.owner_type=='ALBUM')",
                          foreign_keys="[Images.owner_id,Images.owner_type]")

    def to_dict(self):
        obj_dict = super(self.__class__, self).to_dict()
        if self.owner_id:
            obj_dict["owner_id"] = self.owner_id
        if self.display_name :
            obj_dict["display_name"] = self.display_name
        if self.description:
            obj_dict["description"] = self.description
        if self.created_at:
            obj_dict["created_at"] = self.created_at
        if self.owner_type:
            obj_dict["owner_type"] = self.owner_type

        return obj_dict

    def from_dict(self, obj_dict):
        super(self.__class__, self).from_dict(obj_dict)
        if "owner_id" in obj_dict:
            self.owner_id = obj_dict["owner_id"]
        if "display_name" in obj_dict:
            self.display_name = obj_dict["display_name"]
        if "description" in obj_dict:
            self.description = obj_dict["description"]
        if "created_at" in obj_dict:
            self.created_at = obj_dict["created_at"]
        if "owner_type" in obj_dict:
            self.owner_type = obj_dict["owner_type"].upper()

        return self

    def to_json_dict(self, image_url_context=None, include=None):
        if not include:
            include = []
        obj_dict = self.to_dict()

        if self.images:
            obj_dict["images"] = []
            for image in self.images:
                image_json = image.to_json_dict(image_url_context)
                image_json["album"] = {"id" : image_json["owner_id"]}
                if "owner_type" in image_json: del image_json["owner_type"]
                if "owner_id" in image_json: del image_json["owner_id"]
                obj_dict["images"].append(image_json)
        return obj_dict

    def from_json_dict(self, obj_dict):
        self.from_dict(obj_dict)
        return self

    @classmethod
    def get_all_albums(cls, db, start=0, count=5, owner_id=None, owner_type=None, order_by=None):
        query = db.query(cls)

        if owner_id:
            query = query.filter(cls.owner_id == owner_id)

        if owner_type:
            query = query.filter(cls.owner_type == owner_type)

        if not order_by:
            order_by = cls.id.desc()

        return query.order_by(order_by).offset(start).limit(count).all()

    @classmethod
    def get_all_count(cls, db, owner_id=None, owner_type=None):
        query = db.query(cls)

        if owner_id:
            query = query.filter(cls.owner_id == owner_id)
        if owner_type:
            query = query.filter(cls.owner_type == owner_type)

        return query.count()

    @classmethod
    def get_by_display_name(cls, db, display_name, owner_id, owner_type):
        query = db.query(cls)
        query = query.filter(cls.owner_id == owner_id)
        query = query.filter(cls.owner_type == owner_type)
        query = query.filter(cls.display_name == display_name)
        return query.first()
