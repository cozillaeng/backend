__author__ = 'ramakrishnag'


import datetime

from sqlalchemy import Column, Integer, DateTime, Float, ForeignKey, String
from sqlalchemy.orm import relationship
from model.common import ModelBase
from model.product_sku import ProductSku

class CatalogueProvider(ModelBase):
    __tablename__ = 'catalogue_providers'

    created_at = Column(DateTime, nullable=False, default=datetime.datetime.now())
    updated_at = Column(DateTime, nullable=False, default=datetime.datetime.now())
    name = Column(String(256),nullable=False)
    display_name = Column(String(256),nullable=False)
    
    def to_dict(self):
        obj_dict = super(self.__class__, self).to_dict()
        if self.name:
            obj_dict["name"] = self.name
        if self.display_name:
            obj_dict["display_name"] = self.display_name
       
        return obj_dict

    def from_dict(self, obj_dict):
        super(self.__class__, self).from_dict(obj_dict)
        if "name" in obj_dict:
            self.name = obj_dict["name"]
        if "display_name" in obj_dict:
            self.display_name = obj_dict["display_name"]
        
        return self

    def to_json_dict(self, include=None):
        if not include: include = []
        obj_dict = self.to_dict()

        return obj_dict

    def from_json_dict(self, obj_dict):
        self.from_dict(obj_dict)

        return self
