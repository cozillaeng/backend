import datetime
from sqlalchemy import Column, String, Boolean, Enum, DateTime, Integer, ForeignKey, Text, Float
from sqlalchemy.orm import relationship
from model.common import ModelBase
from model.designer import Designer
from model.common import ModelBase
from model.city import City
from model.location import Location



class Society(ModelBase):
    __tablename__ = "societies"

    name = Column(String(50))
    type = Column(Enum("GATED", "NORMAL"), nullable=False)
    location_id = Column(ForeignKey(Location.id), nullable=False)
    address = Column(String(100), nullable=False)
    city_id = Column(Integer, ForeignKey(City.id), nullable=False)
    pincode = Column(String(20), nullable=False)
    geolat = Column(Float)
    geolong = Column(Float)
    no_of_wings = Column(Integer)
    flats_in_a_wing = Column(Integer)
    total_flats = Column(Integer)
    floors_in_a_wing = Column(Integer)
    security_type = Column(Enum("NONE","1_SPOT","2_SPOT"))
    security_cameras = Column(Boolean, default=False)

    lift = Column(Boolean, default=False)
    swimming_pool = Column(Boolean, default=False)
    gym = Column(Boolean, default=False)
    badminton = Column(Boolean, default=False)
    playing_area = Column(Boolean, default=False)
    club_house = Column(Boolean, default=False)
    power_backup = Column(Boolean, default=False)
    lawn_tennis = Column(Boolean, default=False)
    table_tennis = Column(Boolean, default=False)
    basket_ball = Column(Boolean, default=False)
    gas_pipeline = Column(Boolean, default=False)

    city = relationship(City,foreign_keys='Society.city_id', lazy="joined")
    location = relationship(Location,foreign_keys='Society.location_id',lazy="joined" )

    created_at = Column(DateTime, nullable=False, default=datetime.datetime.now())
    updated_at = Column(DateTime, nullable=False, default=datetime.datetime.now())
    created_by_id = Column(Integer, nullable=False)
    updated_by_id = Column(Integer, nullable=False)

    def to_dict(self):
        obj_dict = super(self.__class__, self).to_dict()
        if self.name:
            obj_dict["name"] = self.name
        if self.type:
            obj_dict["type"] = self.type
        if self.location_id:
            obj_dict["location_id"] = self.location_id
        if self.address:
            obj_dict["address"] = self.address
        if self.city_id:
            obj_dict["city_id"] = self.city_id
        if self.pincode:
            obj_dict["pincode"] = self.pincode
        if self.geolat:
            obj_dict["geolat"] = self.geolat
        if self.geolong:
            obj_dict["geolong"] = self.geolong
        if self.no_of_wings:
            obj_dict["no_of_wings"] = self.no_of_wings
        if self.flats_in_a_wing:
            obj_dict["flats_in_a_wing"] = self.flats_in_a_wing
        if self.total_flats:
            obj_dict["total_flats"] = self.total_flats
        if self.floors_in_a_wing:
            obj_dict["floors_in_a_wing"] = self.floors_in_a_wing
        if self.security_type:
            obj_dict["security_type"] = self.security_type
        if self.security_cameras:
            obj_dict["security_cameras"] = self.security_cameras
        if self.lift:
            obj_dict["lift"] = self.lift
        if self.swimming_pool:
            obj_dict["swimming_pool"] = self.swimming_pool
        if self.gym:
            obj_dict["gym"] = self.gym
        if self.badminton:
            obj_dict["badminton"] = self.badminton
        if self.playing_area:
            obj_dict["playing_area"] = self.playing_area

        if self.club_house:
            obj_dict["club_house"] = self.club_house
        if self.power_backup:
            obj_dict["power_backup"] = self.power_backup
        if self.lawn_tennis:
            obj_dict["lawn_tennis"] = self.lawn_tennis
        if self.table_tennis:
            obj_dict["table_tennis"] = self.table_tennis
        if self.basket_ball:
            obj_dict["basket_ball"] = self.basket_ball
        if self.gas_pipeline:
            obj_dict["gas_pipeline"] = self.gas_pipeline

        if self.created_at:
            obj_dict["created_at"] = self.created_at
        if self.updated_at:
            obj_dict["updated_at"] = self.updated_at
        if self.created_by_id:
            obj_dict["created_by_id"] = self.created_by_id
        if self.updated_by_id:
            obj_dict["updated_by_id"] = self.updated_by_id
        return obj_dict

    def from_dict(self, obj_dict):
        super(self.__class__, self).from_dict(obj_dict)

        if "name" in obj_dict:
            self.name = obj_dict["name"]
        if "type" in obj_dict:
            self.type = obj_dict["type"]
        if "location_id" in obj_dict:
            self.location_id = obj_dict["location_id"]
        if "address" in obj_dict:
            self.address = obj_dict["address"]
        if "city_id" in obj_dict:
            self.city_id = obj_dict["city_id"]
        if "pincode" in obj_dict:
            self.pincode = obj_dict["pincode"]
        if "geolat" in obj_dict:
            self.geolat = obj_dict["geolat"]
        if "geolong" in obj_dict:
            self.geolong = obj_dict["geolong"]
        if "no_of_wings" in obj_dict:
            self.no_of_wings = obj_dict["no_of_wings"]
        if "flats_in_a_wing" in obj_dict:
            self.flats_in_a_wing = obj_dict["flats_in_a_wing"]
        if "total_flats" in obj_dict:
            self.total_flats = obj_dict["total_flats"]
        if "floors_in_a_wing" in obj_dict:
            self.floors_in_a_wing = obj_dict["floors_in_a_wing"]
        if "security_type" in obj_dict:
            self.security_type = obj_dict["security_type"]
        if "security_cameras" in obj_dict:
            self.security_cameras = obj_dict["security_cameras"]
        if "lift" in obj_dict:
            self.lift = obj_dict["lift"]
        if "swimming_pool" in obj_dict:
            self.swimming_pool = obj_dict["swimming_pool"]
        if "gym" in obj_dict:
            self.gym = obj_dict["gym"]
        if "badminton" in obj_dict:
            self.badminton = obj_dict["badminton"]
        if "playing_area" in obj_dict:
            self.playing_area = obj_dict["playing_area"]

        if "club_house" in obj_dict:
            self.club_house = obj_dict["club_house"]
        if "power_backup" in obj_dict:
            self.power_backup = obj_dict["power_backup"]
        if "lawn_tennis" in obj_dict:
            self.lawn_tennis = obj_dict["lawn_tennis"]
        if "table_tennis" in obj_dict:
            self.table_tennis = obj_dict["table_tennis"]
        if "basket_ball" in obj_dict:
            self.basket_ball = obj_dict["basket_ball"]
        if "gas_pipeline" in obj_dict:
            self.gas_pipeline = obj_dict["gas_pipeline"]

        if "created_at" in obj_dict:
            self.created_at = obj_dict["created_at"]
        if "updated_at" in obj_dict:
            self.updated_at = obj_dict["updated_at"]
        if "created_by_id" in obj_dict:
            self.created_by_id = obj_dict["created_by_id"]
        if "updated_by_id" in obj_dict:
            self.updated_by_id = obj_dict["updated_by_id"]
        return self

    def from_json_dict(self, obj_dict):
        self.from_dict(obj_dict)
        if "city" in obj_dict:
            self.city_id = City().from_dict(obj_dict["city"]).id
        if "location" in obj_dict:
            self.location_id = Location().from_dict(obj_dict["location"]).id

        return self

    def to_json_dict(self, include=None):
        if not include: include = []

        obj_dict = self.to_dict()
        if "city" in include and self.city:
            obj_dict["city"] = self.city.to_dict()
        else:
            obj_dict["city"] = {"id": self.city_id}

        if "location" in include and self.location:
            obj_dict["location"] = self.location.to_dict()
        else:
            obj_dict["location"] = {"id": self.location_id}

        obj_dict["created_at"] = self.created_at
        obj_dict["updated_at"] = self.updated_at
        obj_dict["created_by_id"] = self.created_by_id
        obj_dict["updated_by_id"] = self.updated_by_id

        if "city_id" in obj_dict: del obj_dict["city_id"]
        if "location_id" in obj_dict: del obj_dict["location_id"]

        return obj_dict

    @classmethod
    def get_all(cls, db, start=0, count=25, order_by=None):
        query = db.query(cls)
        if not order_by:
            order_by = cls.id.desc()
        return query.order_by(order_by).offset(start).limit(count).all()

    @classmethod
    def get_all_count(cls, db):
        query = db.query(cls)
        return query.count()
