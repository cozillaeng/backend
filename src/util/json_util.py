import json
import datetime
from flask import Response
from conf import MIME_TYPE_JSON
from model.common import JSONSerializable
from util.common import DatetimeUtil


def json_response(obj, include=None, status=200, mimetype=MIME_TYPE_JSON):
    if isinstance(obj, basestring):
        response = obj
    elif isinstance(obj, list):
        final_obj = []
        for o in obj:
            if isinstance(o, JSONSerializable):
                final_obj.append(o.to_json_dict(include))
            else:
                final_obj.append(o)
        response = dump_json(final_obj)
    elif isinstance(obj, JSONSerializable):
        response = dump_json(obj.to_json_dict(include))
    else:
        response = dump_json(obj)
    return Response(response=response, mimetype=mimetype, status=status)


def __serialize_object(obj):
    if isinstance(obj, datetime.datetime):
        return DatetimeUtil.dt_to_iso(obj)
    elif issubclass(obj.__class__, JSONSerializable):
        return obj.to_json_dict()
    else:
        return iter(obj)


def dump_json(obj):
    return json.dumps(obj, default=__serialize_object)


def load_json(obj):
    return json.loads(obj, default=__serialize_object)
