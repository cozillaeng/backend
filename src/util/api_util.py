from exception import InvalidValueException


class ApiFilters(object):
    def __init__(self, filter_str):
        self.filters = {}
        self.keys = []
        if not filter_str:
            return
        segments = filter_str.split(";")
        for segment in segments:
            segment = segment.strip()
            if not segment:
                continue
            parts = segment.split(":")
            if len(parts) < 2:
                raise InvalidValueException("Invalid filters format")
            key = parts[0]
            values = parts[1]
            self.filters[key] = [value.strip() for value in values.split(",")]
            self.keys.append(key)

    def get(self, key):
        if key in self.filters:
            return self.filters[key][0]

    def get_all(self, key):
        if key in self.filters:
            return self.filters[key]

    def has(self, key):
        return key in self.filters
