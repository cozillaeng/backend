__author__ = 'Govind'

import datetime
from client.database import Db
from model.images import Images
from exception import InvalidInputException
from services import DatabaseService
from exception import NotFoundException
from model.common import Page


class ImageService(DatabaseService):

    def add(self, image_obj):
        if not image_obj.file_name:
            raise InvalidInputException("filename not found")
        if not image_obj.owner_id:
            raise InvalidInputException("owner id not found")
        if not image_obj.owner_type:
            raise InvalidInputException("owner type not found")
        with Db.get() as self._db:
            album = self.__add(image_obj)
            self._db.commit()
        return album

    def __add(self, image_obj):
        image_obj.id = None
        image_obj.created_at = datetime.datetime.now()
        self._db.add(image_obj)
        return image_obj

    def get_by_filename(self, file_name, owner_type):
        with Db.get() as self._db:
            images = Images.get_by_filename(self._db, file_name, owner_type)
            if not images:
                raise NotFoundException("Image not found for filename : " + file_name)
            return images

    def get_page(self, owner_id, owner_type, start=0, count=5,  order_by=None):
        if not start:
            start = 0
        if not count:
            count = 5

        page = Page()
        with Db.get() as self._db:
            page.items = Images.get_all_images(self._db, owner_id, owner_type, start, count, order_by)
            page.count = len(page.items)
            page.total_count = Images.get_all_count(self._db, owner_id, owner_type)
        return page