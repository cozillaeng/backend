import datetime
from client.aws import AmazonS3Client
from client.database import Db
from conf import Conf
from exception import InvalidInputException, DuplicateValueException
from model.common import Page
from model.city import City
from services import DatabaseService
from exception import NotFoundException
from util.common import DictUtil


class CityService(DatabaseService):
    def add(self, cities):
        with Db.get() as self._db:
            cities = self.__add(cities)
            self._db.commit()
            self._db.refresh(cities)
        return cities

    def get_page(self, start=0, count=50, order_by=None):
        if not start: start = 0
        if not count: count = 50

        page = Page()
        with Db.get() as self._db:
            page.items = City.get_all(self._db, start, count+1,  order_by)
            page.total_count = City.get_all_count(self._db)
            page.gen_page_data(start, count)
        return page

    def __add(self, cities):
        cities.id = None
        cities.created_at = cities.updated_at = datetime.datetime.now()
        self._db.add(cities)
        return cities



    def update(self, id, cities_obj):
        with Db.get() as self._db:
            cities = City.get(self._db, id)
            if not cities:
                raise NotFoundException("City not found")
            cities = self.__update(cities, cities_obj)
            self._db.commit()
        return cities

    def __update(self, cities, cities_obj):
        cities_obj.created_at = cities_obj.created_by_id = None
        cities.from_json_dict(cities_obj.to_dict())
        cities.updated_at = datetime.datetime.now()
        cities = self._db.merge(cities)
        return cities

    def delete(self, cities_id):
        with Db.get() as self._db:
            self.__delete(cities_id)
            self._db.commit()
        return "success"

    def __delete(self, cities_id):
        cities = None
        cities = City.get(self._db, cities_id)
        if not cities:
            raise NotFoundException()
        City.delete(self._db, cities_id)
