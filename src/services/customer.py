import datetime
from client.aws import AmazonS3Client
from client.database import Db
from conf import Conf
from exception import InvalidInputException, DuplicateValueException
from model.common import Page
from model.customer import Customer
from services import DatabaseService
from exception import NotFoundException
from util.common import DictUtil


class CustomerService(DatabaseService):
    def add(self, customer):
        with Db.get() as self._db:
            customer = self.__add(customer)
            self._db.commit()
            self._db.refresh(customer)
        return customer

    def get_page(self, start=0, count=50, order_by=None):
        if not start: start = 0
        if not count: count = 50

        page = Page()
        with Db.get() as self._db:
            page.items = Customer.get_all(self._db, start, count+1,  order_by)
            page.total_count = Customer.get_all_count(self._db)
            page.gen_page_data(start, count)
        return page

    def __add(self, customer):
        customer.id = None
        if not customer.first_name or not customer.last_name:
            raise InvalidInputException("First name or last name can not be empty")
        customer.created_at = customer.updated_at = datetime.datetime.now()
        customer.display_name = customer.first_name + " " + customer.last_name
        self._db.add(customer)
        return customer

    # def upload_images(self, customer):
    #     if (customer.layout_filename is not None):
    #         layout_filename = self.__upload_image_to_s3(customer, "layout_filename", customer.layout_filename)
    #         customer.layout_filename = layout_filename
    #
    # def __upload_profile_image_to_s3(self, customer, layout_filename):
    #     s3_conf = Conf.get("amazon_s3")
    #     s3_client = AmazonS3Client(s3_conf["key_id"], s3_conf["key_secret"])
    #     images_bucket = s3_client.get_bucket(s3_conf["bucket_images"])
    #     key = "/customers/layout_files/" + customer.id + "/" + customer.layout_filename
    #     s3_client.put_file(images_bucket, key, layout_filename)

    def update(self, id, customer_obj):
        with Db.get() as self._db:
            customer = Customer.get(self._db, id)
            if not customer:
                raise NotFoundException("Customer not found")
            customer = self.__update(customer, customer_obj)
            self._db.commit()
        return customer

    def __update(self, customer, customer_obj):
        customer_obj.created_at = customer_obj.created_by_id = None
        customer.from_json_dict(customer_obj.to_dict())
        customer.updated_at = datetime.datetime.now()
        customer.display_name = customer.first_name + " " + customer.last_name
        customer = self._db.merge(customer)
        return customer

    def delete(self, customer_id):
        with Db.get() as self._db:
            self.__delete(customer_id)
            self._db.commit()
        return "success"

    def __delete(self, customer_id):
        customer = None
        customer = Customer.get(self._db, customer_id)
        if not customer:
            raise NotFoundException()
        Customer.delete(self._db, customer_id)
