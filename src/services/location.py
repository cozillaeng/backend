import datetime
from client.aws import AmazonS3Client
from client.database import Db
from conf import Conf
from exception import InvalidInputException, DuplicateValueException
from model.common import Page
from model.location import Location
from services import DatabaseService
from exception import NotFoundException
from util.common import DictUtil


class LocationService(DatabaseService):
    def add(self, location):
        with Db.get() as self._db:
            location = self.__add(location)
            self._db.commit()
            self._db.refresh(location)
        return location

    def get_page(self, start=0, count=50, city_id=None, is_enabled=None, order_by=None):
        if not start: start = 0
        if not count: count = 50

        page = Page()
        with Db.get() as self._db:
            page.items = Location.get_all(self._db, start, count+1,  city_id, is_enabled, order_by)
            page.total_count = Location.get_all_count(self._db, city_id, is_enabled)
            page.gen_page_data(start, count)
        return page

    def __add(self, location):
        location.created_at = location.updated_at = datetime.datetime.now()
        self._db.add(location)
        return location



    def update(self, id, location_obj):
        with Db.get() as self._db:
            location = Location.get(self._db, id)
            if not location:
                raise NotFoundException("Location not found")
            location = self.__update(location, location_obj)
            self._db.commit()
        return location

    def __update(self, location, location_obj):
        location_obj.created_at = location_obj.created_by_id = None
        location.from_json_dict(location_obj.to_dict())
        location.updated_at = datetime.datetime.now()
        location = self._db.merge(location)
        return location

    def delete(self, location_id):
        with Db.get() as self._db:
            self.__delete(location_id)
            self._db.commit()
        return "success"

    def __delete(self, location_id):
        location = None
        location = Location.get(self._db, location_id)
        if not location:
            raise NotFoundException()
        Location.delete(self._db, location_id)
