from services.images import ImageService

__author__ = 'Govind'

import datetime
from client.database import Db
from model.albums import Album
from exception import InvalidInputException, InvalidStateException
from services import DatabaseService
from exception import NotFoundException
from model.common import Page

class AlbumService(DatabaseService):

    def add(self, album_obj):
        if not album_obj.owner_id:
            raise InvalidInputException("Invalid owner_type id")
        if not album_obj.owner_type:
            raise InvalidInputException("Invalid owner_type type")
        with Db.get() as self._db:
            album = self.__add(album_obj)
            self._db.commit()
        return album

    def __add(self, album_obj):
        album_obj.id = None
        album_obj.created_at = datetime.datetime.now()
        self._db.add(album_obj)
        return album_obj

    def delete(self, album_id):
        with Db.get() as self._db:
            album = Album.get(self._db, album_id)
            if not album:
                return
            self.__delete(album_id)
            self._db.commit()
        return "success"

    def __delete(self, album_id):
        album = self.__get_album(album_id)
        if not album:
            return
        Album.delete(self._db, album_id)

    def get(self, id, owner_id=None, owner_type=None):
        with Db.get() as self._db:
            album = self.__get_album(id)
            if not album:
                raise NotFoundException("Album not found for album id: " + id)
            if owner_id is not None and owner_type is not None:
                if album.owner_id != int(owner_id) or album.owner_type != owner_type:
                    raise NotFoundException("Album id:" + id + " doesn't belong to " + owner_type + " with id " + owner_id )
            return album

    def __get_album(self, album_id):
        album = Album.get(self._db, album_id)
        return album

    def get_album_by_name(self, album_name, owner_id, owner_type):
        with Db.get() as self._db:
            album = self.__get_album_by_name(album_name, owner_id, owner_type)
            if not album:
                raise NotFoundException("Album not found for album name: " + album_name + " and owner_id " + owner_id + " and owner_type " + owner_type)
            return album

    def __get_album_by_name(self, album_name, owner_id, owner_type):
        album = Album.get_by_display_name(self._db, album_name, owner_id, owner_type)
        return album

    def get_page(self, start=0, count=5, owner_id = None, owner_type = None, order_by=None):
        if not start: start = 0
        if not count: count = 5

        page = Page()
        with Db.get() as self._db:
            page.items = Album.get_all_albums(self._db, start, count + 1, owner_id, owner_type, order_by)
            page.count = len(page.items)
            page.total_count = Album.get_all_count(self._db, owner_id, owner_type)
            page.gen_page_data(start, count)
        return page

