import datetime
from client.aws import AmazonS3Client
from client.database import Db
from conf import Conf
from exception import InvalidInputException, DuplicateValueException
from model.common import Page
from model.designer import Designer
from services import DatabaseService
from exception import NotFoundException
from util.common import DictUtil


class DesignerService(DatabaseService):
    def add(self, designer):
        with Db.get() as self._db:
            designer = self.__add(designer)
            self._db.commit()
            self._db.refresh(designer)
        return designer

    def get_page(self, is_approved=None, source=None, start=0, count=50, order_by=None):
        if not start: start = 0
        if not count: count = 50

        page = Page()
        with Db.get() as self._db:
            page.items = Designer.get_all(self._db, start, count + 1, is_approved, source, order_by)
            page.total_count = Designer.get_all_count(self._db, is_approved, source)
            page.gen_page_data(start, count)
        return page

    def __add(self, designer):
        designer.id = None
        designer.is_approved = False
        designer.is_enabled = True
        if not designer.website_designer_id:
            raise InvalidInputException("Invalid website designer id")
        if Designer.get_by_website_designer_id(self._db, designer.website_designer_id):
            raise DuplicateValueException(
                "Designer already exists with website_designer_id: %s" % designer.website_designer_id)
        if Designer.get_by_email(self._db, designer.email):
            raise DuplicateValueException("Designer already exists with email: %s" % designer.email)
        if not designer.first_name or not designer.last_name:
            raise InvalidInputException("First name or last name can not be empty")
        designer.created_at = designer.updated_at = datetime.datetime.now()
        designer.display_name = designer.first_name + " " + designer.last_name
        self._db.add(designer)
        return designer

    def upload_images(self, designer):
        if (designer.profile_image_filename is not None):
            profile_image_filename = self.__upload_image_to_s3(designer, "primary_image_filename",
                                                               designer.profile_image_filename)
            designer.profile_image_filename = profile_image_filename

    def __upload_profile_image_to_s3(self, designer, profile_image_file):
        s3_conf = Conf.get("amazon_s3")
        s3_client = AmazonS3Client(s3_conf["key_id"], s3_conf["key_secret"])
        images_bucket = s3_client.get_bucket(s3_conf["bucket_images"])
        key = "/designers/profile_images/" + designer.id + "/" + designer.profile_image_filename
        s3_client.put_file(images_bucket, key, profile_image_file)

    def update(self, id, designer_obj):
        with Db.get() as self._db:
            designer = Designer.get(self._db, id)
            if not designer:
                raise NotFoundException("Designer not found")
            designer = self.__update(designer, designer_obj)
            self._db.commit()
        return designer

    def __update(self, designer, designer_obj):
        designer_obj.created_at = designer_obj.created_by_id = None
        designer.from_json_dict(designer_obj.to_dict())
        designer.updated_at = datetime.datetime.now()
        designer.display_name = designer.first_name + " " + designer.last_name
        designer = self._db.merge(designer)
        return designer

    def delete(self, designer_id):
        with Db.get() as self._db:
            self.__delete(designer_id)
            self._db.commit()
        return "success"

    def __delete(self, designer_id):
        designer = None
        designer = Designer.get(self._db, designer_id)
        if not designer:
            raise NotFoundException()
        Designer.delete(self._db, designer_id)
