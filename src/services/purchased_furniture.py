import datetime
from client.aws import AmazonS3Client
from client.database import Db
from conf import Conf
from exception import InvalidInputException, DuplicateValueException
from model.common import Page
from model.purchased_furniture import PurchasedFurniture
from services import DatabaseService
from exception import NotFoundException
from util.common import DictUtil


class PurchasedFurnitureService(DatabaseService):
    def add(self, purchased_furniture):
        with Db.get() as self._db:
            purchased_furniture = self.__add(purchased_furniture)
            self._db.commit()
            self._db.refresh(purchased_furniture)
        return purchased_furniture

    def get_page(self, start=0, count=50, order_by=None):
        if not start: start = 0
        if not count: count = 50

        page = Page()
        with Db.get() as self._db:
            page.items = PurchasedFurniture.get_all(self._db, start, count+1,  order_by)
            page.total_count = PurchasedFurniture.get_all_count(self._db)
            page.gen_page_data(start, count)
        return page

    def __add(self, purchased_furniture):
        purchased_furniture.id = None
        purchased_furniture.created_at = purchased_furniture.updated_at = datetime.datetime.now()
        self._db.add(purchased_furniture)
        return purchased_furniture



    def update(self, id, purchased_furniture_obj):
        with Db.get() as self._db:
            purchased_furniture = PurchasedFurniture.get(self._db, id)
            if not purchased_furniture:
                raise NotFoundException("PurchasedFurniture not found")
            purchased_furniture = self.__update(purchased_furniture, purchased_furniture_obj)
            self._db.commit()
        return purchased_furniture

    def __update(self, purchased_furniture, purchased_furniture_obj):
        purchased_furniture_obj.created_at = purchased_furniture_obj.created_by_id = None
        purchased_furniture.from_json_dict(purchased_furniture_obj.to_dict())
        purchased_furniture.updated_at = datetime.datetime.now()
        purchased_furniture = self._db.merge(purchased_furniture)
        return purchased_furniture

    def delete(self, purchased_furniture_id):
        with Db.get() as self._db:
            self.__delete(purchased_furniture_id)
            self._db.commit()
        return "success"

    def __delete(self, purchased_furniture_id):
        purchased_furniture = None
        purchased_furniture = PurchasedFurniture.get(self._db, purchased_furniture_id)
        if not purchased_furniture:
            raise NotFoundException()
        PurchasedFurniture.delete(self._db, purchased_furniture_id)
