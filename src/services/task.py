__author__ = 'shubham'

import datetime
from client.database import Db
from model.task import Task
from exception import InvalidInputException
from services import DatabaseService
from exception import NotFoundException
from model.common import Page
from util.common import DictUtil

class TaskService(DatabaseService):

    def add(self, task_obj):
        if not task_obj.owner_id:
            raise InvalidInputException("Invalid owner_type id")
        if not task_obj.owner_type:
            raise InvalidInputException("Invalid owner_type")
        with Db.get() as self._db:
            task = self.__add(task_obj)
            self._db.commit()
        return task

    def __add(self, task_obj):
        task_obj.id = None
        task_obj.created_at = datetime.datetime.now()
        self._db.add(task_obj)
        return task_obj

    def delete(self, task_id):
        with Db.get() as self._db:
            self.__delete(task_id)
            self._db.commit()
        return "success"

    def __delete(self, task_id):
        task = None
        task = self.__get_task(task_id)
        if not task:
            raise NotFoundException()
        Task.delete(self._db,task_id)

    def get(self, id):
        with Db.get() as self._db:
            task = self.__get_task(id)
            return task

    def __get_task(self, task_id):
        task = None
        task = Task.get(self._db, task_id)
        if not task:
            raise NotFoundException()
        return task

    def get_page(self, start=0, count=5, owner_id = None, owner_type = None, status=None, order_by=None):
        if not start: start = 0
        if not count: count = 5

        page = Page()
        with Db.get() as self._db:
            page.items = Task.get_all_tasks(self._db, start, count + 1, owner_id, owner_type, status, order_by)
            page.total_count = Task.get_all_count(self._db, owner_id, status, owner_type)
            page.gen_page_data(start, count)
        return page

    def update(self, id, task_obj):
        with Db.get() as self._db:
            task = Task.get(self._db, id)
            if not task:
                raise NotFoundException("Task not found")
            task = self.__update(task, task_obj)
            self._db.commit()
        return task

    def __update(self, task, task_obj):
        task_obj.created_at = task_obj.created_by_id = None
        task.from_json_dict(task_obj.to_dict())
        task.updated_at = datetime.datetime.now()
        task = self._db.merge(task)
        return task