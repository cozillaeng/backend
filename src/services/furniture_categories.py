import datetime
from client.aws import AmazonS3Client
from client.database import Db
from conf import Conf
from exception import InvalidInputException, DuplicateValueException
from model.common import Page
from model.furniture_categories import FurnitureCategories
from services import DatabaseService
from exception import NotFoundException
from util.common import DictUtil


class FurnitureCategoriesService(DatabaseService):
    def add(self, furniture_categories):
        with Db.get() as self._db:
            furniture_categories = self.__add(furniture_categories)
            self._db.commit()
            self._db.refresh(furniture_categories)
        return furniture_categories

    def get_page(self, start=0, count=50, order_by=None):
        if not start: start = 0
        if not count: count = 50

        page = Page()
        with Db.get() as self._db:
            page.items = FurnitureCategories.get_all(self._db, start, count+1,  order_by)
            page.total_count = FurnitureCategories.get_all_count(self._db)
            page.gen_page_data(start, count)
        return page

    def __add(self, furniture_categories):
        furniture_categories.id = None
        furniture_categories.created_at = furniture_categories.updated_at = datetime.datetime.now()
        self._db.add(furniture_categories)
        return furniture_categories



    def update(self, id, furniture_categories_obj):
        with Db.get() as self._db:
            furniture_categories = FurnitureCategories.get(self._db, id)
            if not furniture_categories:
                raise NotFoundException("FurnitureCategories not found")
            furniture_categories = self.__update(furniture_categories, furniture_categories_obj)
            self._db.commit()
        return furniture_categories

    def __update(self, furniture_categories, furniture_categories_obj):
        furniture_categories_obj.created_at = furniture_categories_obj.created_by_id = None
        furniture_categories.from_json_dict(furniture_categories_obj.to_dict())
        furniture_categories.updated_at = datetime.datetime.now()
        furniture_categories = self._db.merge(furniture_categories)
        return furniture_categories

    def delete(self, furniture_categories_id):
        with Db.get() as self._db:
            self.__delete(furniture_categories_id)
            self._db.commit()
        return "success"

    def __delete(self, furniture_categories_id):
        furniture_categories = None
        furniture_categories = FurnitureCategories.get(self._db, furniture_categories_id)
        if not furniture_categories:
            raise NotFoundException()
        FurnitureCategories.delete(self._db, furniture_categories_id)
