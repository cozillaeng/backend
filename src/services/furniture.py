import datetime
from client.aws import AmazonS3Client
from client.database import Db
from conf import Conf
from exception import InvalidInputException, DuplicateValueException
from model.common import Page
from model.furniture import Furniture
from services import DatabaseService
from exception import NotFoundException
from util.common import DictUtil


class FurnitureService(DatabaseService):
    def add(self, furniture):
        with Db.get() as self._db:
            furniture = self.__add(furniture)
            self._db.commit()
            self._db.refresh(furniture)
        return furniture

    def get_page(self, start=0, count=50, order_by=None):
        if not start: start = 0
        if not count: count = 50

        page = Page()
        with Db.get() as self._db:
            page.items = Furniture.get_all(self._db, start, count+1,  order_by)
            page.total_count = Furniture.get_all_count(self._db)
            page.gen_page_data(start, count)
        return page

    def __add(self, furniture):
        furniture.id = None
        furniture.created_at = furniture.updated_at = datetime.datetime.now()
        self._db.add(furniture)
        return furniture



    def update(self, id, furniture_obj):
        with Db.get() as self._db:
            furniture = Furniture.get(self._db, id)
            if not furniture:
                raise NotFoundException("Furniture not found")
            furniture = self.__update(furniture, furniture_obj)
            self._db.commit()
        return furniture

    def __update(self, furniture, furniture_obj):
        furniture_obj.created_at = furniture_obj.created_by_id = None
        furniture.from_json_dict(furniture_obj.to_dict())
        furniture.updated_at = datetime.datetime.now()
        furniture = self._db.merge(furniture)
        return furniture

    def delete(self, furniture_id):
        with Db.get() as self._db:
            self.__delete(furniture_id)
            self._db.commit()
        return "success"

    def __delete(self, furniture_id):
        furniture = None
        furniture = Furniture.get(self._db, furniture_id)
        if not furniture:
            raise NotFoundException()
        Furniture.delete(self._db, furniture_id)
