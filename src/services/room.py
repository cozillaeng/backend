import datetime
from client.aws import AmazonS3Client
from client.database import Db
from conf import Conf
from exception import InvalidInputException, DuplicateValueException
from model.common import Page
from model.room import Room
from services import DatabaseService
from exception import NotFoundException
from util.common import DictUtil


class RoomService(DatabaseService):
    def add(self, room):
        with Db.get() as self._db:
            room = self.__add(room)
            self._db.commit()
            self._db.refresh(room)
        return room

    def get_page(self, start=0, count=50, flat_id=None, is_enabled=None, order_by=None):
        if not start: start = 0
        if not count: count = 50

        page = Page()
        with Db.get() as self._db:
            page.items = Room.get_all(self._db, start, count+1, flat_id, is_enabled, order_by)
            page.total_count = Room.get_all_count(self._db, flat_id, is_enabled)
            page.gen_page_data(start, count)
        return page

    def __add(self, room):
        room.id = None
        room.created_at = room.updated_at = datetime.datetime.now()
        room.created_by_id = room.updated_by_id = 1;
        self._db.add(room)
        return room

    def update(self, id, room_obj):
        with Db.get() as self._db:
            room = Room.get(self._db, id)
            if not room:
                raise NotFoundException("Room not found")
            room = self.__update(room, room_obj)
            self._db.commit()
        return room

    def __update(self, room, room_obj):
        room_obj.created_at = room_obj.created_by_id = None
        room.from_json_dict(room_obj.to_dict())
        room.updated_at = datetime.datetime.now()
        room = self._db.merge(room)
        return room

    def delete(self, room_id):
        with Db.get() as self._db:
            self.__delete(room_id)
            self._db.commit()
        return "success"

    def __delete(self, room_id):
        room = None
        room = Room.get(self._db, room_id)
        if not room:
            raise NotFoundException()
        Room.delete(self._db, room_id)
