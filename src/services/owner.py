import datetime
from client.aws import AmazonS3Client
from client.database import Db
from conf import Conf
from exception import InvalidInputException, DuplicateValueException
from model.common import Page
from model.owner import Owner
from services import DatabaseService
from exception import NotFoundException
from util.common import DictUtil


class OwnerService(DatabaseService):
    def add(self, owner):
        with Db.get() as self._db:
            owner = self.__add(owner)
            self._db.commit()
            self._db.refresh(owner)
        return owner

    def get_page(self, start=0, count=50, order_by=None):
        if not start: start = 0
        if not count: count = 50

        page = Page()
        with Db.get() as self._db:
            page.items = Owner.get_all(self._db, start, count+1,  order_by)
            page.total_count = Owner.get_all_count(self._db)
            page.gen_page_data(start, count)
        return page

    def __add(self, owner):
        owner.id = None
        # if not owner.first_name or not owner.last_name:
        #     raise InvalidInputException("First name or last name can not be empty")
        owner.created_at = owner.updated_at = datetime.datetime.now()

        owner.created_by_id = owner.updated_by_id = 1
        self._db.add(owner)
        return owner



    def update(self, id, owner_obj):
        with Db.get() as self._db:
            owner = Owner.get(self._db, id)
            if not owner:
                raise NotFoundException("Owner not found")
            owner = self.__update(owner, owner_obj)
            self._db.commit()
        return owner

    def __update(self, owner, owner_obj):
        owner_obj.created_at = owner_obj.created_by_id = None
        owner.from_json_dict(owner_obj.to_dict())
        owner.updated_at = datetime.datetime.now()
        owner = self._db.merge(owner)
        return owner

    def delete(self, owner_id):
        with Db.get() as self._db:
            self.__delete(owner_id)
            self._db.commit()
        return "success"

    def __delete(self, owner_id):
        owner = None
        owner = Owner.get(self._db, owner_id)
        if not owner:
            raise NotFoundException()
        Owner.delete(self._db, owner_id)
