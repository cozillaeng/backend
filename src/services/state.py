import datetime
from client.aws import AmazonS3Client
from client.database import Db
from conf import Conf
from exception import InvalidInputException, DuplicateValueException
from model.common import Page
from model.state import State
from services import DatabaseService
from exception import NotFoundException
from util.common import DictUtil


class StateService(DatabaseService):
    def add(self, state):
        with Db.get() as self._db:
            state = self.__add(state)
            self._db.commit()
            self._db.refresh(state)
        return state

    def get_page(self, start=0, count=50, order_by=None):
        if not start: start = 0
        if not count: count = 50

        page = Page()
        with Db.get() as self._db:
            page.items = State.get_all(self._db, start, count+1,  order_by)
            page.total_count = State.get_all_count(self._db)
            page.gen_page_data(start, count)
        return page

    def __add(self, state):
        state.created_at = state.updated_at = datetime.datetime.now()
        self._db.add(state)
        return state



    def update(self, id, state_obj):
        with Db.get() as self._db:
            state = State.get(self._db, id)
            if not state:
                raise NotFoundException("State not found")
            state = self.__update(state, state_obj)
            self._db.commit()
        return state

    def __update(self, state, state_obj):
        state_obj.created_at = state_obj.created_by_id = None
        state.from_json_dict(state_obj.to_dict())
        state.updated_at = datetime.datetime.now()
        state = self._db.merge(state)
        return state

    def delete(self, state_id):
        with Db.get() as self._db:
            self.__delete(state_id)
            self._db.commit()
        return "success"

    def __delete(self, state_id):
        state = None
        state = State.get(self._db, state_id)
        if not state:
            raise NotFoundException()
        State.delete(self._db, state_id)
