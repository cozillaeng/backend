import datetime
from client.aws import AmazonS3Client
from client.database import Db
from conf import Conf
from exception import InvalidInputException, DuplicateValueException
from model.common import Page
from model.home_amenities import HomeAmenities
from services import DatabaseService
from exception import NotFoundException
from util.common import DictUtil


class HomeAmenitiesService(DatabaseService):
    def add(self, home_amenities):
        with Db.get() as self._db:
            home_amenities = self.__add(home_amenities)
            self._db.commit()
            self._db.refresh(home_amenities)
        return home_amenities

    def get_page(self, start=0, count=50, order_by=None):
        if not start: start = 0
        if not count: count = 50

        page = Page()
        with Db.get() as self._db:
            page.items = HomeAmenities.get_all(self._db, start, count+1,  order_by)
            page.total_count = HomeAmenities.get_all_count(self._db)
            page.gen_page_data(start, count)
        return page

    def __add(self, home_amenities):
        home_amenities.id = None
        home_amenities.created_at = home_amenities.updated_at = datetime.datetime.now()
        self._db.add(home_amenities)
        return home_amenities



    def update(self, id, home_amenities_obj):
        with Db.get() as self._db:
            home_amenities = HomeAmenities.get(self._db, id)
            if not home_amenities:
                raise NotFoundException("HomeAmenities not found")
            home_amenities = self.__update(home_amenities, home_amenities_obj)
            self._db.commit()
        return home_amenities

    def __update(self, home_amenities, home_amenities_obj):
        home_amenities_obj.created_at = home_amenities_obj.created_by_id = None
        home_amenities.from_json_dict(home_amenities_obj.to_dict())
        home_amenities.updated_at = datetime.datetime.now()
        home_amenities = self._db.merge(home_amenities)
        return home_amenities

    def delete(self, home_amenities_id):
        with Db.get() as self._db:
            self.__delete(home_amenities_id)
            self._db.commit()
        return "success"

    def __delete(self, home_amenities_id):
        home_amenities = None
        home_amenities = HomeAmenities.get(self._db, home_amenities_id)
        if not home_amenities:
            raise NotFoundException()
        HomeAmenities.delete(self._db, home_amenities_id)
