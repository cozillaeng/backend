import datetime
from client.aws import AmazonS3Client
from client.database import Db
from conf import Conf
from exception import InvalidInputException, DuplicateValueException
from model.common import Page
from model.rented_furniture import RentedFurniture
from services import DatabaseService
from exception import NotFoundException
from util.common import DictUtil


class RentedFurnitureService(DatabaseService):
    def add(self, rented_furniture):
        with Db.get() as self._db:
            rented_furniture = self.__add(rented_furniture)
            self._db.commit()
            self._db.refresh(rented_furniture)
        return rented_furniture

    def get_page(self, start=0, count=50, order_by=None):
        if not start: start = 0
        if not count: count = 50

        page = Page()
        with Db.get() as self._db:
            page.items = RentedFurniture.get_all(self._db, start, count+1,  order_by)
            page.total_count = RentedFurniture.get_all_count(self._db)
            page.gen_page_data(start, count)
        return page

    def __add(self, rented_furniture):
        rented_furniture.id = None
        rented_furniture.created_at = rented_furniture.updated_at = datetime.datetime.now()
        self._db.add(rented_furniture)
        return rented_furniture



    def update(self, id, rented_furniture_obj):
        with Db.get() as self._db:
            rented_furniture = RentedFurniture.get(self._db, id)
            if not rented_furniture:
                raise NotFoundException("RentedFurniture not found")
            rented_furniture = self.__update(rented_furniture, rented_furniture_obj)
            self._db.commit()
        return rented_furniture

    def __update(self, rented_furniture, rented_furniture_obj):
        rented_furniture_obj.created_at = rented_furniture_obj.created_by_id = None
        rented_furniture.from_json_dict(rented_furniture_obj.to_dict())
        rented_furniture.updated_at = datetime.datetime.now()
        rented_furniture = self._db.merge(rented_furniture)
        return rented_furniture

    def delete(self, rented_furniture_id):
        with Db.get() as self._db:
            self.__delete(rented_furniture_id)
            self._db.commit()
        return "success"

    def __delete(self, rented_furniture_id):
        rented_furniture = None
        rented_furniture = RentedFurniture.get(self._db, rented_furniture_id)
        if not rented_furniture:
            raise NotFoundException()
        RentedFurniture.delete(self._db, rented_furniture_id)
