import copy
from client.database import Db
from exception import NotFoundException
from model.product_sku import ProductSku
from model.common import Page
from services import DatabaseService


class ProductSkuService(DatabaseService):
    def get_page(self, is_placeholder=False, product_id=None, source=None, start=0, count=50, order_by=None):
        if not start: start = 0
        if not count: count = 50

        page = Page()
        with Db.get() as self._db:
            page.items = ProductSku.get_all(self._db, start, count + 1, product_id, is_placeholder, order_by)
            page.total_count = ProductSku.get_all_count(self._db, product_id)
            page.gen_page_data(start, count)
        return page

    def get_product_sku(self,sku_id):
		
		with Db.get() as self._db:
			product_sku = ProductSku.get(self._db,sku_id)    	
			if not product_sku:
				raise NotFoundException()
			return product_sku

