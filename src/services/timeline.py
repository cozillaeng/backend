__author__ = 'ramakrishnag'

import datetime
from client.database import Db
from model.timeline import TimeLine
from exception import InvalidInputException
from services import DatabaseService
from exception import NotFoundException
from model.common import Page

class TimeLineService(DatabaseService):

    def add(self, timeline_obj):
        if not timeline_obj.owner_id:
            raise InvalidInputException("Invalid owner_type id")
        if not timeline_obj.owner_type:
            raise InvalidInputException("Invalid owner_type")

        with Db.get() as self._db:
            timeline = self.__add(timeline_obj)
            self._db.commit()
        return timeline

    def __add(self, timeline_obj):
        timeline_obj.id = None
        timeline_obj.created_at = datetime.datetime.now()
        self._db.add(timeline_obj)
        return timeline_obj

    def delete(self, timeline_id):
        with Db.get() as self._db:
            self.__delete(timeline_id)
            self._db.commit()
        return "success"

    def __delete(self, timeline_id):
        timeline = None
        timeline = self.__get_timeline(timeline_id)
        if not timeline:
            raise NotFoundException()
        TimeLine.delete(self._db,timeline_id)

    def get(self, id):
        with Db.get() as self._db:
            timeline = self.__get_timeline(id)
            return timeline

    def __get_timeline(self, timeline_id):
        timeline = None
        timeline = TimeLine.get(self._db, timeline_id)
        if not timeline:
            raise NotFoundException()
        return timeline

    def get_page(self, start=0, count=5, owner_id=None, owner_type=None, order_by=None):
        if not start: start = 0
        if not count: count = 5

        page = Page()
        with Db.get() as self._db:
            page.items = TimeLine.get_all_timelines(self._db, start, count + 1, owner_id, owner_type, order_by)
            page.count = len(page.items)
            page.total_count = TimeLine.get_all_count(self._db, owner_id, owner_type)
            page.gen_page_data(start, count)
        return page