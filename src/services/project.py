import datetime
from client.aws import AmazonS3Client
from client.database import Db
from conf import Conf
from exception import InvalidInputException, DuplicateValueException
from model.common import Page
from model.project import Project
from services import DatabaseService
from exception import NotFoundException
from util.common import DictUtil


class ProjectService(DatabaseService):
    def add(self, project):
        with Db.get() as self._db:
            project = self.__create(project)
            self._db.commit()
        return project

    def get_page(self, start=0, count=50, designer_id=None, order_by=None):
        if not start: start = 0
        if not count: count = 50

        page = Page()
        with Db.get() as self._db:
            page.items = Project.get_all(self._db, start, count + 1, designer_id, order_by)
            page.total_count = Project.get_all_count(self._db, designer_id)
            page.gen_page_data(start, count)
        return page

    def __create(self, project):
        project.id = None
        project.created_at = project.updated_at = datetime.datetime.now()
        self._db.add(project)
        return project

    def update(self, id, project_dict):
        with Db.get() as self._db:
            project = self.__update(id,project_dict)
            self._db.commit()
        return project

    def __update(self, id, project_dict):
        project = Project.get(self._db,id)
        DictUtil.delete_keys(project_dict, "id", "created_at", "created_by_id")
        project = Project().from_json_dict(project_dict)
        project.id = id
        project.updated_at = datetime.datetime.now()
        project = self._db.merge(project)
        return project

    def get(self, id):
        with Db.get() as self._db:
            project = self.__get_project(id)
            if not project:
                raise NotFoundException("Project not found for project id: " + id)
            return project

    def __get_project(self, project_id):
        project = Project.get(self._db, project_id)
        return project

