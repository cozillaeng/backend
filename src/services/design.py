__author__ = 'ramakrishnag'
from client.database import Db
from model.common import Page
from model.design import Design
from model.project import Project
from exception import InvalidInputException
from services import DatabaseService
from exception import NotFoundException
from client.aws import AmazonS3Client
from conf import Conf
from util.json_util import dump_json
from model.design_version import DesignVersion
import datetime

class DesignService(DatabaseService):

    def add(self, design_obj):
        design_obj.is_enabled = True
        if not design_obj.designer_id:
            raise InvalidInputException("Invalid designer id")
        with Db.get() as self._db:
            design = self.__add(design_obj)
            self._db.commit()
            self._db.refresh(design)
            self.gen_instance_id_sku_mappings(design)
            self._db.commit()
            self._db.refresh(design)
            #Creating the intial version of the design
            self.add_version(design.id)
            #self.upload_images(design)
            #self._db.commit()
            #self._db.refresh(design)
        return design

    def gen_instance_id_sku_mappings(self,design):
        if design.floors:
            floors = design.floors
            for floor in floors:
                if floor.skus:
                    skus = floor.skus
                    instance_map={}
                    for sku in skus:
                        if instance_map.get(sku.sku_id):
                            instance_map[sku.sku_id] = instance_map.get(sku.sku_id) + 1
                        else:
                            instance_map[sku.sku_id] = 1;
                        sku.instance_id = instance_map.get(sku.sku_id)


    def upload_images(self, design):
        if design.design_image_filename is not None:
            uploaded_file_ext = self.__upload_image_to_s3(design,"design_images","designImage",design.design_image_filename)
            design.design_image_filename = "designImage." + uploaded_file_ext
        if design.cover_image_filename is not None:
            uploaded_file_ext = self.__upload_image_to_s3(design,"cover_images","coverImage",design.cover_image_filename)
            design.cover_image_filename = "coverImage." + uploaded_file_ext

    def __add(self, design_obj):
        design_obj.id= None
        self._db.add(design_obj)
        return design_obj

    def update(self, id, design_dict):
        with Db.get() as self._db:
            design = Design.get(self._db, id)
            if not design:
                raise NotFoundException("Design not found")
            design = self.__update(id, design, design_dict)
            self._db.commit()
            self._db.refresh(design)
            self.gen_instance_id_sku_mappings(design)
            self._db.commit()
            self._db.refresh(design)
        return design

    def __update(self, id, design, design_dict):

        design_dict["created_at"] = design.created_at
        design_dict["created_by_id"] = design.created_by_id
        design_new = Design().from_json_dict(design_dict)
        design_new.id = id
        #self.upload_images(design_new)
        design = self._db.merge(design_new)
        return design

    def delete(self, id):
        with Db.get() as self._db:
            self.__delete(id)
            self._db.commit()
        return "success"

    def __delete(self, design_id):
        design = None
        design = self.__get_design(design_id)
        if not design:
            raise NotFoundException()
        Design.delete(self._db,design_id)

    def get(self, id):
        with Db.get() as self._db:
            design = self.__get_design(id)
            return design

    def __get_design(self, design_id):
        design = None
        design = Design.get(self._db, design_id)
        if not design:
            raise NotFoundException()
        return design

    def get_page(self, is_enabled=None, created_by_id=None, project_id = None, designer_id = None, start=0, count=5, order_by=None):
        if not start: start = 0
        if not count: count = 5

        page = Page()
        with Db.get() as self._db:
            page.items = Design.get_all_designs(self._db, start, count + 1, is_enabled,
                                                created_by_id, project_id, designer_id, order_by)
            page.total_count = Design.get_all_count(self._db, is_enabled, created_by_id, project_id, designer_id)
            page.gen_page_data(start, count)
        return page

    def __upload_image_to_s3(self, design, image_category, target_filename, image_filename):
        temp_array = image_filename.split(".")
        ext = temp_array[len(temp_array)-1]
        s3_conf = Conf.get("amazon_s3")
        s3_client = AmazonS3Client(s3_conf["key_id"], s3_conf["key_secret"])
        images_bucket = s3_client.get_bucket(s3_conf["bucket_images"])
        key = "/designs/" + image_category + "/" + design.id + "/" + target_filename + "." + ext
        s3_client.put_filename(images_bucket, key, image_filename)
        return ext

    #This method removes the mapping of design and product_sku
    #def delete_design_sku_mappings(self,design_id=None, design_sku_id_list=None):
    #    query = self._db.query(DesignSkuMap)
    #    if design_id is not None:
    #        query = query.filter(DesignSkuMap.design_id == design_id)
    #    if design_sku_id_list is not None and len(design_sku_id_list) > 0:
    #        query = query.filter(DesignSkuMap.id.in_(design_sku_id_list))
    #
    #    query.delete(synchronize_session='fetch')

    #Remove design_sku mapping
    #def delete_design_sku(self, floor_sku_id):
    #    with Db.get() as self._db:
    #        design_sku = self._db.query(FloorSkuMap).filter(FloorSkuMap.id==floor_sku_id).one()
    #        if not design_sku:
    #            raise NotFoundException()
    #        design_id = design_sku.design_id
    #
    #        FloorSkuMap.delete(self._db,floor_sku_id)
    #        self._db.commit()
    #        return self.__get_design(design_id)

    def clone_design(self,id):
        with Db.get() as self._db:
            design = Design.get(self._db, id)
            if not design:
                raise NotFoundException("Design not found")

            new_design = Design().clone(design)
            new_design = self.__add(new_design)
            self._db.commit()
            self._db.refresh(new_design)
            self.gen_instance_id_sku_mappings(new_design)
            self._db.commit()
            self._db.refresh(new_design)
            #self.__upload_image_to_s3(new_design,"design_images","designImage","designs/design_images/" + id + "/" + design.design_image_filename)
            #self.__upload_image_to_s3(new_design,"cover_images","coverImage","designs/design_images/" + id + "/" + design.design_image_filename)
            return new_design


    def check_design_permission(self, design_id=0, designer_id=0, project_id=0):
        if not project_id:
            return True
        query_output = 0
        with Db.get() as self._db:
            if design_id:
                query_output = self._db.query(Design,Project).filter(Design.id == design_id).filter(Project.id == project_id).filter(Design.project_id == Project.id).filter(Project.designer_id == designer_id).count()
            else:
                query_output = self._db.query(Project).filter(Project.id == project_id).filter(Project.designer_id == designer_id).count()
        return query_output

    def add_version(self,design_id):
        with Db.get() as self._db:
            design = Design.get(self._db, design_id)
            if not design:
                raise NotFoundException("Design not found")

            new_design = Design().clone(design)
            design_version = DesignVersion()
            design_version.design_id = design_id
            design_version.design_data = dump_json(new_design)
            design_version.created_at = datetime.datetime.now()
            self._db.add(design_version)
            self._db.commit()
