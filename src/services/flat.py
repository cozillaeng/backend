import datetime
from client.aws import AmazonS3Client
from client.database import Db
from conf import Conf
from exception import InvalidInputException, DuplicateValueException
from model.common import Page
from model.flat import Flat
from services import DatabaseService
from exception import NotFoundException
from util.common import DictUtil


class FlatService(DatabaseService):
    def add(self, flat):
        with Db.get() as self._db:
            flat = self.__add(flat)
            self._db.commit()
            self._db.refresh(flat)
        return flat

    def get_page(self, start=0, count=50, order_by=None):
        if not start: start = 0
        if not count: count = 50

        page = Page()
        with Db.get() as self._db:
            page.items = Flat.get_all(self._db, start, count+1,  order_by)
            page.total_count = Flat.get_all_count(self._db)
            page.gen_page_data(start, count)
        return page

    def __add(self, flat):
        flat.id = None
        flat.created_at = flat.updated_at = datetime.datetime.now()
        flat.created_by_id = flat.updated_by_id = 1
        self._db.add(flat)
        return flat



    def update(self, id, flat_obj):
        with Db.get() as self._db:
            flat = Flat.get(self._db, id)
            if not flat:
                raise NotFoundException("Flat not found")
            flat = self.__update(flat, flat_obj)
            self._db.commit()
        return flat

    def __update(self, flat, flat_obj):
        flat_obj.created_at = flat_obj.created_by_id = None
        flat.from_json_dict(flat_obj.to_dict())
        flat.updated_at = datetime.datetime.now()
        flat = self._db.merge(flat)
        return flat

    def delete(self, flat_id):
        with Db.get() as self._db:
            self.__delete(flat_id)
            self._db.commit()
        return "success"

    def __delete(self, flat_id):
        flat = None
        flat = Flat.get(self._db, flat_id)
        if not flat:
            raise NotFoundException()
        Flat.delete(self._db, flat_id)
