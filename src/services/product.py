import copy
from client.database import Db
from exception import NotFoundException, MultipleRowsFoundException
from model.product import Product
from model.common import Page
from services import DatabaseService
from model.product import Product
from model.product_component import ProductComponent
from model.product_component_option import ProductComponentOption
from sqlalchemy import tuple_ , func
from model.componentoption_sku_map import ComponentOptionSkuMap
from model.product_sku import ProductSku


class ProductService(DatabaseService):

    def get_product(self,id):
        with Db.get() as self._db:
            product = Product.get(self._db,id)       
            if not product:
                raise NotFoundException()
            return product

    def get_filtered_skus(self,product_id=None,filters=None):

        keys = []
        componentid_value_list = []
        for key in filters.keys:
            keys.append(key)

        with Db.get() as self._db:
            components = self._db.query(ProductComponent).filter(ProductComponent.product_id == product_id).filter(ProductComponent.name.in_(keys)).all()

            for component in components:
                value = filters.get(component.name)
                if value:
                    componentid_value_list.append((component.id,value))

            component_options = self._db.query(ProductComponentOption).filter(tuple_(ProductComponentOption.product_component_id,ProductComponentOption.name).in_(componentid_value_list)).all()
            option_id_list = []
            for option in component_options:
                option_id_list.append(option.id)


            sku_count = self._db.query(ComponentOptionSkuMap.sku_id).filter(ComponentOptionSkuMap.component_option_id.in_(option_id_list)).group_by(ComponentOptionSkuMap.sku_id).having(func.count(ComponentOptionSkuMap.sku_id)==len(keys)).count()
            print sku_count
            if not sku_count:
                raise NotFoundException()
            elif sku_count > 1:
                raise MultipleRowsFoundException(message = "Multiple rows found , unexpected behaviour")

            sku_id = self._db.query(ComponentOptionSkuMap.sku_id).filter(ComponentOptionSkuMap.component_option_id.in_(option_id_list)).group_by(ComponentOptionSkuMap.sku_id).having(func.count(ComponentOptionSkuMap.sku_id)==len(keys)).one()
            #sku = self._db.query(ProductSku).filter(ProductSku.id == sku_id)
            return sku_id
