import datetime
from client.aws import AmazonS3Client
from client.database import Db
from conf import Conf
from exception import InvalidInputException, DuplicateValueException
from model.common import Page
from model.tenant import Tenant
from services import DatabaseService
from exception import NotFoundException
from util.common import DictUtil


class TenantService(DatabaseService):
    def add(self, tenant):
        with Db.get() as self._db:
            tenant = self.__add(tenant)
            self._db.commit()
            self._db.refresh(tenant)
        return tenant

    def get_page(self, start=0, count=50, order_by=None):
        if not start: start = 0
        if not count: count = 50

        page = Page()
        with Db.get() as self._db:
            page.items = Tenant.get_all(self._db, start, count+1,  order_by)
            page.total_count = Tenant.get_all_count(self._db)
            page.gen_page_data(start, count)
        return page

    def __add(self, tenant):
        tenant.id = None
        if not tenant.first_name or not tenant.last_name:
            raise InvalidInputException("First name or last name can not be empty")
        tenant.created_at = tenant.updated_at = datetime.datetime.now()
        tenant.created_by_id = tenant.updated_by_id = 1;
        self._db.add(tenant)
        return tenant



    def update(self, id, tenant_obj):
        with Db.get() as self._db:
            tenant = Tenant.get(self._db, id)
            if not tenant:
                raise NotFoundException("Tenant not found")
            tenant = self.__update(tenant, tenant_obj)
            self._db.commit()
        return tenant

    def __update(self, tenant, tenant_obj):
        tenant_obj.created_at = tenant_obj.created_by_id = None
        tenant.from_json_dict(tenant_obj.to_dict())
        tenant.updated_at = datetime.datetime.now()
        tenant = self._db.merge(tenant)
        return tenant

    def delete(self, tenant_id):
        with Db.get() as self._db:
            self.__delete(tenant_id)
            self._db.commit()
        return "success"

    def __delete(self, tenant_id):
        tenant = None
        tenant = Tenant.get(self._db, tenant_id)
        if not tenant:
            raise NotFoundException()
        Tenant.delete(self._db, tenant_id)
