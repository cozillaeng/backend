import datetime
from client.aws import AmazonS3Client
from client.database import Db
from conf import Conf
from exception import InvalidInputException, DuplicateValueException
from model.common import Page
from model.society import Society
from services import DatabaseService
from exception import NotFoundException
from util.common import DictUtil


class SocietyService(DatabaseService):
    def add(self, society):
        with Db.get() as self._db:
            society = self.__add(society)
            self._db.commit()
            self._db.refresh(society)
        return society

    def get_page(self, start=0, count=50, order_by=None):
        if not start: start = 0
        if not count: count = 50

        page = Page()
        with Db.get() as self._db:
            page.items = Society.get_all(self._db, start, count+1,  order_by)
            page.total_count = Society.get_all_count(self._db)
            page.gen_page_data(start, count)
        return page

    def __add(self, society):
        society.id = None
        society.created_at = society.updated_at = datetime.datetime.now()
        society.created_by_id = society.updated_by_id = 1
        self._db.add(society)
        return society

    def update(self, id, society_obj):
        with Db.get() as self._db:
            society = Society.get(self._db, id)
            if not society:
                raise NotFoundException("Society not found")
            society = self.__update(society, society_obj)
            self._db.commit()
        return society

    def __update(self, society, society_obj):
        society_obj.created_at = society_obj.created_by_id = None
        society.from_json_dict(society_obj.to_dict())
        society.updated_at = datetime.datetime.now()
        society = self._db.merge(society)
        return society

    def delete(self, society_id):
        with Db.get() as self._db:
            self.__delete(society_id)
            self._db.commit()
        return "success"

    def __delete(self, society_id):
        society = None
        society = Society.get(self._db, society_id)
        if not society:
            raise NotFoundException()
        Society.delete(self._db, society_id)
