__author__ = 'ramakrishnag'

import datetime
from client.database import Db
from model.note import Note
from exception import InvalidInputException
from services import DatabaseService
from exception import NotFoundException
from model.common import Page
#from client.message_client import TimeLineMessageClient
from model.timeline import TimeLine
from services.timeline import TimeLineService

class NoteService(DatabaseService):
    #timeLineMessageClient = TimeLineMessageClient.get_instance()

    def add(self, note_obj):
        if not note_obj.owner_id:
            raise InvalidInputException("Invalid owner_type id")
        if not note_obj.owner_type:
            raise InvalidInputException("Invalid owner_type")
        with Db.get() as self._db:
            note = self.__add(note_obj)
            self._db.commit()
            self._db.refresh(note)

        #Adding an entry to on note creation action
        timeline_message = TimeLine()
        timeline_message.owner_type = "NOTE"
        timeline_message.owner_id = note.id
        timeline_message.description = "Created note " + str(note.id)
        timeline_message.created_at = datetime.datetime.now()
        TimeLineService().add(timeline_message)
        #self.timeLineMessageClient.send_message(timeline_message.to_json_dict())
        return note

    def __add(self, note_obj):
        note_obj.id = None
        note_obj.created_at = datetime.datetime.now()
        self._db.add(note_obj)
        return note_obj

    def delete(self, note_id):
        with Db.get() as self._db:
            self.__delete(note_id)
            self._db.commit()

    def __delete(self, note_id):
        Note.delete(self._db, note_id)

    def get(self, id):
        with Db.get() as self._db:
            note = self.__get_note(id)
            if not note:
                raise NotFoundException("Note not found")
            return note

    def __get_note(self, note_id):
        note = None
        note = Note.get(self._db, note_id)
        return note

    def get_page(self, start=0, count=5, owner_id = None, owner_type = None, order_by=None):
        if not start: start = 0
        if not count: count = 5

        page = Page()
        with Db.get() as self._db:
            page.items = Note.get_all_notes(self._db, start, count + 1, owner_id, owner_type, order_by)
            page.total_count = Note.get_all_count(self._db, owner_id, owner_type)
            page.gen_page_data(start, count)
        return page