import datetime
from client.aws import AmazonS3Client
from client.database import Db
from conf import Conf
from exception import InvalidInputException, DuplicateValueException
from model.common import Page
from model.owners_furniture import OwnersFurniture
from services import DatabaseService
from exception import NotFoundException
from util.common import DictUtil


class OwnersFurnitureService(DatabaseService):
    def add(self, owners_furniture):
        with Db.get() as self._db:
            owners_furniture = self.__add(owners_furniture)
            self._db.commit()
            self._db.refresh(owners_furniture)
        return owners_furniture

    def get_page(self, start=0, count=50, order_by=None):
        if not start: start = 0
        if not count: count = 50

        page = Page()
        with Db.get() as self._db:
            page.items = OwnersFurniture.get_all(self._db, start, count+1,  order_by)
            page.total_count = OwnersFurniture.get_all_count(self._db)
            page.gen_page_data(start, count)
        return page

    def __add(self, owners_furniture):
        owners_furniture.id = None
        owners_furniture.created_at = owners_furniture.updated_at = datetime.datetime.now()
        self._db.add(owners_furniture)
        return owners_furniture



    def update(self, id, owners_furniture_obj):
        with Db.get() as self._db:
            owners_furniture = OwnersFurniture.get(self._db, id)
            if not owners_furniture:
                raise NotFoundException("OwnersFurniture not found")
            owners_furniture = self.__update(owners_furniture, owners_furniture_obj)
            self._db.commit()
        return owners_furniture

    def __update(self, owners_furniture, owners_furniture_obj):
        owners_furniture_obj.created_at = owners_furniture_obj.created_by_id = None
        owners_furniture.from_json_dict(owners_furniture_obj.to_dict())
        owners_furniture.updated_at = datetime.datetime.now()
        owners_furniture = self._db.merge(owners_furniture)
        return owners_furniture

    def delete(self, owners_furniture_id):
        with Db.get() as self._db:
            self.__delete(owners_furniture_id)
            self._db.commit()
        return "success"

    def __delete(self, owners_furniture_id):
        owners_furniture = None
        owners_furniture = OwnersFurniture.get(self._db, owners_furniture_id)
        if not owners_furniture:
            raise NotFoundException()
        OwnersFurniture.delete(self._db, owners_furniture_id)
