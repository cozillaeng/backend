import datetime
from client.aws import AmazonS3Client
from client.database import Db
from conf import Conf
from exception import InvalidInputException, DuplicateValueException
from model.common import Page
from model.furniture_set import FurnitureSet
from services import DatabaseService
from exception import NotFoundException
from util.common import DictUtil


class FurnitureSetService(DatabaseService):
    def add(self, furniture_set):
        with Db.get() as self._db:
            furniture_set = self.__add(furniture_set)
            self._db.commit()
            self._db.refresh(furniture_set)
        return furniture_set

    def get_page(self, start=0, count=50, order_by=None):
        if not start: start = 0
        if not count: count = 50

        page = Page()
        with Db.get() as self._db:
            page.items = FurnitureSet.get_all(self._db, start, count+1,  order_by)
            page.total_count = FurnitureSet.get_all_count(self._db)
            page.gen_page_data(start, count)
        return page

    def __add(self, furniture_set):
        furniture_set.id = None
        if not furniture_set.first_name or not furniture_set.last_name:
            raise InvalidInputException("First name or last name can not be empty")
        furniture_set.created_at = furniture_set.updated_at = datetime.datetime.now()
        self._db.add(furniture_set)
        return furniture_set



    def update(self, id, furniture_set_obj):
        with Db.get() as self._db:
            furniture_set = FurnitureSet.get(self._db, id)
            if not furniture_set:
                raise NotFoundException("FurnitureSet not found")
            furniture_set = self.__update(furniture_set, furniture_set_obj)
            self._db.commit()
        return furniture_set

    def __update(self, furniture_set, furniture_set_obj):
        furniture_set_obj.created_at = furniture_set_obj.created_by_id = None
        furniture_set.from_json_dict(furniture_set_obj.to_dict())
        furniture_set.updated_at = datetime.datetime.now()
        furniture_set = self._db.merge(furniture_set)
        return furniture_set

    def delete(self, furniture_set_id):
        with Db.get() as self._db:
            self.__delete(furniture_set_id)
            self._db.commit()
        return "success"

    def __delete(self, furniture_set_id):
        furniture_set = None
        furniture_set = FurnitureSet.get(self._db, furniture_set_id)
        if not furniture_set:
            raise NotFoundException()
        FurnitureSet.delete(self._db, furniture_set_id)
