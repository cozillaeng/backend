import copy
from client.database import Db
from exception import NotFoundException
from model.product_category import ProductCategory, ProductCategoryChild
from model.product_sku import ProductSku
from services import DatabaseService


class CategoryService(DatabaseService):
    def get_all(self, is_enabled=None):
        with Db.get() as self._db:
            return ProductCategory.get_all(self._db, is_enabled=is_enabled)

    def get_tree(self, is_enabled=None):
        with Db.get() as self._db:
            categories = ProductCategory.get_all(self._db, is_enabled=is_enabled)
            category_children = ProductCategoryChild.get_all(self._db)

        categories_map = {}
        children_map = {}
        for category in categories:
            categories_map[category.id] = category
            children_map[category.id] = self.__get_children(category.id, category_children)

        categories_tree = []
        for category in categories:
            parents = self.__get_parents(category.id, category_children)
            if len(parents) == 0:
                category.level = 0
                categories_tree.append(category)
                self.__load_hierarchy(category, categories_map, children_map)

        return categories_tree

    def get_children(self, category_id):
        with Db.get() as self._db:
            category = ProductCategory.get(self._db, category_id)
            if not category:
                raise NotFoundException()
            children_mappings = ProductCategoryChild.get_all_by_category(self._db, category.id)
            children_ids = [m.child_category_id for m in children_mappings]
            return ProductCategory.get_by_ids(self._db, children_ids)

    def get_products(self, category_id):
        with Db.get() as self._db:
            category = ProductCategory.get(self._db, category_id)
            if not category:
                raise NotFoundException()
            products = Product.get_all_by_category(self._db, category.id)
            products_list = []
            for product in products:
                # Fetch product sku details and append info
                products_list.append(product)

            return products_list

    def __load_hierarchy(self, category, categories_map, children_map):
        children_ids = children_map[category.id]
        if len(children_ids) == 0: return

        for child_id in children_ids:
            if not hasattr(category, "children"):
                category.children = []
            child = copy.copy(categories_map[child_id])
            child.level = category.level + 1
            category.children.append(child)

        for child in category.children:
            self.__load_hierarchy(child, categories_map, children_map)

    def __get_children(self, category_id, category_children):
        children = []
        for category_child in category_children:
            if category_child.category_id == category_id:
                children.append(category_child.child_category_id)
        return children

    def __get_parents(self, category_id, category_children):
        parents = []
        for category_child in category_children:
            if category_child.child_category_id == category_id:
                parents.append(category_child.category_id)
        return parents
