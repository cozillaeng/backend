import logging
import os
import math
from boto import sqs

from boto.s3.connection import S3Connection, OrdinaryCallingFormat
from boto.s3.key import Key
from filechunkio import FileChunkIO


class AmazonS3Client(object):
    def __init__(self, key_id, key_secret):
        self.__connection = S3Connection(aws_access_key_id=key_id, aws_secret_access_key=key_secret)

    def get_bucket(self, bucket_name):
        return self.__connection.get_bucket(bucket_name)

    def download_as_str(self, bucket, key):
        k = Key(bucket)
        k.key = key
        return k.get_contents_as_string()

    def move(self, bucket_name, src_key, target_key):
        k = Key(self.get_bucket(bucket_name))
        k.key = src_key
        k.copy(bucket_name, target_key)
        k.delete()

    def put_str(self, bucket, key, data, headers=None):
        k = Key(bucket)
        k.key = key
        k.set_contents_from_string(data, headers=headers)

    def download_to_file_stream(self, bucket, key, filename):
        k = Key(bucket)
        k.key = key
        k.get_contents_to_file(filename)

    def put_file_stream(self, bucket, key, stream, headers=None):
        k = Key(bucket)
        k.key = key
        k.set_contents_from_stream(stream, headers=headers)

    def download_to_filename(self, bucket, key, filename):
        k = Key(bucket)
        k.key = key
        k.get_contents_to_filename(filename)

    def put_filename(self, bucket, key, filename):
        k = Key(bucket)
        k.key = key
        k.set_contents_from_filename(filename)

    def put_multipart_filename(self, bucket, key, filename):
        bucket = self.__connection.get_bucket(bucket)
        mp = bucket.initiate_multipart_upload(key)

        source_size = os.stat(filename).st_size
        chunk_size = 1048576
        chunk_count = int(math.ceil(source_size / chunk_size))
        for i in range(chunk_count + 1):
            logging.debug("Uploading part: %s" % i)
            offset = chunk_size * i
            chunk_bytes = min(chunk_size, source_size - offset)
            with FileChunkIO(filename, 'r', offset=offset, bytes=chunk_bytes) as fp:
                mp.upload_part_from_file(fp, part_num=i + 1)
        mp.complete_upload()

    def get_key(self, bucket, key):
        return bucket.get_key(key)

    def get_keys(self, bucket, prefix=""):
        return [key.name for key in bucket.list(prefix=prefix)]

    def delete_key(self, bucket, key):
        bucket.delete_key(key)

    def delete_keys(self, bucket, prefix=""):
        keys = self.get_keys(bucket, prefix)
        if len(keys) > 0:
            bucket.delete_keys(keys)


class AmazonSQSClient(object):
    def __init__(self, access_key_id, secret_access_key, region):
        self.__connection = sqs.connect_to_region(region_name=region, aws_access_key_id=access_key_id,
                                                  aws_secret_access_key=secret_access_key)

    def get_queue(self, name):
        return self.__connection.get_queue(name)

    def get(self, queue):
        messages = queue.get_messages(1)
        if len(messages) > 0:
            return messages[0]

    def get_multiple(self, queue, count):
        return queue.get_messages(count)

    def send(self, queue, message):
        queue.write(message)

    def delete(self, queue, message):
        queue.delete_message(message)

    def get_count(self, queue):
        return queue.count()

    def clear(self, queue):
        queue.clear()
