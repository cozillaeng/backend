from contextlib import contextmanager
import logging

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from conf import Conf


class Db(object):
    __Session = None
    __instance = None

    @staticmethod
    def get_instance():
        if Db.__instance is None:
            Db.__instance = Db()
        return Db.__instance

    def init(self):
        Db.__Session = sessionmaker(autocommit=False, autoflush=False, expire_on_commit=False, bind=self.get_engine())
        logging.debug("DbConfig initialized")

    def get_engine(self):
        db_conf = Conf.get("database")
        db_baseurl = "mysql://%s/%s?user=%s&passwd=%s" % (
            db_conf["host"], db_conf["name"], db_conf["user"], db_conf["password"])
        logging.info("DB Baseurl: %s" % db_baseurl)
        return create_engine(db_baseurl, echo=db_conf["sql_logging"], pool_recycle=3600)

    def __get_session(self):
        return Db.__Session()

    @staticmethod
    def get_db():
        return Db.get_instance().__get_session()

    @staticmethod
    @contextmanager
    def get():
        db = Db.get_db()
        try:
            yield db
        except:
            db.rollback()
            raise
        finally:
            db.close()
