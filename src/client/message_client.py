from util.json_util import dump_json

__author__ = 'ramakrishnag'

from contextlib import contextmanager
from aws import AmazonSQSClient
from conf import Conf
from boto.sqs.message import Message
import json
from threading import Thread
from time import sleep

class TimeLineMessageClient(object):
    __sqs_client = None
    __queue = None
    __instance = None
    __started = None

    @staticmethod
    def get_instance():
        if TimeLineMessageClient.__instance is None:
            TimeLineMessageClient.__instance = TimeLineMessageClient()
        return TimeLineMessageClient.__instance

    def init(self):
        if not self.__started:
            self.__started = True
            TimeLineMessageClient.__sqs_client = self.attach_sqs_client()
            TimeLineMessageClient.__queue = self.attach_sqs_queue()
            listener_thread = Thread(target=self.get_messages, args=(10,))
            listener_thread.start()

    def attach_sqs_client(self):
        sqs_conf = Conf.get("sqs_params")
        return AmazonSQSClient(sqs_conf["aws_access_key_id"], sqs_conf["aws_secret_key"],sqs_conf["aws_region"])

    def attach_sqs_queue(self):
        sqs_conf = Conf.get("sqs_params")
        return TimeLineMessageClient.__sqs_client.get_queue(sqs_conf.get("timeline_queue"))

    def __get_queue_instance(self):
        return TimeLineMessageClient.__queue

    def send_message(self, message):
        m = Message()
        m.set_body(dump_json(message))
        self.__get_queue_instance().write(m)

    def delete_message(self, message):
        self.__get_queue_instance().delete_message(message)

    def get_messages(self,message_count):
        while True:
            print "Getting message"
            processed_messages = []
            messages = self.__get_queue_instance().get_messages(message_count)
            if len(messages) > 0:
                for message in messages:
                    try:
                        message_body = message.get_body()
                        message_dict = json.loads(message_body)
                        print message_dict
                        processed_messages.append(message)
                    except Exception as e:
                        print "Exception while processing message"
                if len(processed_messages) > 0:
                    self.__get_queue_instance().delete_message_batch(processed_messages)

            sleep(10)

    @staticmethod
    @contextmanager
    def get():
        instance = TimeLineMessageClient.get_instance()
        try:
            yield instance
        except:
            raise
