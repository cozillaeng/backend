import logging
import os

from client.database import Db
from conf import Conf


# noinspection PyUnresolvedReferences
from model import *
# noinspection PyUnresolvedReferences
from model.common import *
# noinspection PyUnresolvedReferences
from model.designer import *


def init():
    logging.info("Initializing")
    Conf.get_instance().init(os.path.dirname(os.path.realpath(__file__)))
    Db.get_instance().init()
    logging.info("Initialize successful")


def migrate():
    logging.info("Migrating tables")
    ModelBase.metadata.create_all(Db.get_instance().get_engine())
    logging.info("Migration successful")


def insert_data():
    logging.info("Inserting data")
    with Db.get() as db:
        if not Designer.get_by_email(db, "rajeev1982@gmail.com"):
            db.add(Designer(email="rajeev1982@gmail.com", first_name="Rajeev", last_name="Sharma",
                            phone="+919611666009", is_approved=True, source="IN_HOUSE"))
        db.commit()
    logging.info("Insert successful")


if __name__ == "__main__":
    init()
    migrate()
    insert_data()
