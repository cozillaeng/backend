from app import flask as application
# noinspection PyUnresolvedReferences
import controller

if __name__ == '__main__':
    application.run(host="api.cozilla.local", port=7000, debug=True)
