import os

from flask import Flask
from client.database import Db
from conf import Conf
from decorator import Controller, Permissions, Security

flask = Flask(__name__, template_folder="../templates", static_folder="../static")
Conf.get_instance().init(os.path.dirname(os.path.realpath(__file__)))
Db.get_instance().init()

controller = Controller.get_instance()
permissions = Permissions.get_instance()
security = Security.get_instance()
